package com.example.shaircombhair;

import java.util.Timer;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;
import android.util.Log;

public class SensorService extends Service implements SensorEventListener
{
    private static final String TAG = "SensorService";

    private boolean isRunning  = false;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mMagnetometer; 
    private float[] mGravity;
    private float[] mGeomagnetic;   
	private float[] mValuesMagnet      			= new float[3];
	private float[] mValuesAccel       			= new float[3];
	private float[] mValuesOrientation 			= new float[3];
	
	private double mTargetAngle;
	SharedPreferences mSharedTargetAngle;
	float targetX = 200.0f;
	float targetY = 200.0f;
	float currentX;
	float currentY;	
	Timer mUpdateTimer;	    
    
    
    

	public void onAccuracyChanged(Sensor sensor, int accuracy) {  }


	public void onSensorChanged(SensorEvent event) 
	{
	     if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
	       mGravity = event.values;
	     if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
	       mGeomagnetic = event.values;
	     
	     if (mGravity != null && mGeomagnetic != null) 
	     {
	       float R[] = new float[9];
	       float I[] = new float[9];
	       boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
	       
	       if (success) 
	       {
	    	   float diff = 0;
	         float orientation[] = new float[3];
	         SensorManager.getOrientation(R, orientation);
	         mValuesOrientation[0] = orientation[0] * GlobalConstants.RAD2DEG; // orientation contains: azimut, pitch and roll
	         mValuesOrientation[1] = orientation[1] * GlobalConstants.RAD2DEG;
	         mValuesOrientation[2] = orientation[2] * GlobalConstants.RAD2DEG;
	         
	         if  (mValuesOrientation[0] <0)
	         {
	        	  diff= 180 +mValuesOrientation[0] ;
	        	  mValuesOrientation[0] = 180+diff;
	         }
	       }
	     }
	   } 
    
    
    
    
    
    
    @Override
    public void onCreate()
    {
        Log.i(TAG, "Service onCreate");
        isRunning = true;
	    mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
	    mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	    mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);    
	    
	    mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
	    mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_UI);	
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) 
    {

        Log.i(TAG, "Service onStartCommand");


        new Thread(new Runnable() 
        {
            @Override
            public void run() 
            {
            	while(true)
            	{
	            	try
	                {
	                    Thread.sleep(10000);
	    				//mTargetAngle = mValuesOrientation[0];// - currentAngle;
	
	    	    		Log.i(TAG,"SensorService:   " + mValuesOrientation[0] );
	
	                }
	                catch (Exception e) 
	                {
	                    	
	                }
	
	                if(isRunning)
	                {
	                    Log.i(TAG, "Service running");
	                }
            	}

                //stopSelf();
            }
        }).start();

        return Service.START_STICKY;
    }

	
	@Override
	public IBinder onBind(Intent intent) 
	{
		return null;
	}
	
	
	
    @Override
    public void onDestroy() 
    {

        isRunning = false;
        
		if(mSensorManager != null)
			mSensorManager.unregisterListener(this);
		
        Log.i(TAG, "Service onDestroy");
    }

}
