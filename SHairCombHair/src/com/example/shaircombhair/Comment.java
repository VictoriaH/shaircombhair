package com.example.shaircombhair;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Comment {
	
	private String commentID = "na";
	private String userID = "nobody"; //wer hat kommentiert
	private String text = "nothing"; //was wurde kommentiert
	public final static SimpleDateFormat FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss", Locale.US);
	private Date mDate = new Date(); //wann wurde kommentiert
	private String pictureID ="no picture"; //zu welchem bild wurde kommentiert

	
	public String getCommentID() {
		return commentID;
	}
	
	public void setCommentID(String id) {
		commentID=id;
	}
	
	public String getUserID() {
		return userID;
	}
	
	public void setUserID(String id) {
		userID=id;
	}

	public String getText() {
		return text;
	}
	
	public void setText(String newText) {
		text=newText;
	}

	public Date getDate() {
		return mDate;
	}

	public void setDate(Date commentDate) {
		mDate = commentDate;
	}
	
	public String getPictureID() {
		return pictureID;
	}
	
	public void setPictureID(String id) {
		pictureID=id;
	}
}
