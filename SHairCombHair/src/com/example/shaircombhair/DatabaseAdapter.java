package com.example.shaircombhair;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import org.json.JSONException;

import com.example.shaircombhair.ListPicture.Category;
import com.example.shaircombhair.MainActivity.ListType;

import android.os.Environment;
import android.util.Log;
import android.view.HapticFeedbackConstants;
import android.view.ViewDebug.HierarchyTraceType;

public class DatabaseAdapter {	
	String file_name;
	File mPath;
	Database mDatabase;
	int numberOfHaircutters;
	int numberOfSelfies;
	ArrayList<ListPicture> haircutterPictures;
	ArrayList<ListPicture> tempSelfiesPictures;
	ArrayList<ListPicture> selfiesPictures;
	
public DatabaseAdapter () { // get a ListPicture from Database by entering the ID
	file_name = "friseur_database.txt";
	mPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
	mDatabase = new Database(mPath.getPath());
	mDatabase.loadFromFile_friseur();
	mDatabase.loadFromFile_selfie();
	numberOfHaircutters = mDatabase.numberOfHaircutters;
	numberOfSelfies = mDatabase.numberOfSelfies;
	Log.i("DatabaseAdapter", "number of haircutters: " + numberOfHaircutters);
	Log.i("DatabaseAdapter", "number of selfies: " + numberOfSelfies);
	}

public ArrayList<ListPicture> getPictureArray(ListType type) {
	Log.i("DatabaseAdapter","1");
	//leere liste
			ArrayList<ListPicture> picList = new ArrayList<ListPicture>();
	
	ListPicture bild1; 
	ListPicture bild2;
	ListPicture bild3;
	bild1 = new ListPicture();
	bild2 = new ListPicture();
	bild3 = new ListPicture();
	//entsprechende liiste erstellen
	
	if(type == ListType.bestselfie){
		//leeree Bildelemente
		selfiesPictures = new ArrayList<ListPicture>();
		
		for(int i=0;i<numberOfSelfies;i++){
			selfiesPictures.add(getListPictureFromSelfieID(i));
			//Log.i("DatabaseAdapter","Selfie index: " +i);
		}
		
		Collections.sort(selfiesPictures, new Comparator() {
		    @Override
		    public int compare(Object o1, Object o2) {
		    	ListPicture pic1=(ListPicture) o1;
		    	ListPicture pic2=(ListPicture) o2;
		    	if (pic1.getRating() > pic2.getRating()) {
		    		return -1;
		        	}
		        if (pic1.getRating() < pic2.getRating()) {
		        	return 1;
		        	}
		            return 0;
		    }
		});
		picList = selfiesPictures;
		Log.i("DatabaseAdapter","made best selfie");
	}
	if(type == ListType.besthaircutter){
		//leeree Bildelemente
		haircutterPictures = new ArrayList<ListPicture>();
		
		for(int i=0;i<numberOfHaircutters;i++){
			haircutterPictures.add(getListPictureFromHaircutterID(i));
			//Log.i("DatabaseAdapter","Haircutter index: " +i);
		}
		
		Collections.sort(haircutterPictures, new Comparator() {
		    @Override
		    public int compare(Object o1, Object o2) {
		    	ListPicture pic1=(ListPicture) o1;
		    	ListPicture pic2=(ListPicture) o2;
		    	if (pic1.getHaircutterRating() > pic2.getHaircutterRating()) {
		    		return -1;
		        	}
		        if (pic1.getHaircutterRating() < pic2.getHaircutterRating()) {
		        	return 1;
		        	}
		            return 0;
		    }
		});
		picList = haircutterPictures;
		Log.i("DatabaseAdapter","made best haircutter");
	}
	if(type == ListType.newselfie){
		Log.i("DatabaseAdapter","entered new selfie");
		
		selfiesPictures = new ArrayList<ListPicture>();
		
		for(int i=numberOfSelfies-1;i>=0;i--){
			selfiesPictures.add(getListPictureFromSelfieID(i));
			//Log.i("DatabaseAdapter","Selfie index: " +i);
		}
		picList = selfiesPictures;
		Log.i("DatabaseAdapter","made new selfie");
	}
	if(type == ListType.newhaircutter){
		//leeree Bildelemente
		haircutterPictures = new ArrayList<ListPicture>();
		for(int i=numberOfHaircutters-1;i>=0;i--){
			haircutterPictures.add(getListPictureFromHaircutterID(i));
			//Log.i("DatabaseAdapter","Haircutter index: " +i);
		}
		picList = haircutterPictures;
		Log.i("DatabaseAdapter","made new haircutter");
	}
	if(type == ListType.none){
		//Bildeintraege erstellen: (bei none: best selfie)
		//leeree Bildelemente
		selfiesPictures = new ArrayList<ListPicture>();
		
		for(int i=0;i<numberOfSelfies;i++){
			selfiesPictures.add(getListPictureFromSelfieID(i));
			//Log.i("DatabaseAdapter","Selfie index: " +i);
		}
		
		Collections.sort(selfiesPictures, new Comparator() {
		    @Override
		    public int compare(Object o1, Object o2) {
		    	ListPicture pic1=(ListPicture) o1;
		    	ListPicture pic2=(ListPicture) o2;
		    	if (pic1.getRating() > pic2.getRating()) {
		    		return -1;
		        	}
		        if (pic1.getRating() < pic2.getRating()) {
		        	return 1;
		        	}
		            return 0;
		    }
		});
		picList = selfiesPictures;
		Log.i("DatabaseAdapter","made none");
	}

	return picList;
}


public ArrayList<ListPicture> getPictureArrayFromHaircutterID(int HaircutterID) {
	
	
	selfiesPictures = new ArrayList<ListPicture>();
	tempSelfiesPictures = new ArrayList<ListPicture>();
	
	for(int i=0;i<numberOfSelfies;i++){
		tempSelfiesPictures.add(getListPictureFromSelfieID(i));
		//Log.i("DatabaseAdapter","Selfie index: " +i);
		if (tempSelfiesPictures.get(i).getShotAtHaircutterID() == HaircutterID){
			selfiesPictures.add(tempSelfiesPictures.get(i));
		}
	}
	return selfiesPictures;
}

public ArrayList<ListPicture> getPictureArrayFromUserID(int userID){	
	selfiesPictures = new ArrayList<ListPicture>();
	tempSelfiesPictures = new ArrayList<ListPicture>();
	
	for(int i=0;i<numberOfSelfies;i++){
		tempSelfiesPictures.add(getListPictureFromSelfieID(i));
		//Log.i("DatabaseAdapter","Selfie index: " +i);
		if (tempSelfiesPictures.get(i).getUserID() == userID){
			selfiesPictures.add(tempSelfiesPictures.get(i));
		}
	}
	return selfiesPictures;
}


public ListPicture getListPictureFromSelfieID(int ID){	
	
	ListPicture bild; 
	bild = new ListPicture();
	SelfieInformation currentSefieInfo = new SelfieInformation();
	
	try {
		currentSefieInfo = mDatabase.getSelfie(ID);
		Log.i("DatabaseAdapter","selfie info 1  " + currentSefieInfo.selfieID + " " + currentSefieInfo.userID);		
		} catch (JSONException e) {
		e.printStackTrace();
		Log.i("db", "gestSelfie error");
		}
	
	bild.setCategory(Category.selfie);
	bild.setSelfieID(ID);
	bild.setUserName(currentSefieInfo.userName);
	bild.setUserID(currentSefieInfo.userID);
	bild.setImageLocation(currentSefieInfo.mPictureLocation);
	bild.setImageLocation2(currentSefieInfo.mPictureLocation2);
	bild.setShotAtHaircutterID(currentSefieInfo.shotAtHaircutterID);
	bild.setRating(currentSefieInfo.mRating);
	bild.setNumberOfRatings(currentSefieInfo.mNumberOfRatings);
	//bild.setDate(currentSelfieInfo.mDate);

	Log.i("db", "Loaction Database Adapter: " + bild.getImageLocation());
	return bild;
}


public ListPicture getListPictureFromHaircutterID(int HaircutterID){	
	ListPicture bild; 
	bild = new ListPicture();
	HaircutterInformation currentHaircutterInfo = new HaircutterInformation();
	
	try {
		currentHaircutterInfo = mDatabase.getFriseur(HaircutterID);
		Log.i("DatabaseAdapter","cutter info 1  " + currentHaircutterInfo.id + " " + currentHaircutterInfo.name);		
		} catch (JSONException e) {
		e.printStackTrace();
		}
	
	bild.setCategory(Category.haircutter);
	bild.setHaircutterID(HaircutterID);
	bild.setHaircutterName(currentHaircutterInfo.name);
	bild.setImageLocation(currentHaircutterInfo.mPictureLocation);
	bild.setHaircutterAdress(currentHaircutterInfo.adress);
	bild.setHaircutterRating(currentHaircutterInfo.rating);
	bild.setHaircutterOpeningHours(currentHaircutterInfo.openingHours);
	bild.setHaircutterLongitude(currentHaircutterInfo.longitude);
	bild.setHaircutterLatitude(currentHaircutterInfo.latitude);

	return bild;
}

public HaircutterInformation getHaircuterInformationFromListpicture(ListPicture haircutter) {
	HaircutterInformation haircutterInfo = new HaircutterInformation();
	haircutterInfo.id = haircutter.getHaircutterID();
	haircutterInfo.name = haircutter.getHaircutterName();
	haircutterInfo.adress = haircutter.getHaircutterAdress();
	haircutterInfo.rating = haircutter.getHaircutterRating();
	haircutterInfo.openingHours = haircutter.getHaircutterOpeningHours();
	haircutterInfo.longitude = haircutter.getHaircutterLongitude();
	haircutterInfo.latitude = haircutter.getHaircutterLatitude();
	haircutterInfo.mPictureLocation = haircutter.getImageLocation();
	return haircutterInfo;
}

public SelfieInformation getSelfieInformationFromListpicture(ListPicture selfie) {
	SelfieInformation selfieInfo = new SelfieInformation();
	selfieInfo.selfieID = selfie.getSelfieID();
	selfieInfo.userName = selfie.getUserName();
	selfieInfo.userID = selfie.getUserID();
	selfieInfo.mPictureLocation = selfie.getImageLocation();
	selfieInfo.mPictureLocation2 = selfie.getImageLocation2();
	selfieInfo.shotAtHaircutterID = selfie.getShotAtHaircutterID();
	selfieInfo.mRating = selfie.getRating();
	selfieInfo.mNumberOfRatings = selfie.getNumberOfRatings();
	return selfieInfo;
}

public void updateHaircutterInfoInDB(HaircutterInformation newHaircutterInfo){
	try{
		mDatabase.updateHaircutterInfo(newHaircutterInfo);
	}catch (JSONException e){
		Log.i("DatabaseAdaper", "HaircutterInfo Not updated in txt");
	}catch (FileNotFoundException e) {
		Log.i("DatabaseAdaper", "HaircutterInfo Not updated in txt");
	}
	
}

public void updateSelfieInfoInDB(SelfieInformation newSelfieInfo){
	try{
		mDatabase.updateSelfieInfo(newSelfieInfo);
	}catch (JSONException e){
		Log.i("DatabaseAdaper", "SelfieInfo Not updated in txt");
	}catch (FileNotFoundException e) {
		Log.i("DatabaseAdaper", "SelfieInfo Not updated in txt");
	}
	
}
}
