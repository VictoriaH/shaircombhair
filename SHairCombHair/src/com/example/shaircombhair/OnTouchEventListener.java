package com.example.shaircombhair;

public interface OnTouchEventListener
{
    public void onFingerDownEvent(double vx, double vy, double px, double py);
    public void onFingerMoveEvent(double vx, double vy, double px, double py);
    public void onFingerUpEvent(double vx, double vy, double px, double py);
}
