package com.example.shaircombhair;



import java.util.Timer;
import java.util.TimerTask;

import com.google.android.gms.maps.model.LatLng;

import nxr.tpad.lib.TPad;
import nxr.tpad.lib.TPadImpl;
import nxr.tpad.lib.TPadService;
import nxr.tpad.lib.consts.TPadVibration;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Point;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;

public class MapHapticActivity extends Activity implements OnTouchEventListener, SensorEventListener
{
	private final String TAG = "MapHapticActivity";
   
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mMagnetometer; 
    private float[] mGravity;
    private float[] mGeomagnetic;   
	private float[] mValuesMagnet      			= new float[3];
	private float[] mValuesAccel       			= new float[3];
	private float[] mValuesOrientation 			= new float[3];
	final float pi = (float) Math.PI;
	final float rad2deg = 180/pi;  
	private double mTargetAngle;
	SharedPreferences mSharedTargetAngle;
	private float mDiffAngle;
	private float mOldDiffAngle;
	float targetX = 200.0f;
	float targetY = 200.0f;
	float currentX;
	float currentY;	
	Timer mUpdateTimer;	
	

	public VelocityTracker vTracker 			= null;
	private static float vy 					= 0.0f;
	private static float vx 					= 0.0f;
	private static float px 					= 0.0f;
	private static float py 					= 0.0f;

	private TPad mTpad 							= null;
	
	

	private Bitmap mArrowBmp;
	private float mBitmapx;
	private float mBitmapy;
	private Matrix mMatrix;
    private Bitmap mRotatedBitmap;
	
	

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_map_haptic);
		setContentView(new MyView(this));
		
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
	    mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
	    mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	    mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

	    mSharedTargetAngle = getSharedPreferences("SharedAngleFile", 0);
	    mTargetAngle = mSharedTargetAngle.getFloat("SharedAngle",(float)mTargetAngle);
	    
		mArrowBmp = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);
		mBitmapx = (float) mArrowBmp.getWidth() / 2;
		mBitmapy = (float) mArrowBmp.getHeight() / 2;

	    mRotatedBitmap = Bitmap.createBitmap(mArrowBmp , 0, 0, mArrowBmp .getWidth(), mArrowBmp .getHeight(), mMatrix, true);
	}
	
	
	public void onAccuracyChanged(Sensor sensor, int accuracy) {  }


	public void onSensorChanged(SensorEvent event) 
	{
	     if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
	       mGravity = event.values;
	     if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
	       mGeomagnetic = event.values;
	     
	     if (mGravity != null && mGeomagnetic != null) 
	     {
	       float R[] = new float[9];
	       float I[] = new float[9];
	       boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
	       
	       if (success) 
	       {
	    	   float diff = 0;
	         float orientation[] = new float[3];
	         SensorManager.getOrientation(R, orientation);
	         mValuesOrientation[0] = orientation[0] * rad2deg; // orientation contains: azimut, pitch and roll
	         mValuesOrientation[1] = orientation[1] * rad2deg;
	         mValuesOrientation[2] = orientation[2] * rad2deg;
	         
	         if  (mValuesOrientation[0] < 0)
	         { 
	        	  mValuesOrientation[0] = mValuesOrientation[0] + 360;
	         }
	       }
	     }
	   }
	   
	   
    public class MyView extends View
    {
    	Paint paint;
        public MyView(Context context) 
        {
             super(context);
             paint = new Paint();
        }

        @Override
        protected void onDraw(Canvas canvas) 
        {
           super.onDraw(canvas);
           int x = getWidth();
           int y = getHeight();
           int radius;
           radius = 80;
           
           
           paint.setStyle(Paint.Style.FILL);
           paint.setColor(Color.WHITE);
           canvas.drawPaint(paint);
           // Use Color.parseColor to define HTML colors
           paint.setColor(Color.parseColor("#0000ff"));
           
           canvas.drawCircle(targetX, targetY-0, radius, paint);
           //Bitmap mArrowBmp = BitmapFactory.decodeResource(getResources(), R.drawable.arrow);
           
         
           if(  Math.abs(mOldDiffAngle - mDiffAngle) > 2.0f)
           {
        	   
	 	       mMatrix = new Matrix();
		       mMatrix.postTranslate(-mArrowBmp.getWidth()/2, -mArrowBmp.getHeight()/2);
		       mMatrix.postRotate((float)mDiffAngle);         	   
	   			//mMatrix.postTranslate(px, py);
	           mRotatedBitmap = Bitmap.createBitmap(mArrowBmp , 0, 0, mArrowBmp.getWidth(), mArrowBmp.getHeight(), mMatrix, true);
	           
	           mOldDiffAngle = mDiffAngle;
	           canvas.drawBitmap(mRotatedBitmap,targetX,targetY,null);
	           mRotatedBitmap.recycle();
           }
           
        
           invalidate();

       }
    }
	

	public boolean onTouchEvent(MotionEvent event) 
	{
		px = event.getX();
		py = event.getY() - 160.0f;

		switch (event.getActionMasked()) 
		{
			case MotionEvent.ACTION_DOWN:
	
				vx = 0.0f;
				vy = 0.0f;
	
				if (vTracker == null) 
				{
					vTracker = VelocityTracker.obtain();
				} 
				else 
				{
					vTracker.clear();
				}
				vTracker.addMovement(event);
	
				onFingerDownEvent(vx, vy, px, py);
				break;

			case MotionEvent.ACTION_MOVE:

				vTracker.addMovement(event);
				vTracker.computeCurrentVelocity(1);
				vx = vTracker.getXVelocity(); 
				vy = vTracker.getYVelocity(); 
	
				onFingerMoveEvent(vx, vy, px, py);	 	
				break;
	
			case MotionEvent.ACTION_UP:
				
				onFingerUpEvent(vx, vy, px, py);
				break;
		
			case MotionEvent.ACTION_CANCEL:

				vTracker.recycle();
				vTracker = null;
				break;
		}

		return true;
	}


	public void onFingerDownEvent(double vx, double vy, double px, double py) 
	{	
		currentX = (float) px;
		currentY = (float) py;
		
	    mUpdateTimer = new Timer("mUpdateTimer");
	    mUpdateTimer.scheduleAtFixedRate(new TimerTask()
	    {
	    	public void run()
			{
	    		
	    		
	    		// TODO
	    		mDiffAngle = (float) (mTargetAngle - mValuesOrientation[0]);
	    		mDiffAngle = (mDiffAngle+360) % 360;
	    		//mDiffAngle = mValuesOrientation[0];
	    		Log.i("MapHap","Target: "+ mDiffAngle );
	    		//Log.i("TAG","Curr:   " +(currentX) + " " + (currentY) + "Target:" + targetX + " " + targetY );

	    		
				if( Math.pow((Math.abs(currentY - targetY)  + Math.abs(currentX - targetX)),1.3) < 540.0f )
	    		{
	    			int tmp = (int)Math.abs(currentX - targetX);
	    			if( UtilFunctions.mod(tmp,2) == 1)
	    				mTpad.sendVibration(TPadVibration.SAWTOOTH, 120, 1.0f);
	    			else
	    				mTpad.sendVibration(TPadVibration.SAWTOOTH, 210, 1.0f);
	    			
	    		}
	    		else
	    			mTpad.turnOff();
	    		
	    		double r = 5;

	    		
				targetX = targetX + (float)( r*Math.sin(Math.toRadians(mDiffAngle) ));//+ r*Math.sin(Math.toRadians(mDiffAngle))));
				targetY = targetY - (float)( r*Math.cos(Math.toRadians(mDiffAngle) ));//+ r*Math.cos(Math.toRadians(mDiffAngle))));
	    		
				CheckBounds(currentX,currentY);
	    	
	    		
	    		/*
	    		if( (mDiffAngle >= 0) &&  (mDiffAngle < 90) )
	    		{
		    		if(targetX > 200 + (float)(500.0 * Math.cos(Math.toRadians(mDiffAngle))) )
		    			targetX = 200;	 
		    		if(targetY > 200 + 500.0 * Math.sin(Math.toRadians(mDiffAngle)))
		    			targetY = 200;
	    			
	    		}
	    		else if( (mDiffAngle >= 90) &&  (mDiffAngle < 180) )
	    		{
		    		if(targetX < 700 +(float)(500.0 * Math.cos(Math.toRadians(mDiffAngle))) )
		    			targetX = 700;	
		    		if(targetY > 200 +(float)(500.0 * Math.sin(Math.toRadians(mDiffAngle))) )
		    			targetY = 200;
	    		}
	    		else if( (mDiffAngle >= 180) &&  (mDiffAngle < 270) )
	    		{
		    		if(targetX < 700 +(float)(500.0 * Math.cos(Math.toRadians(mDiffAngle))) )
		    			targetX = 700;	
		    		if(targetY < 700 +(float)(500.0 * Math.sin(Math.toRadians(mDiffAngle))) )
		    			targetY = 700;
	    		}
	    		else if( (mDiffAngle >= 270) &&  (mDiffAngle < 360) )
	    		{
		    		if(targetX > 200 + (float)(500.0 * Math.cos(Math.toRadians(mDiffAngle))) )
		    			targetX = 200;	 
		    		if(targetY < 700 + (float)(500.0 * Math.sin(Math.toRadians(mDiffAngle))) )
		    			targetY = 700;
	    		}
	    		else
	    			mDiffAngle = (float)UtilFunctions.mod((int)mDiffAngle,360);
	    		*/
		  		if(mValuesOrientation[1] < -60.0)
				{
					mUpdateTimer.cancel();
					finish();
		
				}
			}
		}, 
		0,5); 
	    
	    

	}

	private void CheckBounds(float px, float py)
	{
		if(  ( Math.abs( targetX - px) +  Math.abs( targetY - py)) > 300 ) 
		{
			float tmpX = (float)(2*( targetX - px));
			while (Math.abs(tmpX) > 2*150)
				tmpX /=  1.1;
			float tmpY = (float)(2*( targetY - py));			
			while (Math.abs(tmpY) > 2*150)
				tmpY /= 1.1;
			

			targetX -= tmpX;
			targetY -= tmpY;
		}
		
	}
	
	public void onFingerMoveEvent(double vx, double vy, double px, double py)
	{
		currentX = (float) px;
		currentY = (float) py;
		
		
		if( (Math.abs(currentY - targetY) < 170.0f )  && (Math.abs(currentX - targetX) < 170.0f ))
		{
			int tmp = (int)Math.abs(currentX - targetX);
			if( UtilFunctions.mod(tmp,2) == 1)
				mTpad.sendVibration(TPadVibration.SAWTOOTH,120,1.0f);
			else
				mTpad.sendVibration(TPadVibration.SAWTOOTH,200,1.0f);
		}
		else
			mTpad.turnOff();
		
		 
	}
	public void onFingerUpEvent(double vx, double vy, double px, double py) 
	{
		if(mUpdateTimer != null)
			mUpdateTimer.cancel();
		if(mTpad != null)
		{
			 mTpad.sendFriction(0.0f); 
			 mTpad.turnOff();
		}
	}

	
	
	
	@Override
	protected void onResume() 
	{
		super.onResume();
	    mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
	    mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_UI);	
	    
	    mTpad = new TPadImpl(this);	
	    
	    mSharedTargetAngle = getSharedPreferences("SharedAngleFile", 0);
	    mTargetAngle = mSharedTargetAngle.getFloat("SharedAngle",(float)mTargetAngle);
	}
	
	@Override
	protected void onPause() 
	{
		super.onPause();
		
	    if(mUpdateTimer != null)
	    	mUpdateTimer.cancel();
		if(mSensorManager != null)
			mSensorManager.unregisterListener(this);
		if(mTpad != null)
			mTpad.turnOff();
	}
	
	
	@Override
	protected void onDestroy() 
	{
		if(mTpad != null)
			mTpad.turnOff();
	    if(mUpdateTimer != null)
	    	mUpdateTimer.cancel();
		if(mSensorManager != null)
			mSensorManager.unregisterListener(this);
		super.onDestroy();
	}
	
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		getMenuInflater().inflate(R.menu.map_haptic, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			return true;
		}
		if (id == android.R.id.home)
		{
			super.onBackPressed();
            return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
