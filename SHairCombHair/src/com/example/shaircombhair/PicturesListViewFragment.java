package com.example.shaircombhair;

import android.app.Fragment;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

//hier wird das listview fragment definiert?? 
public class PicturesListViewFragment extends ListFragment{

	private static final int LOAD_MORE_ITEMS_REQUEST = 0; //konstante fuer aufruf der LoadMorePictures activity

	public PicturesListViewFragment(){
	}

	PicturesListViewAdapter mAdapter;
	
	  @Override
	  public void onActivityCreated(Bundle savedInstanceState) {
	    super.onActivityCreated(savedInstanceState);
	    
	    //create adapter
	  //hier fehler:  mAdapter = new PicturesListViewAdapter(getApplicationContext());
	    
	 // Put divider between pictures and FooterView
	 //   getListView().setFooterDividersEnabled(true);
	    
	 // Inflate footerView for footer_view.xml file
	 //	LayoutInflater inflater = LayoutInflater.from(getActivity()); //from richtig??
	 //	TextView footerView = (TextView) inflater.inflate(R.layout.footer_view, null);
	    
		// Add footerView to ListView
	//	getListView().addFooterView(footerView);
		
		// Attach Listener to FooterView
	//			footerView.setOnClickListener(new OnClickListener() {
	//				@Override
	//				public void onClick(View v) {
						// Implement OnClick().
	//					Intent loadMorePicturesIntent = new Intent(PicturesListViewFragment.this, LoadMorePictures.class);
	//					startActivityForResult(loadMorePicturesIntent, LOAD_MORE_ITEMS_REQUEST);
	//				}
	//			});
		
		getListView().setAdapter(mAdapter);
	  }

	  //hier muessen dann irgendwie mehr bilder geladen werden
	//  @Override
	//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

	//		Log.i(TAG,"Entered onActivityResult()");

			// Check result code and request code
			// if user submitted a new ToDoItem
			// Create a new ToDoItem from the data Intent
			// and then add it to the adapter
	//		if(resultCode == RESULT_OK && requestCode == LOAD_MORE_ITEMS_REQUEST){
	//			ToDoItem item = new ToDoItem(data);
	//			mAdapter.add(item);
	//		}
			//mAdapter.add();
	//	}
	  
	//  @Override
	//  public void onListItemClick(ListView l, View v, int position, long id) {
	    // TODO implement some logic 
	//  }
	
	
}


//hieraus muss ein listview entstehen