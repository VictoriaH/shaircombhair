package com.example.shaircombhair;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class Database {
	static final String file_name_friseur = "friseur_database.txt";
	static final String file_name_numberOfHaircutters = "numberOfHaircutters.txt";
	static final String file_name_numberOfSelfies = "numberOfSelfies.txt";
	static final String file_name_selfie = "selfie_database.txt";
	public int numberOfHaircutters;
	public int numberOfSelfies;
	private String mPath;
	public String mRawData_friseur;
	public String mRawData_selfie;
	public Database(){}
	
	public Database(String file_path){
		mPath = file_path;
	 }
	
	// Load content from txt file and return a string.
public void loadFromFile_friseur() { //Haircutters
	try {
		
        File f = new File(mPath + "/" + file_name_friseur);
        FileInputStream is = new FileInputStream(f);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        mRawData_friseur = new String(buffer);
        
        f = new File(mPath + "/" + file_name_numberOfHaircutters);
        is = new FileInputStream(f);
        size = is.available();
        buffer = new byte[size];
        is.read(buffer);
        is.close();
        numberOfHaircutters = Integer.parseInt(new String(buffer));
        
        //Log.i("db","read from text " + mRawData);
    } catch (IOException e) {
    	Log.i("db","File does not exist or reading not successful!");
        e.printStackTrace();
    }
}

public void loadFromFile_selfie() {
	try {
		
        File f = new File(mPath + "/" + file_name_selfie);
        FileInputStream is = new FileInputStream(f);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        mRawData_selfie = new String(buffer);
        
        f = new File(mPath + "/" + file_name_numberOfSelfies);
        is = new FileInputStream(f);
        size = is.available();
        buffer = new byte[size];
        is.read(buffer);
        is.close();
        numberOfSelfies = Integer.parseInt(new String(buffer));
        
        //Log.i("db","read from text " + mRawData);
    } catch (IOException e) {
    	Log.i("db","File does not exist or reading not successful!");
        e.printStackTrace();
    }
}

public int getNumHaircutters(){
	loadFromFile_friseur();
	return numberOfHaircutters;
}

public int getNumSelfies(){
	loadFromFile_selfie();
	return numberOfSelfies;
}


	// get Friseur Information with index id from txt file
	
public HaircutterInformation getFriseur(int id) throws JSONException{
	 loadFromFile_friseur();
	 JSONObject jsonObj = new JSONObject(mRawData_friseur);

	 HaircutterInformation haircutterInfo = new HaircutterInformation();


     // Getting data JSON Array nodes
     JSONArray dataArray  = jsonObj.getJSONArray("haircutters");
     JSONObject c = dataArray.getJSONObject(id);

     try {
    	 haircutterInfo.id = Integer.parseInt(c.getString("id"));
    	} 
     catch(NumberFormatException nfe){
    	 return null;
     }
     haircutterInfo.name =  c.getString("name");
     haircutterInfo.mPictureLocation = c.getString("mPictureLocation");
     haircutterInfo.rating = Float.parseFloat(c.getString("rating"));
     haircutterInfo.adress = c.getString("adress");
     haircutterInfo.openingHours = c.getString("openingHours");
     haircutterInfo.longitude = Float.parseFloat(c.getString("longitude"));
     haircutterInfo.latitude = Float.parseFloat(c.getString("latitude"));
     
     //TODO: clear the mRawData and array
     mRawData_friseur=null;
     dataArray=null;
     return haircutterInfo;

}

public SelfieInformation getSelfie(int id) throws JSONException{
	 loadFromFile_selfie();
	 JSONObject jsonObj = new JSONObject(mRawData_selfie);

	 SelfieInformation selfieinfo = new SelfieInformation();


    // Getting data JSON Array nodes
    JSONArray dataArray  = jsonObj.getJSONArray("selfies");
    JSONObject c = dataArray.getJSONObject(id);
//    Log.i("db", "0");
    try {
    	selfieinfo.selfieID = Integer.parseInt(c.getString("id"));
   	} 
    catch(NumberFormatException nfe){
   	 return null;
    }
//    Log.i("db", "1");
    selfieinfo.userName =  c.getString("userName");
//    Log.i("db", "2");
    selfieinfo.userID =  Integer.parseInt(c.getString("userID")); 
//    Log.i("db", "3");
    selfieinfo.mPictureLocation = c.getString("mPictureLocation");
//    Log.i("db", "4");
    selfieinfo.mPictureLocation2 = c.getString("mPictureLocation2");
//    Log.i("db", "5");
    selfieinfo.mRating = Float.parseFloat(c.getString("rating"));
//    Log.i("db", "6");
    selfieinfo.mNumberOfRatings = Integer.parseInt(c.getString("mNumberOfRatings"));
//    Log.i("db", "7");
    selfieinfo.shotAtHaircutterID = Integer.parseInt(c.getString("shotAtHaircutterID"));
//    Log.i("db", "8");
    //selfieinfo.mDate = Date.parse(c.getString("mDate"));
    	
    //TODO: clear the mRawData and array
    mRawData_selfie=null;
    dataArray=null;
    Log.i("db", "return selfie Location (Database): " + selfieinfo.mPictureLocation + " ID: " + selfieinfo.selfieID);
    return selfieinfo;

}


public void addHaircutter2db(HaircutterInformation newHaircutterInfo) throws JSONException, FileNotFoundException{
	loadFromFile_friseur();
	JSONObject jsonObj = new JSONObject(mRawData_friseur);
	JSONArray dataArray  = jsonObj.getJSONArray("haircutters");
	int numberOfHaircutters = dataArray.length();// id of haircutters starts with 0. 
	int newId = numberOfHaircutters; //So numberOfHaircutters will be the id of the new newHaircutterInfo.
	JSONObject newJson = new JSONObject();
	
	newJson.put("id", Integer.toString(newId));
	newJson.put("name", newHaircutterInfo.name);
	newJson.put("mPictureLocation",newHaircutterInfo.mPictureLocation);
	newJson.put("rating",Float.toString(newHaircutterInfo.rating));
	newJson.put("adress", newHaircutterInfo.adress);
	newJson.put("openingHours", newHaircutterInfo.openingHours);
	newJson.put("longitude",Float.toString(newHaircutterInfo.longitude));
	newJson.put("latitude",Float.toString(newHaircutterInfo.latitude));

	dataArray.put(newJson);
	Log.i("db",dataArray.length() + " " + newJson.getString("id"));
		
	JSONObject haircutterObj = new JSONObject();
	Log.i("db","1");
	haircutterObj.put("haircutters", dataArray);
	Log.i("db","2");
	String mJsonResponse = haircutterObj.toString();
	Log.i("db","3");
	clearContenctOfTxt_friseur();
	Log.i("db","4");
	writeJSON2txt_friseur(mJsonResponse);
	Log.i("db","5");
	
	numberOfHaircutters++;
	writeNumberOfHaircutters(numberOfHaircutters);
}

public void addSelfie2db(SelfieInformation newSelfieInfo) throws JSONException, FileNotFoundException{
	loadFromFile_selfie();
	JSONObject jsonObj = new JSONObject(mRawData_selfie);
	JSONArray dataArray  = jsonObj.getJSONArray("selfies");
	int numberOfSelfies =   dataArray.length();// id of haircutters starts with 0. 
	int newId = numberOfSelfies; //So numberOfHaircutters will be the id of the new newHaircutterInfo.
	JSONObject newJson = new JSONObject();
	
	newJson.put("id", Integer.toString(newId));
	newJson.put("userName", newSelfieInfo.userName);
	newJson.put("userID", Integer.toString(newSelfieInfo.userID));
	newJson.put("mPictureLocation",newSelfieInfo.mPictureLocation);
	newJson.put("mPictureLocation2",newSelfieInfo.mPictureLocation2);
	newJson.put("rating",Float.toString(newSelfieInfo.mRating));
	newJson.put("mNumberOfRatings", Integer.toString(newSelfieInfo.mNumberOfRatings));
	newJson.put("shotAtHaircutterID", Integer.toString(newSelfieInfo.shotAtHaircutterID));
	newJson.put("mDate","2016-01-29 12:35:05");
	
	dataArray.put(newJson);
	Log.i("db",dataArray.length() + " " + newJson.getString("id"));
		
	JSONObject selfieObj = new JSONObject();
	Log.i("db","1");
	selfieObj.put("selfies", dataArray);
	Log.i("db","2");
	String mJsonResponse = selfieObj.toString();
	Log.i("db","3");
	clearContenctOfTxt_selfie();
	Log.i("db","4");
	writeJSON2txt_selfie(mJsonResponse);
	Log.i("db","5");
	
	numberOfSelfies++;
	writeNumberOfSelfies(numberOfSelfies);
}

/** Change the information of newHaircutterInfo. Id will not be changed. */

public void updateHaircutterInfo(HaircutterInformation newHaircutterInfo) throws JSONException, FileNotFoundException{
	loadFromFile_friseur();
	JSONObject jsonObj = new JSONObject(mRawData_friseur);
	JSONArray dataArray  = jsonObj.getJSONArray("haircutters");
	
	int id = newHaircutterInfo.id;
	JSONObject curHaircutter =  dataArray.getJSONObject(id);
	curHaircutter.put("name",newHaircutterInfo.name);
	curHaircutter.put("mPictureLocation",newHaircutterInfo.mPictureLocation);
	curHaircutter.put("rating",Float.toString(newHaircutterInfo.rating));
	curHaircutter.put("adress", newHaircutterInfo.adress);
	curHaircutter.put("openingHours", newHaircutterInfo.openingHours);
	curHaircutter.put("longitude",Float.toString(newHaircutterInfo.longitude));
	curHaircutter.put("latitude",Float.toString(newHaircutterInfo.latitude));

	String mJsonResponse = jsonObj.toString();
	clearContenctOfTxt_friseur();
	writeJSON2txt_friseur(mJsonResponse);
}

/** Change the information of newSelfie. Id will not be changed. */

public void updateSelfieInfo(SelfieInformation newSelfieInfo) throws JSONException, FileNotFoundException{
	loadFromFile_selfie();
	JSONObject jsonObj = new JSONObject(mRawData_selfie);
	JSONArray dataArray  = jsonObj.getJSONArray("selfies");
	
	int id = newSelfieInfo.selfieID;
	JSONObject curSelfie =  dataArray.getJSONObject(id);
	curSelfie.put("userName",newSelfieInfo.userName);
	curSelfie.put("userID",Integer.toString(newSelfieInfo.userID));
	curSelfie.put("mPictureLocation",newSelfieInfo.mPictureLocation);
	curSelfie.put("mPictureLocation2",newSelfieInfo.mPictureLocation2);
	curSelfie.put("rating",Float.toString(newSelfieInfo.mRating));
	curSelfie.put("mNumberOfRatings", Integer.toString(newSelfieInfo.mNumberOfRatings));
	curSelfie.put("shotAtHaircutterID", Integer.toString(newSelfieInfo.shotAtHaircutterID));
	//curSelfie.put("mDate","2016-01-29 12:35:05");
	
	String mJsonResponse = jsonObj.toString();
	clearContenctOfTxt_selfie();
	writeJSON2txt_selfie(mJsonResponse);
}

public void clearContenctOfTxt_friseur() throws FileNotFoundException{
	PrintWriter pw = new PrintWriter(mPath + "/" + file_name_friseur);
	pw.close();
}

public void clearContenctOfTxt_selfie() throws FileNotFoundException{
	PrintWriter pw = new PrintWriter(mPath + "/" + file_name_selfie);
	pw.close();
}

public void writeJSON2txt_friseur(String mJsonResponse) {
    try {

        FileWriter fw = new FileWriter(mPath + "/" + file_name_friseur,true);
        Log.i("database","in create database "+ mPath + "/"+ file_name_friseur);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(mJsonResponse);
        bw.close();
        fw.close();

    } catch (IOException e) {
        Log.e("TAG", "Error in Writing: " + e.getLocalizedMessage());
    }
}

public void writeJSON2txt_selfie(String mJsonResponse) {
    try {

        FileWriter fw = new FileWriter(mPath + "/" + file_name_selfie,true);
        Log.i("database","in create database "+ mPath + "/"+ file_name_selfie);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(mJsonResponse);
        bw.close();
        fw.close();

    } catch (IOException e) {
        Log.e("TAG", "Error in Writing: " + e.getLocalizedMessage());
    }
}

public void writeNumberOfHaircutters(int number){
	try {

        FileWriter fw = new FileWriter(mPath + "/" + file_name_numberOfHaircutters,false);
        Log.i("database","in create database "+ mPath + "/"+ file_name_numberOfHaircutters);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(String.valueOf(number));
        bw.close();
        fw.close();

    } catch (IOException e) {
        Log.e("TAG", "Error in Writing: " + e.getLocalizedMessage());
    }
}

public void writeNumberOfSelfies(int number){
	try {

        FileWriter fw = new FileWriter(mPath + "/" + file_name_numberOfSelfies,false);
        Log.i("database","in create database "+ mPath + "/"+ file_name_numberOfSelfies);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(String.valueOf(number));
        bw.close();
        fw.close();

    } catch (IOException e) {
        Log.e("TAG", "Error in Writing: " + e.getLocalizedMessage());
    }
}
}