package com.example.shaircombhair;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

import com.example.shaircombhair.MainActivity.ListType;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

public class FriseurPage extends Activity {

	static private final int CameraRequestCode = 1; //Requestcode
	ImageView PictureOfFriseur;
	RatingBar RatingOfFriseur;
	TextView NameOfFriseur;
	TextView AdressOfFriseur;
	TextView OpeningHoursOfFriseur;
	Button NavigateToFriseur;
	Button ShowPictures;
	ListView CommentsListView;
	int haircutterID;
	ListPicture mListPicture;
	DatabaseAdapter mDatabaseAdapter;
	File mPath;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		mPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friseur_page);
		
		mDatabaseAdapter = new DatabaseAdapter(); //Database Adapter setzen		
		
		Intent argumentsFromPicture = getIntent();
        if(argumentsFromPicture != null)
        {
        	haircutterID = argumentsFromPicture.getIntExtra("HaircutterID", 0);
        	mListPicture = mDatabaseAdapter.getListPictureFromHaircutterID(haircutterID);
        	Log.i("TAG","Got listpicture from bundle haircutterid: " + haircutterID);
        	
        }
		
		//views setzen
		PictureOfFriseur = (ImageView) findViewById(R.id.fiseurPicture);
		RatingOfFriseur = (RatingBar) findViewById(R.id.friseurRating);
		NameOfFriseur = (TextView) findViewById(R.id.friseurName);
		AdressOfFriseur = (TextView) findViewById(R.id.friseurAdress);
		OpeningHoursOfFriseur = (TextView) findViewById(R.id.friseurOpeningHours);
		NavigateToFriseur = (Button) findViewById(R.id.naviateToFriseur);
		CommentsListView = (ListView) findViewById(R.id.commentsListView);
		ShowPictures = (Button) findViewById(R.id.showPicturesFromFriseur);


		
		
	// Display Bitmap aus mListPicture
		String imagePath = "/sdcard/" + mListPicture.getImageLocation();
		File imgFile = new  File(imagePath);
		
		if(imgFile.exists()){
		    Bitmap MyBitMap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
		    PictureOfFriseur.setImageBitmap(MyBitMap);
		    //Log.i("TAG","PictureOfFriseur set");
		}else{
			Log.i("TAG","Picture not found");
			Log.i("TAG",imagePath);
		}
		
		mListPicture.refreshHaircutterRating();
		//Log.i("db_rating","rating: " + mListPicture.getHaircutterRating());
		RatingOfFriseur.setRating(mListPicture.getHaircutterRating());
		//Log.i("TAG","Rating Set");
		NameOfFriseur.setText(mListPicture.getHaircutterName());
		//Log.i("TAG","NameOfFriseur Set");
		AdressOfFriseur.setText(mListPicture.getHaircutterAdress());
		OpeningHoursOfFriseur.setText(mListPicture.getHaircutterOpeningHours());
		
		
		
		NavigateToFriseur.setOnClickListener(new View.OnClickListener() 
		{
		    @Override
		    public void onClick(View v) 
		    {
		       /* Intent NavigateToFriseurPageIntent = new Intent(FriseurPage.this,Map.class);
		        NavigateToFriseurPageIntent.putExtra("HaircutterID", haircutterID);
		        startActivity(NavigateToFriseurPageIntent);
		        */
		        
		        
		        
		        
				Intent MapIntent = new Intent(FriseurPage.this,Map.class);
				
			    List<String> stylistLocations;
			    List<String> stylistNames;
			    List<Float> stylistRatings;

			    
			    // TODO  get these from database!! 
			    stylistLocations 	= new ArrayList<String>();
			    stylistNames 		= new ArrayList<String>();
			    stylistRatings       = new ArrayList<Float>();


				
//			    
//			    stylistLocations.add( "48.151057, 11.563882");
//			    stylistNames.add("SHAG");
//			    stylistLocations.add("48.151665, 11.573464");
//			    stylistNames.add("Patis");
//			    stylistLocations.add( "48.181646, 11.521441");
//			    stylistNames.add("Black VELVET");
//	            stylistLocations.add( "48.080862, 11.526486");
//	            stylistNames.add("Solln");
//	            stylistLocations.add( "48.154464, 11.636663");
//	            stylistNames.add("Salon Annegret");
//	            stylistLocations.add( "48.154464, 11.636663");
//	            stylistNames.add("Kurz");
//	            stylistLocations.add( "48.132125, 11.690450");
//	            stylistNames.add("Crew ");
//	            stylistLocations.add( "48.105049, 11.769981");
//	            stylistNames.add("am Rathaus");
//	            stylistLocations.add( "48.141693, 11.585738");
//	            stylistNames.add("Klaus Sch�tze Frise ");
//	            stylistLocations.add( "48.130809, 11.574337");
//	            stylistNames.add("Sperl");
//	            
	//
//			      int numberOfStylists = stylistNames.size();
//			      
		    		Database db = new Database(mPath.getPath());
		    		int numberOfStylists = db.getNumHaircutters();
		    		HaircutterInformation curHaircutterInfo = new HaircutterInformation();
		    		String loc;
		    		for(int i = 0;i<numberOfStylists;i++){
			    		try {
			    			curHaircutterInfo = db.getFriseur(i);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			    		loc = String.valueOf(curHaircutterInfo.longitude) + ", " + String.valueOf(curHaircutterInfo.latitude);
			    		stylistLocations.add(loc);
			    		stylistNames.add(curHaircutterInfo.name);
			    		stylistRatings.add(curHaircutterInfo.rating);

		    		}			      
				      
			      
			      MapIntent.putExtra("NumberOfStylists",numberOfStylists);
			      MapIntent.putExtra("SelectedHairCutter",haircutterID);
			      for(int i = 0; i < numberOfStylists; i++)
			      {
			      
			    	  MapIntent.putExtra("stylistNames"+String.valueOf(i), stylistNames.get(i));
			    	  MapIntent.putExtra("LatLng"+String.valueOf(i), stylistLocations.get(i));
			    	  MapIntent.putExtra("stylistRatings"+String.valueOf(i),stylistRatings.get(i));
			    	  Log.i("MAP","MainActivity sends: " + stylistLocations.get(i));
			    	  Log.i("MAP","MainActivity sends rating: " + stylistRatings.get(i));
			      }
			      
				
				startActivity(MapIntent);
				//Toast.makeText(this, "Karte", Toast.LENGTH_SHORT).show();
		        
		        
		        
		        
		        
		        
		        
		        
		    }
		});
		ShowPictures.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
		        Intent ShowPicturesIntent = new Intent(FriseurPage.this,MainActivity.class);
		        ShowPicturesIntent.putExtra("category", ListType.custom);
		        ShowPicturesIntent.putExtra("HaircutterID", mListPicture.getHaircutterID());
		        ShowPicturesIntent.putExtra("UserOrHaircutter", 1);
		        startActivity(ShowPicturesIntent);
		    }
		});
		//Log.i("TAG","Button Set");
		
		
		//TODO: comments setzen
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.friseur_page_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			Toast.makeText(this, "Einstellungen", Toast.LENGTH_SHORT).show();
			return true;
		}
		if (id == R.id.my_pictures) 
		{
			Intent ShowPicturesIntent = new Intent(FriseurPage.this,MainActivity.class);
	        ShowPicturesIntent.putExtra("category", ListType.custom);
	        ShowPicturesIntent.putExtra("UserID", GlobalConstants.currentUserID);
	        ShowPicturesIntent.putExtra("UserOrHaircutter", 0);

	        startActivity(ShowPicturesIntent);
	        return true;
		}
		if (id == R.id.map) 
		{
			Intent MapIntent = new Intent(FriseurPage.this,Map.class);
			
		    List<String> stylistLocations;
		    List<String> stylistNames;
		    List<Float> stylistRatings;

		    
		    // TODO  get these from database!! 
		    stylistLocations 	= new ArrayList<String>();
		    stylistNames 		= new ArrayList<String>();
		    stylistRatings       = new ArrayList<Float>();


			
//		    
//		    stylistLocations.add( "48.151057, 11.563882");
//		    stylistNames.add("SHAG");
//		    stylistLocations.add("48.151665, 11.573464");
//		    stylistNames.add("Patis");
//		    stylistLocations.add( "48.181646, 11.521441");
//		    stylistNames.add("Black VELVET");
//            stylistLocations.add( "48.080862, 11.526486");
//            stylistNames.add("Solln");
//            stylistLocations.add( "48.154464, 11.636663");
//            stylistNames.add("Salon Annegret");
//            stylistLocations.add( "48.154464, 11.636663");
//            stylistNames.add("Kurz");
//            stylistLocations.add( "48.132125, 11.690450");
//            stylistNames.add("Crew ");
//            stylistLocations.add( "48.105049, 11.769981");
//            stylistNames.add("am Rathaus");
//            stylistLocations.add( "48.141693, 11.585738");
//            stylistNames.add("Klaus Sch�tze Frise ");
//            stylistLocations.add( "48.130809, 11.574337");
//            stylistNames.add("Sperl");
//            
//
//		      int numberOfStylists = stylistNames.size();
//		      
	    		Database db = new Database(mPath.getPath());
	    		int numberOfStylists = db.getNumHaircutters();
	    		HaircutterInformation curHaircutterInfo = new HaircutterInformation();
	    		String loc;
	    		for(int i = 0;i<numberOfStylists;i++){
		    		try {
		    			curHaircutterInfo = db.getFriseur(i);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    		loc = String.valueOf(curHaircutterInfo.longitude) + ", " + String.valueOf(curHaircutterInfo.latitude);
		    		stylistLocations.add(loc);
		    		stylistNames.add(curHaircutterInfo.name);
		    		stylistRatings.add(curHaircutterInfo.rating);

	    		}			      
			      
		      
		      MapIntent.putExtra("NumberOfStylists",numberOfStylists);
		      for(int i = 0; i < numberOfStylists; i++)
		      {
		    	  MapIntent.putExtra("stylistNames"+String.valueOf(i), stylistNames.get(i));
		    	  MapIntent.putExtra("LatLng"+String.valueOf(i), stylistLocations.get(i));
		    	  MapIntent.putExtra("stylistRatings"+String.valueOf(i),stylistRatings.get(i));
		    	  Log.i("MAP","MainActivity sends: " + stylistLocations.get(i));
		    	  Log.i("MAP","MainActivity sends rating: " + stylistRatings.get(i));
		      }
		      
			
			startActivity(MapIntent);
			//Toast.makeText(this, "Karte", Toast.LENGTH_SHORT).show();
			return true;
		}
		if (id == R.id.search) 
		{
			Intent SearchIntent = new Intent(FriseurPage.this,Search.class);
			startActivity(SearchIntent);
			//Toast.makeText(this, "Kamera", Toast.LENGTH_SHORT).show();
			return true;
		}
//		if (id == R.id.cam) 
//		{
//			Intent StartCameraIntent = new Intent(FriseurPage.this,CameraActivity.class);
//			startActivityForResult(StartCameraIntent, CameraRequestCode);
//			//Toast.makeText(this, "Kamera", Toast.LENGTH_SHORT).show();
//			return true;
//		}
		if (id == R.id.pictures) 
		{
			Intent StartMainIntent = new Intent(FriseurPage.this,MainActivity.class);
			StartMainIntent.putExtra("category", ListType.bestselfie);
			startActivity(StartMainIntent);
			return true;
		}
		if (id == R.id.haircutters) 
		{
			Intent StartMainIntent = new Intent(FriseurPage.this,MainActivity.class);
			StartMainIntent.putExtra("category", ListType.besthaircutter);
			startActivity(StartMainIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
