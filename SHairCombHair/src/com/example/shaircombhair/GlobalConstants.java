package com.example.shaircombhair;

import java.util.ArrayList;

import nxr.tpad.lib.TPadService;

public class GlobalConstants
{
	
	public static final String home 								= "/sdcard/";//Environment.getExternalStorageDirectory().getAbsolutePath();
	public static final String folder 								= home + "/HapticData/"; //"/sdcard/HapticData/"; 
	public static final String folderMacroImage 					= home + "HapticDataNew/";
	public static final String folderDisplayImage 					= home + "ImageDataNew/";
	public static final String folderSoundData 						= home + "SoundDataNew/";
	public static final String bluetoothPath 						= home + "/bluetooth/";
//	public static final String ROUGHNESS_PREFIX 					= "macro";
	public static final String ROUGHNESS_PREFIX 					= "";
	public static final String SOUND_SUFFIX 						= "_soundtpad.wav";
	public static final String SOUND_SUFFIX_IMPACT					= "_impact.wav";
	public static final String HAPTIC_IMAGE_SUFFIX					= ".jpg";	
	
	public static final int PREDICT_HORIZON 	= (int) (TPadService.OUTPUT_SAMPLE_RATE * (.020f)); // 125 samples, 20ms @ sample rate output	

	public static final int HAPTIC_FREQUENCY 						= 38780;//hz
	public static final int DIRECT 									= 1;
	public static final int GRADIENT 								= 2;
	public static final double DISTORTION_K							= -0.000006;//-0.00001
	public static final int NEIGHBORHOOD_SIZE_TO_DISTORT 			= 250;//80;
	public static final long TIMEOUT_LAST_TOUCH_MS 					= 30;// 40
	public static final int NEIGHBORHOOD_SIZE_UNCHANGED_DISTORT 	= 5;//35;


	public static final int DEFAULT_IMAGE_WIDTH 					= 2000;
	public static final int DEFAULT_IMAGE_HEIGHT					= 3800;	
	
	
    final static float PI = (float) Math.PI;
    final static float RAD2DEG = 180/PI;   
	
    public static int currentUserID = 999;
    public static String currentUserName = "Ich";
	
    public static ArrayList<Boolean> rated = new ArrayList<Boolean>();
    public static ArrayList<Boolean> ratedSorted = new ArrayList<Boolean>();
    public static final String STATE_RATINGS = "ratedPicturesArray";
}
