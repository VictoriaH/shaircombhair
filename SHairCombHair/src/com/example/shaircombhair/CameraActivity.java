package com.example.shaircombhair;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.example.shaircombhair.MainActivity.ListType;
import com.example.shaircombhair.R.id;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.model.LatLng;

import nxr.tpad.lib.TPad;
import nxr.tpad.lib.TPadImpl;
import nxr.tpad.lib.TPadService;
import nxr.tpad.lib.consts.TPadVibration;
import android.app.Activity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.Camera.CameraInfo;
import android.hardware.Camera.PictureCallback;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class CameraActivity extends Activity implements SensorEventListener
{
	
	private static final String TAG = CameraActivity.class.getSimpleName();
	
	private Button 				mButtonCamera;
	private Button 				mButtonOkCamera;
	private Button 				mButtonCancelCamera;
	
	private Vibrator 			mVibrator;
	

	private int 				mDefaultCameraId;
	
	private CameraPreview mPreview;
	private FrameLayout mPreviewFrame;
	private Camera mCamera;
	private boolean mFirstPictureTaken 				= false;
	private boolean mSecondPictureTaken 			= false;
	private String mPictureName;
	
	//private boolean mDataReady 						= false;	
	private String mGapFiller;
	
	private int mHeight;
	private int mWidth;


	

    
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mMagnetometer;  

    private boolean mHasGyroscope;
    private Sensor mRotationSensor;
    
    float[] mGravity;
    float[] mGeomagnetic; 
	private float[] mValuesMagnet      = new float[3];
	private float[] mValuesAccel       = new float[3];
	float mCurrentAcceleration;

	private float[] mValuesOrientation = new float[3];
	
	
	private GoogleMap googleMap;

	
	
    float mRotationMatrix[] = new float[16];
    float[] mRotationVectorValues = new float[3];	
	
	
	
    private float mTargetAngle; 
    private float mStartAngle;
    private float mTargetPitch;
    private float mCurrentPitch;
    private float mTargetRoll;
    private float mCurrentRoll;
    private float mTargetAzimuth;
    private float mCurrentAzimuth;    
    
    
	
	// Android velocity tracking object, used in predicting finger position
	public VelocityTracker vTracker 						= null;
	// Velocity and position variables of the finger. Get updated at approx 60Hz when the user is touching	
	private static float vy 								= 0.0f;
	private static float vx 								= 0.0f;
	private static float px 								= 0.0f;
	private static float py 								= 0.0f;
	private double mCurrentVelocity 						= -1.0f;	


	Timer mUpdateTimer;
	
	
	FrameLayout mFrameLayout;	
	int mRadius;
	Paint mPaint;
	ImageView mView;
	Canvas mCanvas;
	Bitmap mBitmap;

	
	private float mOffsetDrawPositionX = 800;
	private float mOffsetDrawPositionY = 280;
	
	private float mCenterDrawPositionX = 380;
	private float mCenterDrawPositionY = 170;	

	private TPad mTpad 							= null;		

	
    public List<LatLng> stylistLocations;
    public List<String> stylistNames;
	
	
	/**
	 * 
	 * Output
	 * 
	 * 
	 */
	String mSelfieFrontPath;
	String mSelfieSidePath;
    LatLng mCurrentPosition;
	String mClosestStylist;
    int numberOfHairstylists;
    int matchedStylistId;
	
	
	
	
	
	public void onAccuracyChanged(Sensor sensor, int accuracy) {  }
	public void onSensorChanged(SensorEvent event) 
	{	
		
		if( event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR)
		{
	        // Convert the rotation-vector to a 4x4 matrix.
			 SensorManager.getRotationMatrixFromVector(mRotationMatrix , event.values); 
	        SensorManager.remapCoordinateSystem(mRotationMatrix,SensorManager.AXIS_X, SensorManager.AXIS_Z, mRotationMatrix);    
			SensorManager.getOrientation(mRotationMatrix, mRotationVectorValues);
	        mRotationVectorValues[0] = (float) Math.toDegrees(mRotationVectorValues[0]);
	        mRotationVectorValues[1] = (float) Math.toDegrees(mRotationVectorValues[1]);
	        mRotationVectorValues[2] = (float) Math.toDegrees(mRotationVectorValues[2]);
	        if(mRotationVectorValues[0] < 0)
	        	 mRotationVectorValues[0] += 360;
		}
		
		
		
	     if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
	     {
	    	 
	       mGravity = event.values;
	       double oneAxisAcceleration = Math.sqrt( Math.pow(event.values[0], 2) + Math.pow(event.values[1], 2) + Math.pow(event.values[2], 2) );
	       mCurrentAcceleration = (float) Math.abs(oneAxisAcceleration - SensorManager.STANDARD_GRAVITY);
	     }
	     if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
	    	 mGeomagnetic = event.values;
	     
	     if (mGravity != null && mGeomagnetic != null) 
	     {
	    	 float R[] = new float[9];
		     float I[] = new float[9];
		     boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
		       
		     if (success) 
		     {
		    	 float diff = 0;
		         float orientation[] = new float[3];
		         SensorManager.getOrientation(R, orientation);
		         mValuesOrientation[0] = orientation[0] * GlobalConstants.RAD2DEG; // orientation contains: azimut, pitch and roll
		         mValuesOrientation[1] = orientation[1] * GlobalConstants.RAD2DEG;
		         mValuesOrientation[2] = orientation[2] * GlobalConstants.RAD2DEG;

		         if(mValuesOrientation[0] < 0)
		        	 mValuesOrientation[0] += 360;
		     }
	     }
	}
	   
	
	
	
	
	


	
	
	
	@SuppressWarnings("deprecation")
	@Override
    protected void onCreate(Bundle savedInstanceState) 
	{
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);		
        getActionBar().setDisplayHomeAsUpEnabled(true);
        
		mRotationMatrix[ 0] = 1;
        mRotationMatrix[ 4] = 1;
        mRotationMatrix[ 8] = 1;
        mRotationMatrix[12] = 1;
        
        Log.i("CameraActivity","OnCreate");
        
        stylistLocations 	= new ArrayList<LatLng>();
        stylistNames 		= new ArrayList<String>();
        
        Bundle extras = getIntent().getExtras();

      
        if(extras !=null)
        {
      	  
      	  numberOfHairstylists= extras.getInt("NumberOfStylists");
      	  Log.i("CameraActivity","Number of received styilists:" + numberOfHairstylists);
      	  for(int i = 0; i < numberOfHairstylists; i++)
      	  {
      		  String name = extras.getString("stylistNames"+String.valueOf(i));
      		  
      		  stylistNames.add(name);
      		  
      		  LatLng tempLatLng;
      		  String tmp = extras.getString("LatLng"+String.valueOf(i));
      		  Log.i("CameraActivity","CameraActivity receives: " + tmp);
      		  String[] latlng = tmp.split(",");
      		  
      		  double latitude = Double.parseDouble(latlng[0]);
      		  double longitude = Double.parseDouble(latlng[1]);
      		  tempLatLng = new LatLng(latitude, longitude);
      		  stylistLocations.add(tempLatLng);
      	  }
      	  
        }
        else
        	Log.i("CameraActivity","Extras == null!!");




        
	    mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
	    mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
	    mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
	    mRotationSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
	    
	    mView = (ImageView)findViewById(R.id.imageView);
	    

        mRadius = 70;
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setColor(Color.WHITE);
        
        mBitmap = Bitmap.createBitmap(850, 330, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
        mCanvas.drawPaint(mPaint);
        // Use Color.parseColor to define HTML colors
        
        // Target Area   
        mPaint.setColor(Color.parseColor("#990000")); 
        mCanvas.drawCircle(425, 165, mRadius, mPaint);
        mPaint.setColor(Color.WHITE);
        mCanvas.drawCircle(425, 165, mRadius/2, mPaint);
        mPaint.setColor(Color.parseColor("#990000")); 
        mCanvas.drawCircle(425, 165, mRadius/4, mPaint);
        
        
        mPaint.setColor(Color.parseColor("#FF0000")); 
        mCanvas.drawCircle(100, 100, mRadius, mPaint);
        //canvas.drawCircle(x / 2, y / 2, radius, paint);
        
	    mView.setImageBitmap(mBitmap);
	    mView.invalidate();
	    mView.setOnTouchListener(new OnTouchListener()
	    {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) 
			{
				px = event.getX();
				py = event.getY();

				switch (event.getActionMasked()) 
				{
					case MotionEvent.ACTION_DOWN:
						vx = 0.0f;
						vy = 0.0f;
			
						if (vTracker == null) 
						{
							vTracker = VelocityTracker.obtain();
						} 
						else 
						{
							vTracker.clear();
						}

						vTracker.addMovement(event);
						
						mView.invalidate();
						mCanvas.drawColor(Color.WHITE);
						//mCanvas.drawCircle(mCenterDrawPositionX, mCenterDrawPositionY + mValuesOrientation[1], mRadius, mPaint);
						mCanvas.drawCircle(mCenterDrawPositionX, mCenterDrawPositionY, mRadius, mPaint);

						if(mHasGyroscope)
						{
							mStartAngle  = mRotationVectorValues[0];
							mTargetAngle = mStartAngle - 90;
						}
						else
						{
							mStartAngle  = mValuesOrientation[0];
							mTargetAngle = mStartAngle - 90;
						}
						if(mTargetAngle < 0)
							mTargetAngle += 360;
						
						if(mCamera == null)
						{
							mCamera = setupCamera();
						}
						
						
						if(!mFirstPictureTaken)
						{
							mPictureName = "FrontPicture";
			        		mCamera.takePicture(null, null, mPicture);
							try 
							{
								Thread.sleep(2000);
							} 
							catch (InterruptedException e) 
							{
								e.printStackTrace();
							}
							mCamera.startPreview();
							mFirstPictureTaken = true;
							
						}
						break;

					case MotionEvent.ACTION_MOVE:
			
						vTracker.addMovement(event);
						vTracker.computeCurrentVelocity(1);
						vx = vTracker.getXVelocity(); 
						vy = vTracker.getYVelocity(); 
			
						
						mCanvas.drawColor(Color.WHITE);
						
						
						
						if(mHasGyroscope)
						{
							
							//mCanvas.drawCircle(mCenterDrawPositionX + 2 * Math.abs(mTargetAngle - mRotationVectorValues[0]) ,    mCenterDrawPositionY, mRadius, mPaint);
							mCanvas.drawCircle(mCenterDrawPositionX + 3 * Math.abs(mTargetAngle - mRotationVectorValues[0]) ,    py, mRadius, mPaint);
							Log.i("ROTATE","Values.................."+ mRotationVectorValues[0]);
						}
						else
						{
							
							mCanvas.drawCircle(mCenterDrawPositionX + 3 * Math.abs(mTargetAngle - mValuesOrientation[0]) ,    py, mRadius, mPaint);
							
							if(  (Math.abs(mCenterDrawPositionX + 3 * Math.abs(mTargetAngle - mValuesOrientation[0]) - px) < 2*mRadius ) && (Math.abs(mCenterDrawPositionY - (py-100)) < 2*mRadius ) )
							{
								mTpad.sendVibration(TPadVibration.SAWTOOTH, 120, 1.0f);
							}
							else
							{
								if(mValuesOrientation[1] > -85.0)
								{
									if( py < 25 || py > 315)
									{
										float tmp = (90.0f+mValuesOrientation[1]) / 90.0f;
										if(tmp < 0.0) tmp = 0.0f;
										if(tmp > 1.0f) tmp = 1.0f;
										mTpad.sendVibration(TPadVibration.SAWTOOTH, 300, tmp  );
									}
									else
									{
										mTpad.turnOff();
									}
								}	
								
							}

							
						}
						
						
						

						
						mView.setImageBitmap(mBitmap);
						mView.invalidate();
			
						break;
			
					case MotionEvent.ACTION_UP:

						if(!mHasGyroscope)
							mTpad.turnOff();
						
						mPictureName = "SidePicture";
						


						
	        		    mUpdateTimer = new Timer("mUpdateTimer");
	        		    mUpdateTimer.scheduleAtFixedRate(new TimerTask()
	        		    {
	        		    	public void run()
	        				{
	        		    		if(mSecondPictureTaken)
	        		    		{
	        		    	    
	        		    			mUpdateTimer.cancel();
	        		    			
	        		    		}
	        		    		else
	        		    		{
	        					
		        					if(mHasGyroscope)
		        					{
		        						Log.i("ROTATION","Azimuth mit RotVec "   + mRotationVectorValues[0]);
		        					//Log.i("ROTATION"," Yaw: " + orientationVals[0] + "\n Pitch: "   + orientationVals[1] + "\n Roll (not used): "   + orientationVals[2]);
		        						mCurrentPitch   = mValuesOrientation[1];// - currentAngle;
		        						Log.i("TAG", "ANGLE...................." + mCurrentPitch);
		        						Log.i("TAG", "ACCEL...................." + mCurrentAcceleration);	
		        		
		        						
		        						
		        						
		        						if((mCurrentAcceleration) < 0.4f && (mCurrentPitch < -70))
		        						{
		        							if(!mSecondPictureTaken)
		        							{
		        								mSecondPictureTaken = true;
		        								
		        								if(mCamera != null)
		        								{
		        					        		mCamera.takePicture(null, null, mPicture);
		        									try 
		        									{
		        										Thread.sleep(3000);
		        									} 
		        									catch (InterruptedException e) 
		        									{
		        										e.printStackTrace();
		        									}
		        									mCamera.startPreview();
		        								}
		        								else	
		        								{
		        									mCamera = setupCamera();
		        								}
		        								
		        								try 
		        								{
													Thread.sleep(1000);
												} 
		        								catch (InterruptedException e) 
		        								{
													e.printStackTrace();
												}
		        								
		        								Log.i("CameraActivity","Second Picture Path: " + mSelfieSidePath);
		        							
		        								// TODO  finish activity and send result to mainactivity
		        								Intent returnIntent = new Intent();
		        								returnIntent.putExtra("FirstSelfie",mSelfieFrontPath);
		        								returnIntent.putExtra("SecondSelfie",mSelfieSidePath);
		        								returnIntent.putExtra("MyLocation",mCurrentPosition.toString());
		        								setResult(Activity.RESULT_OK,returnIntent);
		        								finish();
		        							}
		        						}
		        					}
		        					else
		        					{
		        						mCurrentAzimuth = mValuesOrientation[0];
		        						mCurrentPitch   = mValuesOrientation[1];// - currentAngle;
		        						mCurrentRoll    = mValuesOrientation[2];
		        						
		        						Log.i("TAG", "ANGLE...................." + mCurrentAzimuth);
		        						Log.i("TAG", "ACCEL...................." + mCurrentAcceleration);	
		        						
		
		        						
		        						if((mCurrentAcceleration) < 0.4f && (mCurrentPitch < -70))
		        						{
		        							if(!mSecondPictureTaken)
		        							{
		        								mSecondPictureTaken = true;
		        								
		        								mCamera.takePicture(null, null, mPicture);
		        								
		        								try 
		        								{
													Thread.sleep(1000);
												} 
		        								catch (InterruptedException e) 
		        								{
													e.printStackTrace();
												}
		        								
		        								Log.i("CameraActivity","Second Picture Path: " + mSelfieSidePath);
		        							
		        								// TODO  finish activity and send result to mainactivity
		        								Intent returnIntent = new Intent();
		        								returnIntent.putExtra("FirstSelfie",mSelfieFrontPath);
		        								returnIntent.putExtra("SecondSelfie",mSelfieSidePath);
		        								returnIntent.putExtra("MyLocation",mCurrentPosition.toString());
		        								
		        								String matchedStylist = matchCurrentLocationToStylist();
		        								returnIntent.putExtra("MatchedStylist",matchedStylist);
		        								returnIntent.putExtra("MatchedStylistId",matchedStylistId);
		        								Log.i("matchedStylist","Closest Stylist:  " + matchedStylist + " id: " + matchedStylistId);
		        								Log.i("CameraActivity","Closest Stylist:  " + matchedStylist + " id: " + matchedStylistId);
		        								
		        								setResult(Activity.RESULT_OK,returnIntent);
		        								finish();
		        							}
		        							
		        						}
		        					}
	        		    		}
	        				}

	        			}, 
	        			0,500); 
						
						
						
							
						
						
						mView.setImageBitmap(mBitmap);
						mView.invalidate();
						break;
				
					case MotionEvent.ACTION_CANCEL:
						vTracker.recycle();
						vTracker = null;
						break;
				}
	
				return true;				
			}
		});
	    
	    
	    googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map2)).getMap();
	    googleMap.setMyLocationEnabled(true); 
	    googleMap.setOnMyLocationChangeListener(new OnMyLocationChangeListener() 
        {
       	 @Override
       	    public void onMyLocationChange(Location location) 
       	 {
       	        LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
       	        mCurrentPosition = loc;
       	        /////currentPositionMarker = googleMap.addMarker(new MarkerOptions().position(loc));
       	        Log.i("Camera","CurrentPosition: " + mCurrentPosition.toString());
       	    }
		});
	}
		
	private String matchCurrentLocationToStylist() 
	{
		String match; 
		
		int index = 0;
		double[] result = new double[1];
		result[0] = 1000000.0f;
		for(int i = 0; i < numberOfHairstylists;i++)
		{
			double tmp = Math.sqrt(  Math.pow( (double)(mCurrentPosition.latitude - stylistLocations.get(i).latitude), 2) +  Math.pow( (double)(mCurrentPosition.longitude - stylistLocations.get(i).longitude), 2)  );
			if( tmp < result[0])
			{
				result[0] = tmp;
				index = i;
			}
			
		}
		
		match = stylistNames.get(index);
		
		matchedStylistId = index;
		
		
		return match;
	}

	
	// TODO still some OnResume bug: camera 
	
	@Override
	protected void onResume() 
	{
		super.onResume();
		if(mSensorManager != null)
		{
			mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
			mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_UI);	
			//mSensorManager.registerListener(this, mGyroscope, SensorManager.SENSOR_DELAY_FASTEST);
			mHasGyroscope = mSensorManager.registerListener(this, mRotationSensor, 10000);
			if(!mHasGyroscope)	
				Log.i("ROTATION","HINT: no gyroscope on device!!!!");
			else
				Log.e("ERROR","Found gyroscope on device");
		}
	    
		if(!mHasGyroscope)
			mTpad = new TPadImpl(this);
	    
		//if(!mDataReady) 
		//{
	        
		
		try
		{
			mCamera = setupCamera();
	        mPreview = new CameraPreview(this, mCamera);
	        mPreviewFrame = (FrameLayout) findViewById(R.id.framelayout_camera);
	        mPreviewFrame.addView(mPreview);
		}
		catch(Exception e)
		{
			Log.e("ERROR","OnResumeError!!!");
		}

	        
	        
	        mButtonCancelCamera = (Button) findViewById(R.id.button_cancel_camera);
	        mButtonCancelCamera.setOnClickListener(new OnClickListener() 
	        {
	        	
	        	@Override
	        	public void onClick(View v)
	        	{
					Intent returnIntent = new Intent();

					setResult(Activity.RESULT_CANCELED,returnIntent);
					finish();
	        	}
	        }); 
	        

		
		}
	//}

	@Override 
	protected void onPause() 
	{

		super.onPause();
		try
		{
			mCamera.stopPreview();
			mCamera.setPreviewCallback(null);
			mPreview.getHolder().removeCallback(mPreview);
			mCamera.release();
			mCamera = null;
		}
		catch(Exception e)
		{
			Log.e("ERROR","OnPauseError!!!");
		}
		
		
	    if(mSensorManager != null)
	    	mSensorManager.unregisterListener(this);	
	    
	    if(mUpdateTimer != null)
	    	mUpdateTimer.cancel();
	    
	    if(mTpad != null)
	    	mTpad.turnOff();
	}	

	@Override
	protected void onDestroy()
	{

	    if(mSensorManager != null)
	    	mSensorManager.unregisterListener(this);	
	    
	    if(mUpdateTimer != null)
	    	mUpdateTimer.cancel();
	    
	    if(mTpad != null)
	    	mTpad.turnOff();

	    
		super.onDestroy();
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	// TODO Make own camera class
	
	
	
	
	private PictureCallback mPicture = new PictureCallback() 
	{
		
		 @Override
		 public void onPictureTaken(byte[] data, Camera camera) 
		 {
			 Log.e("ERROR","Before WriteMediaFile");
		        File pictureFile = writeMediaFile();
		        Log.e("ERROR","After WriteMediaFile");
		        
		        if (pictureFile == null)
		        {
		            Log.e("ERROR", "Error creating media file, check storage permissions "); //e.getMessage
		            return;
		        }

		        try
		        {
		            FileOutputStream fos = new FileOutputStream(pictureFile,false);
		            byte[] data_rotated = rotateByteImg(data, 270);
		            fos.write(data_rotated);
		            //fos.write(data);
		            fos.close();
		            Log.e("ERROR","Closed FOS");
		            
		        } 
		        catch (FileNotFoundException e) 
		        {
		            Log.e("ERROR", "File not found: " + e.getMessage());
		        } 
		        catch (IOException e) 
		        {
		            Log.e("ERROR", "Error accessing file: " + e.getMessage());
		        }
		        
		   
		        if(!mHasGyroscope)	
		        {
		        	mVibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		        	mVibrator.vibrate(200);
		        }
				try 
				{
					Thread.sleep(1000);
				} 
				catch (InterruptedException e) 
				{
					e.printStackTrace();
				}
		        Log.e("ERROR","Done.");

		    }
	};

	@SuppressWarnings("deprecation")
	public int getSurfaceOrientation() 
	{
		// Adapt rotation to current orientation
		android.hardware.Camera.CameraInfo info = new android.hardware.Camera.CameraInfo();
		android.hardware.Camera.getCameraInfo( mDefaultCameraId, info);
		int rotation = this.getWindowManager().getDefaultDisplay().getRotation();
		int degrees = 0;
		switch (rotation) {
		case Surface.ROTATION_0: degrees = 0; break;
		case Surface.ROTATION_90: degrees = 90; break;
		case Surface.ROTATION_180: degrees = 180; break;
		case Surface.ROTATION_270: degrees = 270; break;
		}

		int result;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) 
		{
			result = (info.orientation + degrees) % 360;
			result = (360 - result) % 360;  // compensate the mirror
		} 
		else 
		{  // back-facing
			result = (info.orientation - degrees + 360) % 360;
		}

		return result;
	}
	public Camera setupCamera()
	{
		Camera cam = null;
		
		
        cam = getCameraInstance();
        
        if(cam == null)
        {
        	Log.e("ERROR", "mCamera is null!!!");

        }
       
        Camera.Parameters params = cam.getParameters();
        
        Log.d(TAG, "getSupportedPreviewSizes()");
        
		List<android.hardware.Camera.Size> sizes = params.getSupportedPreviewSizes();

		
		if (sizes != null) 
		{

			int minIndex=0; 
			double minDiff=1000000;
			
			for (int i=0; i<sizes.size(); i++) 
			{
				android.hardware.Camera.Size size = sizes.get(i);
				Log.i(TAG, "size " + i + "=" + size.height + "x" + size.width);
				double currentDiff = Math.sqrt(Math.pow(size.height - mHeight, 2.0) + Math.pow(size.width - mWidth, 2.0));
				if (currentDiff < minDiff) 
				{
					minDiff = currentDiff;
					minIndex = i;
				}
			}
			mWidth  = sizes.get(minIndex).width;
			mHeight = sizes.get(minIndex).height;

			/* Select the size that fits surface considering maximum size allowed */
			params.setPreviewFormat(ImageFormat.NV21);
			params.setPreviewSize(mWidth, mHeight);

			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH)
			{
				params.setRecordingHint(true);
			}
			
			
			cam.setParameters(params);



		}

		int result = getSurfaceOrientation();
		Log.i(TAG, "camera orientation " + result);
		if( cam != null)
		{
			cam.setDisplayOrientation(result);
		}
		
		return cam;
	}
	
	@SuppressWarnings("deprecation")
	public static Camera getCameraInstance() 
	{
	    int cameraCount = 0;
	    Camera cam = null;
	    
	    Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
	    cameraCount = Camera.getNumberOfCameras();
	    for ( int camIdx = 0; camIdx < cameraCount; camIdx++ ) 
	    {
	        Camera.getCameraInfo( camIdx, cameraInfo );
	        if ( cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT  ) 
	        {
	            try 
	            {
	                cam = Camera.open( camIdx );
	            } 
	            catch (RuntimeException e) 
	            {
	                Log.e(TAG, "Camera failed to open: " + e.getLocalizedMessage());
	            }
	        }
	    }

	    return cam;
	}
	
	private File writeMediaFile() 
	{
		File LoggingDir = Environment.getExternalStorageDirectory();
		//File LoggingDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
		if (! LoggingDir.exists())
		{
	        if (! LoggingDir.mkdirs())
	        {
	            Log.d(TAG, "failed to create directory");
	            return null;
	        }
	    }
		Long tsLong = System.currentTimeMillis()/1000;
		String timeStamp = tsLong.toString();
		//String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date(0));
	    File mediaFile;
	    Log.i("TAG",timeStamp);
//	    mediaFile = new File(LoggingDir.getPath() + File.separator + "IMG_"+ timeStamp + ".jpg");
	    mediaFile = new File(LoggingDir.getPath() + File.separator + "IMG_"+ mPictureName + timeStamp + ".jpg");
	    
	    
	    if( (mFirstPictureTaken == true) && (mSecondPictureTaken == false))
	    {
	    	//mSelfieFrontPath = LoggingDir.getPath() + File.separator + "IMG_"+ mPictureName + timeStamp + ".jpg";
	    	mSelfieFrontPath = "IMG_"+ mPictureName + timeStamp + ".jpg";
	    	Log.i("CameraActivity","First Picture Path: " + mSelfieFrontPath);
	    }	
	    else if( (mFirstPictureTaken == true) && (mSecondPictureTaken == true))
	    {
	    	//mSelfieSidePath = LoggingDir.getPath() + File.separator + "IMG_"+ mPictureName + timeStamp + ".jpg";
	    	mSelfieSidePath = "IMG_"+ mPictureName + timeStamp + ".jpg";
	    	Log.i("CameraActivity","Second Picture Path: " + mSelfieSidePath);
	    }
	    else
	    {
	    	
	    }
	    
	    
	    return mediaFile;
	}
	
	public byte[] rotateByteImg(byte[] data, int degree){
		Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);
		Bitmap bmp_rotated = rotateBitmapImage(degree, bmp);
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bmp_rotated.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] flippedImageByteArray = stream.toByteArray();
		return flippedImageByteArray;
		
	}
	public Bitmap rotateBitmapImage(int angle, Bitmap bitmapSrc) {
	    Matrix matrix = new Matrix();
	    matrix.postRotate(angle);
	    return Bitmap.createBitmap(bitmapSrc, 0, 0, 
	        bitmapSrc.getWidth(), bitmapSrc.getHeight(), matrix, true);
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

	 	if(id == android.R.id.home)
	 	{
		    if(mSensorManager != null)
		    	mSensorManager.unregisterListener(this);	
		    
		    if(mUpdateTimer != null)
		    	mUpdateTimer.cancel();
		    
		    if(mTpad != null)
		    	mTpad.turnOff();
			try
			{
				mCamera.stopPreview();
				mCamera.setPreviewCallback(null);
				mPreview.getHolder().removeCallback(mPreview);
				mCamera.release();
				mCamera = null;
			}
			catch(Exception e)
			{
				Log.e("ERROR","OnPauseError!!!");
			}  
	        finish();
	        return true;
	    }

		return super.onOptionsItemSelected(item);
	}


	
}