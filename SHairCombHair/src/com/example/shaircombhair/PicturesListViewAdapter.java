package com.example.shaircombhair;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.example.shaircombhair.ListPicture.Category;
import com.example.shaircombhair.MainActivity.ListType;

import android.R.string;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import nxr.tpad.lib.TPad;
import nxr.tpad.lib.TPadImpl;
import nxr.tpad.lib.consts.TPadVibration;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RatingBar.OnRatingBarChangeListener;

//hier wird der Adapter der listview erstellt

public class PicturesListViewAdapter extends BaseAdapter{
	
	private final List<ListPicture> mItems = new ArrayList<ListPicture>();
	private Context mContext;
	private int mX; //koordinaten fuer ontouchevent
	private int mY;
	private int width;
	private int height;
	//private ArrayList<Boolean> rated = new ArrayList<Boolean>();
	//private boolean tempBolean;
	
	//variablen fuer Tpad
	private static TPad mTpad								= null;
	private static float vy 								= 0.0f;
	private static float vx 								= 0.0f;
	private static float px 								= 0.0f;
	private static float py 								= 0.0f;
	public VelocityTracker vTracker = null;
	
	
	public PicturesListViewAdapter(Context context, TPad tpad) {
		this.mContext = context;
		Log.i("TAG", "context set");
		mTpad=tpad;
		
	}
	
	//additem??
	public void add(ListPicture item) {
		mItems.add(item);
//		GlobalConstants.rated.add(false);
		notifyDataSetChanged();
		
	}
	
	public void add(ArrayList<ListPicture> bildArray) {
		mItems.addAll(bildArray);
//		for(int i=0;i<bildArray.size();i++){
//			GlobalConstants.rated.add(false);
//		}
		notifyDataSetChanged();
	}
	
	//change data in array
	public void changeData(ArrayList<ListPicture> bildArray) {
		mItems.clear();
//		GlobalConstants.rated.clear();
		mItems.addAll(bildArray);
//		for(int i=0;i<bildArray.size();i++){
//			GlobalConstants.rated.add(false);
//		}
		notifyDataSetChanged();
	}
	
	//automatisch erstellt
	@Override
	public int getCount() {
		return mItems.size();
	}
	@Override
	public Object getItem(int position) {
		return mItems.get(position);
	}
	@Override
	public long getItemId(int position) {
		return position;
	}
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
 
				final ListPicture mListPicture = (ListPicture) getItem(position);		
				

				// Inflate the View 
				RelativeLayout itemLayout = null;
				
				//Here, position is the index in the list, the convertView is the view to be
				//recycled (or created), and parent is the ListView itself.
				
				//Use ViewHolder, findViewById will not be called frequently.
				itemLayout = (RelativeLayout) convertView;
				final ViewHolder holder;
				//check whether convertView holds an already allocated View before created a new View.
				if(itemLayout == null){
					LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
					itemLayout = (RelativeLayout) inflater.inflate(R.layout.pictures_in_listview, parent, false);	
					
					holder = new ViewHolder();
					holder.imageView = (ImageView) itemLayout.findViewById(R.id.picture);
					holder.userRatingBar = (RatingBar) itemLayout.findViewById(R.id.userRatingBar);
					holder.avRatingBar = (RatingBar) itemLayout.findViewById(R.id.avRatingBar);
					//store it as the 'tag' of our view
					itemLayout.setTag(holder);
				} else {
					holder = (ViewHolder) itemLayout.getTag();
				}
		
				// Display Bitmap aus mListPicture
				String imagePath = "/sdcard/" + mListPicture.getImageLocation();
				File imgFile = new  File(imagePath);

				if(imgFile.exists()){
				    Bitmap MyBitMap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
				    holder.imageView.setImageBitmap(MyBitMap);
				    //String drwablePath = "R.drawable." + mListPicture.getImageLocation(); -> macht momentan keinen sinn....
				    //holder.imageView.setImageResource(drwablePath);
				}else{
					Log.i("TAG","Picture not found");
					Log.i("TAG",imagePath);
				}
				
				
				holder.imageView.setOnTouchListener(new View.OnTouchListener() {
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						    int x = (int) event.getX();
						    int y = (int) event.getY();
						    mX = x;
						    mY = y;
						    //Log.i("TAG", mX +" "+ mY);
						    return false;
					}
				});
				
				
				//ontouch fuer tpad auf ratingbar
				holder.userRatingBar.setOnTouchListener(new View.OnTouchListener() {
					
					@Override
					public boolean onTouch(View v, MotionEvent event) {
						// Get position coordinates, and scale for proper dataBitmap size
						px = (int) event.getX();
						py = (int) event.getY();

						switch (event.getActionMasked()) 
						{

							// Case where user first touches down on the screen
							case MotionEvent.ACTION_DOWN:
					
								//Log.i("TAG","TOUCHED!!!!!!!!!!!!!!!!!!! ");
								
								// Start a new velocity tracker
								if (vTracker == null) 
								{
									vTracker = VelocityTracker.obtain();
								} 
								else 
								{
									vTracker.clear();
								}
					
								// Add first event to tracker
								vTracker.addMovement(event);
					
								//onFingerDownEvent(vx, vy, px, py);
								// Image display is called for one, two or three fingers, because it handles scaling, dragging and so on

					
								break;
					
								
							case MotionEvent.ACTION_MOVE:
					
								// Add new motion even to the velocity tracker
								vTracker.addMovement(event);
					
								// Compute velocity in pixels/ms
								vTracker.computeCurrentVelocity(1);
					
								// Get computed velocities, and scale them appropriately
								vx = vTracker.getXVelocity(); 
								vy = vTracker.getYVelocity(); 
					
								//Log.i("TAG","Velo:"+vx);
					
								//onFingerMoveEvent(vx, vy, px, py);
								int vibr = 100;// + (int)(mCurrentVelocity * 50);
								//Log.i("TAG","TPad Frequency: " + vibr);
								
								mTpad.sendVibration(TPadVibration.SQUARE,vibr,1);
					
								break;
					
							case MotionEvent.ACTION_UP:
								//onFingerUpEvent(vx, vy, px, py);
								mTpad.turnOff();
					
								break;
			
							case MotionEvent.ACTION_CANCEL:
								// Recycle velocity tracker on a cancel event
								vTracker.recycle();
								//set to null after recycle to prevent illegal state
								vTracker = null;
								break;
						}
						
						return false;
					}
					
					
				});
						
				
//			final int curPos;
//			curPos = position;
			
//			if(GlobalConstants.rated.get(position)){
			if(GlobalConstants.ratedSorted.get(mListPicture.getSelfieID())){
				holder.avRatingBar.setRating(mListPicture.getRating());	//setzte das rating auf durchschnitts Ratingbar
				holder.avRatingBar.setVisibility(View.VISIBLE); //durchschnittsbewertung sichtbar machen
			}else{
				holder.avRatingBar.setVisibility(View.INVISIBLE); //durchschnittsbewertung sichtbar machen
			}
				
				if (mListPicture.getCategory()==Category.selfie){//falls selfie, bewertbar
				//langer klick auf bild zeigt userRatingBar:
				holder.imageView.setOnLongClickListener(new OnLongClickListener() {

			        @Override
			        public boolean onLongClick(View v) {
			        	Log.i("TAG", mX +" "+ mY);
			        	width = getScreenWidth();
			            Log.i("TAG", "breite:" + width );
			            
//			            if(GlobalConstants.rated.get(curPos) == false){
			            if(GlobalConstants.ratedSorted.get(mListPicture.getSelfieID())==false){
			            	//GlobalConstants.rated.set(curPos, true);
			            	GlobalConstants.ratedSorted.set(mListPicture.getSelfieID(),true);
//			        		Log.i("db", "current index of listView: " + curPos);
			        		//rating bar an richtige stelle setzen
			        		//RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(mX, mY);
			        		RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(
			                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
			        		int ratingBarWidth = holder.userRatingBar.getWidth();
			        		if(mX<=(width-ratingBarWidth)){ //pruefen ob zu weit rechts
						    rel_btn.leftMargin = mX;
						    rel_btn.topMargin = mY;
			        		} else {
			        		rel_btn.leftMargin = width-ratingBarWidth;
			        		rel_btn.topMargin = mY;
			        		}
			        			
						    rel_btn.width = LayoutParams.WRAP_CONTENT;
						    rel_btn.height = LayoutParams.WRAP_CONTENT;
						    holder.userRatingBar.setLayoutParams(rel_btn);
			        		//holder.userRatingBar.setPadding(mX,mY,0,0);
			        		holder.userRatingBar.setVisibility(View.VISIBLE); //userbewertung sichtbar machen
			        		holder.userRatingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
							public void onRatingChanged(RatingBar ratingBar, float rating,
								boolean fromUser) {
								mListPicture.setRating(rating); //das geaenderte rating speichern
								mListPicture.updateRatingInDatabase();
								holder.userRatingBar.setVisibility(View.INVISIBLE); //userbewertung wieder unsichtbar machen
								holder.avRatingBar.setRating(mListPicture.getRating());	//setzte das rating auf durchschnitts Ratingbar
								holder.avRatingBar.setVisibility(View.VISIBLE); //durchschnittsbewertung sichtbar machen
							}
					}); //end holder.userRatingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {			        	
			            
			        	}//end if(rated  == false)
			        	return true;
			        }
			    }); // end holder.imageView.setOnLongClickListener(new OnLongClickListener() {
				} else if(mListPicture.getCategory()==Category.haircutter){//end if (mListPicture.getCategory()==Category.selfie){//falls selfie, bewertbar
					mListPicture.refreshHaircutterRating();					
					holder.avRatingBar.setRating(mListPicture.getHaircutterRating());	//setzte das rating auf durchschnitts Ratingbar
					holder.avRatingBar.setVisibility(View.VISIBLE); //durchschnittsbewertung sichtbar machen
				}//end else if(mListPicture.getCategory()==Category.haircutter)
				
				holder.imageView.setOnClickListener(new OnClickListener() {
					@Override
					 public void onClick(View v){
						//Toast.makeText(getApplicationContext(), "Clicked on: ", Toast.LENGTH_SHORT).show();
						Log.i("TAG", "entered onItemClick");
						if(mListPicture.getCategory()==Category.selfie){//wenn selfie
							Intent ListItemIntent = new Intent(mContext,DetailsPage.class);
							Log.i("TAG", "new selfieintent");
							ListItemIntent.putExtra("SelfieID", mListPicture.getSelfieID());
							Log.i("TAG", "put extra");
							mContext.startActivity(ListItemIntent);
						}else if(mListPicture.getCategory()==Category.haircutter){
							Intent ListItemIntent = new Intent(mContext,FriseurPage.class);
							Log.i("TAG", "new friseurintent");
							ListItemIntent.putExtra("HaircutterID", mListPicture.getHaircutterID());
							Log.i("TAG", "put extra id: " + mListPicture.getHaircutterID());
							mContext.startActivity(ListItemIntent);
						}
					}
				});
				
				
	// Return the View, just created
			return itemLayout;
	} // end public View getView(int position, View convertView, ViewGroup parent) {



	//viewholder for storing
class ViewHolder {
	
	public ViewHolder() {
			// TODO Auto-generated constructor stub
		}
	//andere views definieren
	ImageView imageView;
	RatingBar userRatingBar;
	RatingBar avRatingBar;
	//boolean rated = false;//variable zeigt an, ob bereits bewertet wurde
	
} //end class ViewHolder {

public static int getScreenWidth() {
    return Resources.getSystem().getDisplayMetrics().widthPixels;
}

public static int getScreenHeight() {
    return Resources.getSystem().getDisplayMetrics().heightPixels;
}




}// end public class PicturesListViewAdapter extends BaseAdapter{

