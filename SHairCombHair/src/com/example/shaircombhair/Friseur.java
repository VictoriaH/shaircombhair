package com.example.shaircombhair;

import java.io.Serializable;
import java.util.Date;


public class Friseur implements Serializable {
    // Namen der Felder auf der Datenbank

	    public static final String FRISEUR_BEWERTUNG = "Bewertung";
	    public static final String FRISEUR_STREET = "Street";
	    public static final String FRISEUR_HOUSENUMBER = "Hausnummer";
	    public static final String FRISEUR_POSTCODE = "PLZ";
	    public static final String FRISEUR_PLACE = "Place";
	    public static final String FRISEUR_LOCATION_LATITUDE = "Longitude";
	    public static final String FRISEUR_LOCATION_LONGITUDE = "Latitude";
	    public static final String FRISEUR_UMKREIS = "Umkreis";
	    public static final String FRISEUR_NAME = "Name";
	    public static final String FRISEUR_IDENTIFIER = "id";
	    public static final String FRISEUR_ADRESSE = "Adresse";






    // Member Variablen
    private double Bewertung;
    private String Street; 
    private Integer Hausnummer;
    private Integer PLZ;
    private String Place;
    private double Latitude;
    private double Longitude;
    private double Umkreis;    
    private String Name;
    private Integer id;
    private String Adresse;

    





    public Friseur(){}//Konstrukter


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
    public String getStrasse() { return Street;}
    public void setStrasse(String Street) {
        this.Street = Street;
    }
    
    public String getAdresse() { return Adresse;}
    public void setAdresse(String Adresse) {
        this.Adresse = Adresse;
    }
    
    public Integer getHausnummer() {
        return Hausnummer;
    }

    public void setHausnummer(Integer Hausnummer) {
        this.id = Hausnummer;
    }
    
    public Integer getPLZ() {
        return PLZ;
    }

    public void setPLZ(Integer PLZ) {
        this.PLZ = PLZ;
    }
    
    
    public String getName() { return Name;}
    public void setName(String Name) {
        this.Name = Name;
    }
    
    public String getPlace() { return Place;}
    public void setPlace(String Place) {
        this.Place = Place;
    }
    
   
    
    public double getBewertung() {
        return Bewertung;
    }

    public void setBewertung(double Bewertung) {
        this.Bewertung = Bewertung;
    }
    
    public double getUmkreis() {
        return Umkreis;
    }

    public void setUmkreis(double Umkreis) {
        this.Umkreis = Umkreis;
    }
    
    
    
    public double getLatitude() {
        return Latitude;
    }

    public void setLatitude(double Latitude) {
        this.Latitude = Latitude;
    }
    
    
    
    public double getLongitude() {
        return Longitude;
    }

    public void setLongitude(double Longitude) {
        this.Longitude = Longitude;
    }
    
    

}


