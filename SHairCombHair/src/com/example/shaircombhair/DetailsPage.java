package com.example.shaircombhair;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

import com.example.shaircombhair.MainActivity.ListType;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.RelativeLayout.LayoutParams;
import nxr.tpad.lib.TPad;
import nxr.tpad.lib.TPadImpl;
import nxr.tpad.lib.consts.TPadVibration;

// hier werden die klassen, methoden, etc fuer die frieseur und selfie details seiten definiert
public class DetailsPage extends Activity {
	
	ImageView MainPicture;
	ImageView SidePicture;
	TextView NameOfUser;
	int pictureHeight;
	RatingBar avRatingBar;
	RatingBar userRatingBar;
	ListView commentsListView;
	FriseurDetailsFragment mFriseurDetailsFragment;
	String pictureID = null;
	ListPicture mListPicture = null;
	Button picturesFromUser = null;
	CommentsListViewAdapter mAdapter;//leeren listview adapter erstellen
	ArrayList<Comment> commentArray;
	DatabaseAdapter mDatabaseAdapter;
	//boolean rated = false;//variable zeigt an, ob bereits bewertet wurde
	// returned variables from camera activity
	String mSelfieFrontPath;
	String mSelfieSidePath;
    String mCurrentPosition;	
	File mPath;
	
	private int mX; //koordinaten fuer ontouchevent
	private int mY;
	private int width;
	private int height;
	
	//variablen fuer Tpad
		private static TPad mTpad								= null;
		private static float vy 								= 0.0f;
		private static float vx 								= 0.0f;
		private static float px 								= 0.0f;
		private static float py 								= 0.0f;
		public VelocityTracker vTracker = null;
	
	static private final int CameraRequestCode = 1; //Requestcode
	
	

		
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		//Log.i("DetailsPage", "entered oncreate of DetailsPage");
		super.onCreate(savedInstanceState);
		Log.i("DetailsPage", "superOncreate");
		getActionBar().setDisplayHomeAsUpEnabled(true);
		
		mTpad = new TPadImpl(this);
		
		mPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		
		setContentView(R.layout.details_page);
		Log.i("DetailsPage", "setContentView");
		
		//set database adapter
		mDatabaseAdapter = new DatabaseAdapter();
		
		//get the intent
		int pictureID = getIntent().getIntExtra("SelfieID", 1);
		Log.i("db", "getintentextra PictureID: " + pictureID);
		// get the picture object
		mListPicture = mDatabaseAdapter.getListPictureFromSelfieID(pictureID);
		Log.i("db", "getlistpicture location (DetailsPage): " + mListPicture.getImageLocation());
		
		
		//fragment erstellen
		if(savedInstanceState == null){
			mFriseurDetailsFragment = new FriseurDetailsFragment();
			
			//dem fragment argumente uebergeben
			Bundle argumentsForFragment = new Bundle();
			
			argumentsForFragment.putInt("HaircutterID",mListPicture.getShotAtHaircutterID());
			mFriseurDetailsFragment.setArguments(argumentsForFragment);
			
			FragmentTransaction transaction = getFragmentManager().beginTransaction();		
			transaction.add(R.id.friseurdetailsFragment, mFriseurDetailsFragment);			
			transaction.commit();
			Log.i("DetailsPage", "fragment transaction commited");
		}
				
		picturesFromUser = (Button) findViewById(R.id.morePicturesFromUser);
		MainPicture = (ImageView) findViewById(R.id.picture);
		SidePicture = (ImageView) findViewById(R.id.picture2);
		NameOfUser = (TextView) findViewById(R.id.userName);
		avRatingBar = (RatingBar) findViewById(R.id.avRatingBar);
		userRatingBar = (RatingBar) findViewById(R.id.userRatingBar);
		//friseurdetailsFragment = (Fragment) findViewById(R.id.friseurdetailsFragment);
		
		
		//Display UserName
		NameOfUser.setText(mListPicture.getUserName());
		// Display Bitmap aus mListPicture
		String imagePath = "/sdcard/" + mListPicture.getImageLocation();
		File imgFile = new  File(imagePath);

		if(imgFile.exists()){
		    Bitmap MyBitMap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
		    MainPicture.setImageBitmap(MyBitMap);
		}else{
			Log.i("DetailsPage","Picture not found");
			Log.i("DetailsPage",imagePath);
		}
		
		imagePath = "/sdcard/" + mListPicture.getImageLocation2();
		imgFile = new  File(imagePath);

		if(imgFile.exists()){
		    Bitmap MyBitMap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
		    SidePicture.setImageBitmap(MyBitMap);
		    
		}else{
			Log.i("DetailsPage","Picture not found");
			Log.i("DetailsPage",imagePath);
		}
		
		commentArray=getComments();
		//mAdapter = new CommentsListViewAdapter(getApplicationContext());//adapter erstellen
		//Kommentare in listview laden
		commentsListView = (ListView) findViewById(R.id.commentsListView);
		//mAdapter.add(commentArray);
		
		//TODO: schow rating bar on long click
		//langer klick auf bild zeigt userRatingBar:
		
		
		if(GlobalConstants.ratedSorted.get(mListPicture.getSelfieID())){
			avRatingBar.setRating(mListPicture.getRating());	//setzte das rating auf durchschnitts Ratingbar
			avRatingBar.setVisibility(View.VISIBLE); //durchschnittsbewertung sichtbar machen
		}else{
			avRatingBar.setVisibility(View.INVISIBLE); //durchschnittsbewertung sichtbar machen
		}
		
OnLongClickListener MyOnLongClickListener = new OnLongClickListener() {

	        @Override
	        public boolean onLongClick(View v) {
	        	Log.i("DetailsPage", mX +" "+ mY);
	        	width = getScreenWidth();
	            Log.i("DetailsPage", "breite:" + width );
	        	
	        	if(GlobalConstants.ratedSorted.get(mListPicture.getSelfieID())==false){
	        		GlobalConstants.ratedSorted.set(mListPicture.getSelfieID(),true);
	        		//rating bar an richtige stelle setzen
	        		//RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(mX, mY);
	        		RelativeLayout.LayoutParams rel_btn = new RelativeLayout.LayoutParams(
	                        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	        		int ratingBarWidth = userRatingBar.getWidth();
	        		if(mX<=(width-ratingBarWidth)){ //pruefen ob zu weit rechts
				    rel_btn.leftMargin = mX;
				    rel_btn.topMargin = mY;
	        		} else {
	        		rel_btn.leftMargin = width-ratingBarWidth;
	        		rel_btn.topMargin = mY;
	        		}
	        			
				    rel_btn.width = LayoutParams.WRAP_CONTENT;
				    rel_btn.height = LayoutParams.WRAP_CONTENT;
				    userRatingBar.setLayoutParams(rel_btn);
	        		//holder.userRatingBar.setPadding(mX,mY,0,0);
	        		userRatingBar.setVisibility(View.VISIBLE); //userbewertung sichtbar machen
	        		userRatingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
					public void onRatingChanged(RatingBar ratingBar, float rating,
						boolean fromUser) {
						Log.i("DetailsPage", "entered onRatingChanged");
						setRatingFromRatingBar(rating);
					}
			}); //end holder.userRatingBar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {
	        	     
	        	}//end if(rated  == false)
	        	return true;
	        }
	    };// new OnLongClickListener() 
		
		
		//set onlonglicklistener
		MainPicture.setOnLongClickListener(MyOnLongClickListener); 
		//SidePicture.setOnLongClickListener(MyOnLongClickListener); 

	        		
	        		
	        MainPicture.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					int x = (int) event.getX();
					int y = (int) event.getY();
					mX = x;
					mY = y;
					 //Log.i("DetailsPage", mX +" "+ mY);
					return false;
				}
			});
	        
	        SidePicture.setOnTouchListener(new View.OnTouchListener() {
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					int[] tempIntArray = new int[2];
				    Log.i("DetailsPage", "intarray set");		    
				    SidePicture.getLocationOnScreen(tempIntArray);
				    Log.i("DetailsPage", "glos" + tempIntArray[1]);
				    pictureHeight = tempIntArray[1];//MainPicture.getHeight();//TODO:Hoehe von oberem Bild muss noch verrechnet werden
				    Log.i("DetailsPage", "glos" + pictureHeight);
					int x = (int) event.getX();
					int y = (int) event.getY() + pictureHeight;
					mX = x;
					mY = pictureHeight;
					 //Log.i("DetailsPage", mX +" "+ mY);
					return false;
				}
			});
	      
		
		//TODO load comments
		
	      //zeige mehr bilder vom selben nutzer
			picturesFromUser.setOnClickListener(new View.OnClickListener() {
				
				
			    @Override
			    public void onClick(View v) {
			        Intent ShowPicturesIntent = new Intent(DetailsPage.this,MainActivity.class);
			        ShowPicturesIntent.putExtra("category", ListType.custom);
			        ShowPicturesIntent.putExtra("UserID", mListPicture.getUserID());
			        ShowPicturesIntent.putExtra("UserOrHaircutter", 0);

			        startActivity(ShowPicturesIntent);
			    }
			});
		
		
			//ontouch fuer tpad auf ratingbar
			userRatingBar.setOnTouchListener(new View.OnTouchListener() {
				
				@Override
				public boolean onTouch(View v, MotionEvent event) {
					// Get position coordinates, and scale for proper dataBitmap size
					px = (int) event.getX();
					py = (int) event.getY();

					switch (event.getActionMasked()) 
					{

						// Case where user first touches down on the screen
						case MotionEvent.ACTION_DOWN:
				
							//Log.i("TAG","TOUCHED!!!!!!!!!!!!!!!!!!! ");
							
							// Start a new velocity tracker
							if (vTracker == null) 
							{
								vTracker = VelocityTracker.obtain();
							} 
							else 
							{
								vTracker.clear();
							}
				
							// Add first event to tracker
							vTracker.addMovement(event);
				
							//onFingerDownEvent(vx, vy, px, py);
							// Image display is called for one, two or three fingers, because it handles scaling, dragging and so on

				
							break;
				
							
						case MotionEvent.ACTION_MOVE:
				
							// Add new motion even to the velocity tracker
							vTracker.addMovement(event);
				
							// Compute velocity in pixels/ms
							vTracker.computeCurrentVelocity(1);
				
							// Get computed velocities, and scale them appropriately
							vx = vTracker.getXVelocity(); 
							vy = vTracker.getYVelocity(); 
				
							//Log.i("TAG","Velo:"+vx);
				
							//onFingerMoveEvent(vx, vy, px, py);
							int vibr = 100;// + (int)(mCurrentVelocity * 50);
							//Log.i("TAG","TPad Frequency: " + vibr);
							
							mTpad.sendVibration(TPadVibration.SQUARE,vibr,1);
				
							break;
				
						case MotionEvent.ACTION_UP:
							//onFingerUpEvent(vx, vy, px, py);
							mTpad.turnOff();
				
							break;

						case MotionEvent.ACTION_CANCEL:
							// Recycle velocity tracker on a cancel event
							vTracker.recycle();
							//set to null after recycle to prevent illegal state
							vTracker = null;
							break;
					}
					
					return false;
				}
				
				
			});
		
	}// end protected void onCreate(Bundle savedInstanceState)
	
//@Override
//protected void onActivityResult(int requestCode, int resultCode, Intent data)
//	{
//		super.onActivityResult(requestCode, resultCode, data);
//		
//		if (requestCode == CameraRequestCode) 
//		{
//	        if(resultCode == Activity.RESULT_OK)
//	        {
//	            mSelfieFrontPath 	= data.getStringExtra("FirstSelfie");
//	            mSelfieSidePath 	= data.getStringExtra("SecondSelfie");
//	            mCurrentPosition 	= data.getStringExtra("MyLocation");
//	            
//	            
//	            Log.i("DetailsPage","Result from Camera: " + mSelfieFrontPath + " " + mSelfieSidePath + "   Current Position: " + mCurrentPosition);
//	            
//	            mPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
//	            Database db = new Database(mPath.getPath());
//	            SelfieInformation newSelfieInfo = new SelfieInformation();
//	            newSelfieInfo.mPictureLocation = mSelfieFrontPath;
//	            newSelfieInfo.mPictureLocation2 = mSelfieSidePath;
//	            newSelfieInfo.userName = GlobalConstants.currentUserName;
//	            newSelfieInfo.userID = GlobalConstants.currentUserID;
//	            //Todo: add haircutter from position
//	            //Todo: add date
//	            Log.i("selfieDB", "path: "+mSelfieFrontPath + " path2: " + mSelfieSidePath);
//
//	            try {
//					db.addSelfie2db(newSelfieInfo);
//					Log.i("sefieDB","added new selfieInfo");
//					GlobalConstants.ratedSorted.add(false);
//				} catch (FileNotFoundException e) {
//					// TODO Auto-generated catch block
//					Log.i("selfieDB","fail to add new selfieInfo 1");
//					e.printStackTrace();
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					Log.i("selfieDB","fail to add new selfieInfo 2");
//					e.printStackTrace();
//				}
//	        //aktualisiere Listen und setzen
////	            bildArray = mDatabaseAdapter.getPictureArray(ListType.bestselfie); //Adapter1 fuellen
////	    		mAdapterSB.changeData(bildArray);
////	    		bildArray = mDatabaseAdapter.getPictureArray(ListType.newselfie); // Adapter2 fuellen
////	    		mAdapterSN.changeData(bildArray);
////	    		bildArray = mDatabaseAdapter.getPictureArray(ListType.besthaircutter); // Adapter 3 fuellen
////	    		mAdapterFB.changeData(bildArray);
////	    		bildArray = mDatabaseAdapter.getPictureArray(ListType.newhaircutter); // Adapter 4 fuellen
////	    		mAdapterFN.changeData(bildArray);
////	            
////	    		setCategory(ListType.newselfie);
//	            Intent resetIntent = new Intent(DetailsPage.this, MainActivity.class);
//	            startActivity(resetIntent);
//	        }
//	        if (resultCode == Activity.RESULT_CANCELED)
//	        {
//	            mSelfieFrontPath 	= null;
//	            mSelfieSidePath 	= null;
//	            mCurrentPosition 	= null;
//	        }
//	    }		
//		
//		
//	}
	

	public void setRatingFromRatingBar(float rating){//f�hrt rating bars setzen, etc aus da es nicht im oclicklistener funktioniert
		Log.i("DetailsPage", "entered setRatingFromRatingBar");
		mListPicture.setRating(rating); //das geaenderte rating speichern
		mListPicture.updateRatingInDatabase();
		userRatingBar.setVisibility(View.INVISIBLE); //userbewertung wieder unsichtbar machen
		avRatingBar.setRating(mListPicture.getRating());	//setzte das rating auf durchschnitts Ratingbar
		avRatingBar.setVisibility(View.VISIBLE); //durchschnittsbewertung sichtbar machen
		Log.i("DetailsPage", "finished setRatingFromRatingBar");
		
		//make a new Fragment to update the rating		
		mFriseurDetailsFragment = new FriseurDetailsFragment();		
		//dem fragment argumente uebergeben
		Bundle argumentsForFragment = new Bundle();		
		argumentsForFragment.putInt("HaircutterID",mListPicture.getShotAtHaircutterID());
		mFriseurDetailsFragment.setArguments(argumentsForFragment);		
		FragmentTransaction transaction = getFragmentManager().beginTransaction();		
		transaction.replace(R.id.friseurdetailsFragment, mFriseurDetailsFragment);			
		transaction.commit();

		 Log.i("DetailsPage", "refresh fragment transaction commited");
		
	}
	
	  public static int getScreenWidth() {
          return Resources.getSystem().getDisplayMetrics().widthPixels;
      }

      public static int getScreenHeight() {
      	return Resources.getSystem().getDisplayMetrics().heightPixels;
      }

	private ArrayList<Comment> getComments() {
		//TODO!!
		return null;
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.details_page_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			Toast.makeText(this, "Einstellungen", Toast.LENGTH_SHORT).show();
			return true;
		}
		if (id == R.id.my_pictures) 
		{
			Intent ShowPicturesIntent = new Intent(DetailsPage.this,MainActivity.class);
	        ShowPicturesIntent.putExtra("category", ListType.custom);
	        ShowPicturesIntent.putExtra("UserID", GlobalConstants.currentUserID);
	        ShowPicturesIntent.putExtra("UserOrHaircutter", 0);

	        startActivity(ShowPicturesIntent);
	        return true;
		}
		if (id == R.id.map) 
		{
			Intent MapIntent = new Intent(DetailsPage.this,Map.class);
			
		    List<String> stylistLocations;
		    List<String> stylistNames;
		    List<Float> stylistRatings;

		    
		    // TODO  get these from database!! 
		    stylistLocations 	= new ArrayList<String>();
		    stylistNames 		= new ArrayList<String>();
		    stylistRatings       = new ArrayList<Float>();


			
//		    
//		    stylistLocations.add( "48.151057, 11.563882");
//		    stylistNames.add("SHAG");
//		    stylistLocations.add("48.151665, 11.573464");
//		    stylistNames.add("Patis");
//		    stylistLocations.add( "48.181646, 11.521441");
//		    stylistNames.add("Black VELVET");
//            stylistLocations.add( "48.080862, 11.526486");
//            stylistNames.add("Solln");
//            stylistLocations.add( "48.154464, 11.636663");
//            stylistNames.add("Salon Annegret");
//            stylistLocations.add( "48.154464, 11.636663");
//            stylistNames.add("Kurz");
//            stylistLocations.add( "48.132125, 11.690450");
//            stylistNames.add("Crew ");
//            stylistLocations.add( "48.105049, 11.769981");
//            stylistNames.add("am Rathaus");
//            stylistLocations.add( "48.141693, 11.585738");
//            stylistNames.add("Klaus Sch�tze Frise ");
//            stylistLocations.add( "48.130809, 11.574337");
//            stylistNames.add("Sperl");
//            
//
//		      int numberOfStylists = stylistNames.size();
//		      
	    		Database db = new Database(mPath.getPath());
	    		int numberOfStylists = db.getNumHaircutters();
	    		HaircutterInformation curHaircutterInfo = new HaircutterInformation();
	    		String loc;
	    		for(int i = 0;i<numberOfStylists;i++){
		    		try {
		    			curHaircutterInfo = db.getFriseur(i);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    		loc = String.valueOf(curHaircutterInfo.longitude) + ", " + String.valueOf(curHaircutterInfo.latitude);
		    		stylistLocations.add(loc);
		    		stylistNames.add(curHaircutterInfo.name);
		    		stylistRatings.add(curHaircutterInfo.rating);

	    		}			      
			      
		      
		      MapIntent.putExtra("NumberOfStylists",numberOfStylists);
		      for(int i = 0; i < numberOfStylists; i++)
		      {
		    	  MapIntent.putExtra("stylistNames"+String.valueOf(i), stylistNames.get(i));
		    	  MapIntent.putExtra("LatLng"+String.valueOf(i), stylistLocations.get(i));
		    	  MapIntent.putExtra("stylistRatings"+String.valueOf(i),stylistRatings.get(i));
		    	  Log.i("MAP","MainActivity sends: " + stylistLocations.get(i));
		    	  Log.i("MAP","MainActivity sends rating: " + stylistRatings.get(i));
		      }
		      
			
			startActivity(MapIntent);
			//Toast.makeText(this, "Karte", Toast.LENGTH_SHORT).show();
			return true;
		}
		if (id == R.id.search) 
		{
			Intent SearchIntent = new Intent(DetailsPage.this,Search.class);
			startActivity(SearchIntent);
			//Toast.makeText(this, "Kamera", Toast.LENGTH_SHORT).show();
			return true;
		}
//		if (id == R.id.cam) 
//		{
//			Intent StartCameraIntent = new Intent(DetailsPage.this,CameraActivity.class);
//			startActivityForResult(StartCameraIntent, CameraRequestCode);
//			//Toast.makeText(this, "Kamera", Toast.LENGTH_SHORT).show();
//			return true;
//		}
		if (id == R.id.pictures) 
		{
			Intent StartMainIntent = new Intent(DetailsPage.this,MainActivity.class);
			StartMainIntent.putExtra("category", ListType.bestselfie);
			startActivity(StartMainIntent);
			return true;
		}
		if (id == R.id.haircutters) 
		{
			Intent StartMainIntent = new Intent(DetailsPage.this,MainActivity.class);
			StartMainIntent.putExtra("category", ListType.besthaircutter);
			startActivity(StartMainIntent);
			return true;
		}
	 	if(id == android.R.id.home)
	 	{
	        finish();
	    }
		
		
		return super.onOptionsItemSelected(item);
	}
	
	
}// end public class DetailsPage extends Activity {