package com.example.shaircombhair;

import java.io.File;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

public class FriseurDetailsFragment extends Fragment {

	ImageView PictureOfFriseur;
	RatingBar RatingOfFriseur;
	TextView NameOfFriseur;
	Button GoToFriseur;
	int haircutterID;
	ListPicture mListPicture;
	DatabaseAdapter mDatabaseAdapter;
	
	public FriseurDetailsFragment() 
	{
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) 
	{
		View rootView = inflater.inflate(R.layout.friseur_details_fragment, container,	false);
		
		mDatabaseAdapter = new DatabaseAdapter(); //Database Adapter setzen		
		
		Bundle argumentsFromPicture = getArguments();
        if(argumentsFromPicture != null)
        {
        	haircutterID = argumentsFromPicture.getInt("HaircutterID");
        	mListPicture = mDatabaseAdapter.getListPictureFromHaircutterID(haircutterID);
        	Log.i("TAG","Got listpicture from bundle hairutter id: " + haircutterID);
        	
        }
		
		//views setzen
		PictureOfFriseur = (ImageView) rootView.findViewById(R.id.fiseurPicture);
		RatingOfFriseur = (RatingBar) rootView.findViewById(R.id.friseurRating);
		NameOfFriseur = (TextView) rootView.findViewById(R.id.friseurName);
		GoToFriseur = (Button) rootView.findViewById(R.id.gotoFriseurPage);

		
		
	// Display Bitmap aus mListPicture
		String imagePath = "/sdcard/" + mListPicture.getImageLocation();
		File imgFile = new  File(imagePath);
		
		if(imgFile.exists()){
		    Bitmap MyBitMap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
		    PictureOfFriseur.setImageBitmap(MyBitMap);
		    Log.i("TAG","PictureOfFriseur set");
		}else{
			Log.i("TAG","Picture not found");
			Log.i("TAG",imagePath);
		}
		
		mListPicture.refreshHaircutterRating();
		RatingOfFriseur.setRating(mListPicture.getHaircutterRating());
		Log.i("TAG","Rating Set");
		NameOfFriseur.setText(mListPicture.getHaircutterName());
		Log.i("TAG","NameOfFriseur Set");
		GoToFriseur.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
		        Intent goToFriseurPageIntent = new Intent(getActivity(),FriseurPage.class);
		        goToFriseurPageIntent.putExtra("HaircutterID", haircutterID);
		        startActivity(goToFriseurPageIntent);
		    }
		});
		Log.i("TAG","Button Set");
		
		return rootView;
	}
}
