package com.example.shaircombhair;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.example.shaircombhair.PicturesListViewAdapter.ViewHolder;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnLongClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;

public class CommentsListViewAdapter extends BaseAdapter{
	
	private final List<Comment> mItems = new ArrayList<Comment>();
	private final Context mContext;

	public CommentsListViewAdapter(Context context) {
		mContext = context;
	}
	
	//add items
	public void add(Comment item) {
		mItems.add(item);
		notifyDataSetChanged();
	}
	
	public void add(ArrayList<Comment> bildArray) {
		mItems.addAll(bildArray);
		notifyDataSetChanged();
	}
	
	//change data in array
	public void changeData(ArrayList<Comment> bildArray) {
		mItems.clear();
		mItems.addAll(bildArray);
		notifyDataSetChanged();
	}
	
	
	@Override
	public int getCount() {
		return mItems.size();
	}

	@Override
	public Object getItem(int pos) {
		return mItems.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		return pos;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final Comment mComment = (Comment) getItem(position);		
		

		// Inflate the View 
		RelativeLayout itemLayout = null;
		
		//Here, position is the index in the list, the convertView is the view to be
		//recycled (or created), and parent is the ListView itself.
		
		//Use ViewHolder, findViewById will not be called frequently.
		itemLayout = (RelativeLayout) convertView;
		final ViewHolder holder;
		//check whether convertView holds an already allocated View before created a new View.
		if(itemLayout == null){
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			itemLayout = (RelativeLayout) inflater.inflate(R.layout.comment_listview, parent, false);	
			
			holder = new ViewHolder();
			holder.userName = (TextView) itemLayout.findViewById(R.id.UserName);
			holder.commentText = (TextView) itemLayout.findViewById(R.id.CommentText);
			holder.writeComment = (EditText) itemLayout.findViewById(R.id.WriteComment);
			holder.commentButton = (Button) itemLayout.findViewById(R.id.CommentButton);
			//store it as the 'tag' of our view
			itemLayout.setTag(holder);
		} else {
			holder = (ViewHolder) itemLayout.getTag();
		}

		// Display user Name aus cComment
		    holder.userName.setText(mComment.getUserID());
		//Display Text
		    holder.commentText.setText(mComment.getText());
		//Todo: 
//		    if(letztes item){
//			holder.writeComment.setVisibility(View.VISIBLE);
//			holder.commentButton.setVisibility(View.VISIBLE);
//			}
//		    kommentar schreiben und specihern
		
// Return the View, just created
	return itemLayout;
} // end public View getView(int position, View convertView, ViewGroup parent) {



//viewholder for storing
class ViewHolder {
	
	//andere views definieren
Button commentButton;
EditText writeComment;
TextView commentText;
TextView userName;
ViewHolder() {
	// TODO Auto-generated constructor stub
}

} //end class ViewHolder {
	

}
