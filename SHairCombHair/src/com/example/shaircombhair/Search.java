package com.example.shaircombhair;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

import com.example.shaircombhair.MainActivity.ListType;
import com.google.android.gms.maps.model.LatLng;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class Search extends Activity {

	    static private final int CameraRequestCode = 1; //Requestcode



	    private Button btnFriseurSuchen;
	    private TextView TitelFriseurSuche;
	    private RatingBar ratingBarBewertungsSterne;
	    File mPath;
	    //private float gesuchteMinimaleBewertung;

	    

	    
	    

		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			mPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
			setContentView(R.layout.activity_search);
			    TitelFriseurSuche = (TextView) findViewById(R.id.TitelFriseurSuche);
		        btnFriseurSuchen = (Button) findViewById(R.id.buttonSucheBeginnen);
		        ratingBarBewertungsSterne = (RatingBar) findViewById(R.id.ratingBarBewertungsSterne);	

		        	
		        
		       

		        
		        btnFriseurSuchen.setOnClickListener(new View.OnClickListener() {
		            @Override
		            public void onClick(View v) {
		            	float gesuchteMinimaleBewertung = ratingBarBewertungsSterne.getRating() ;
				        
				        Log.i("Suche","Bewertung: " + gesuchteMinimaleBewertung);

		            	Log.i("Suche", "entered onklick");
	            		Intent MapIntent = new Intent(Search.this,Map.class);
	        			
	        		    List<String> stylistLocations;
	        		    List<String> stylistNames;
	        		    List<Float> stylistRatings;

	        		    
	        		    // TODO  get these from database!! 
	        		    stylistLocations 	= new ArrayList<String>();
	        		    stylistNames 		= new ArrayList<String>();
	        		    stylistRatings       = new ArrayList<Float>();
	        		    int numberOfStylists = 0;
	        		    int numChosenHaircutter = 0;
	        		    float bestRating = 0;
	        			
		            	try {
			            	//makeFriseur();
			            //	getFilter();
//		            		Intent MapIntent = new Intent(Search.this,Map.class);
//		        			
//		        		    List<String> stylistLocations;
//		        		    List<String> stylistNames;
//		        		    List<Float> stylistRatings;
//
//		        		    
//		        		    // TODO  get these from database!! 
//		        		    stylistLocations 	= new ArrayList<String>();
//		        		    stylistNames 		= new ArrayList<String>();
//		        		    stylistRatings       = new ArrayList<Float>();
//		        			

	        	    		Database db = new Database(mPath.getPath());
//	        	    		int numberOfStylists = db.getNumHaircutters();
	        	    		numberOfStylists = db.getNumHaircutters();

	        	    		HaircutterInformation curHaircutterInfo = new HaircutterInformation();
	        	    		String loc;
	        	    		Log.i("Suche", "numberOfStylists: " + numberOfStylists);
	        	    		for(int i = 0;i<numberOfStylists;i++){
	        	    			
	        		    		try {
	        		    			
	        		    			curHaircutterInfo = db.getFriseur(i);
	        		    			Log.i("Suche", "rating of haircutter " + i + " " + curHaircutterInfo.rating);
	        		    			
	        		    			
	        					} catch (JSONException e) {
	        						// TODO Auto-generated catch block
	        						e.printStackTrace();
	        					}
	        		    		if(curHaircutterInfo.rating>=gesuchteMinimaleBewertung){
		        		    		loc = String.valueOf(curHaircutterInfo.longitude) + ", " + String.valueOf(curHaircutterInfo.latitude);
		        		    		stylistLocations.add(loc);
		        		    		stylistNames.add(curHaircutterInfo.name);
		        		    		stylistRatings.add(curHaircutterInfo.rating);
		
	        		    		}
	        		    		if (curHaircutterInfo.rating >= bestRating){
	        		    			bestRating = curHaircutterInfo.rating;
	        		    		}
	        	    		
	        	    		}
	        	    		numChosenHaircutter = stylistRatings.size();
		        			      
		        		      
//		        		      MapIntent.putExtra("NumberOfStylists",numberOfStylists);
//		        		      for(int i = 0; i < numberOfStylists; i++)
//		        		    	  
//		        		      {
//		        		    	  
//		        		    	  MapIntent.putExtra("stylistNames"+String.valueOf(i), stylistNames.get(i));
//		        		    	  MapIntent.putExtra("LatLng"+String.valueOf(i), stylistLocations.get(i));
//		        		    	  MapIntent.putExtra("stylistRatings"+String.valueOf(i),stylistRatings.get(i));
//		        		    	  Log.i("MAP","MainActivity sends: " + stylistLocations.get(i));
//		        		    	  Log.i("MAP","MainActivity sends rating: " + stylistRatings.get(i));
//		        		      
//		        		      }
//		        			
//		        			startActivity(MapIntent);
		        			//Toast.makeText(this, "Karte", Toast.LENGTH_SHORT).show();
		    			} catch(Exception e) { 
		    			    // ...
		    				Log.i("Suche", "Error with finding haircutters!");
		    			} 
//		      		      MapIntent.putExtra("NumberOfStylists",numberOfStylists);
		      		      MapIntent.putExtra("NumberOfStylists",numChosenHaircutter);

	        		      for(int i = 0; i < numChosenHaircutter; i++)
	        		    	  
	        		      {
	        		    	  MapIntent.putExtra("stylistNames"+String.valueOf(i), stylistNames.get(i));
	        		    	  MapIntent.putExtra("LatLng"+String.valueOf(i), stylistLocations.get(i));
	        		    	  MapIntent.putExtra("stylistRatings"+String.valueOf(i),stylistRatings.get(i));
	        		    	  Log.i("Suche","Suche sends: " + stylistLocations.get(i));
	        		    	  Log.i("Suche","Suche sends rating: " + stylistRatings.get(i));

	        		      
	        		      }	        		    	 

	        		      
//	        		      Log.i("Suche","0");
//
//	        		      Bundle extras = MapIntent.getExtras();
//	        		      Log.i("Suche","1");
//	        		      int gettedNumberOfHairstylists= extras.getInt("NumberOfStylists");
//	        		      Log.i("Suche","2");
//
//	        	    	  Log.i("Suche","Number of stored styilists:" + gettedNumberOfHairstylists);
//
//	        	    	  
//	        	    	 	        	    	  
//	        		    	  for(int i = 0; i < gettedNumberOfHairstylists; i++){
//	        		    		  Float tmpRating = extras.getFloat("stylistRatings"+String.valueOf(i));
//	        		    		  Log.i("Suche", "rating: " + tmpRating + "i: " + i );
//	        		    		  String name = extras.getString("stylistNames"+String.valueOf(i));
//	        		    		  String tmp = extras.getString("LatLng"+String.valueOf(i));
//	        		    		  Log.i("Suche", "name: " + name);
//	        		    		  Log.i("Suche", "tmp: " + tmp);
//	        		    	  }
//	    	    		 

	        			
	        		   if(numChosenHaircutter > 0){
	        				startActivity(MapIntent);
	        		   }else
	        		   {
	        			   Toast.makeText(getApplicationContext(), "Keine Friseure gefunden! Die beste Bewertung ist " + bestRating,
	        					   Toast.LENGTH_LONG).show();
	        		   }
	        		
	  		    
		            	Log.i("Suche", "start aktivity");
		                //startActivity(new Intent(getApplicationContext(), Map.class));   
		            	
		                }
		        });
		}

		/*private Friseur makeFriseur() {
			
			Log.i("Suche", "entered makefriseur");
			Friseur friseur = new Friseur();


	        return friseur;
	    }
		*/
		
  /*  private List<Friseur> getFilter() {
	List<Friseur> gefilterteFriseure=null;
	Friseur friseur = new Friseur();
	

	        return gefilterteFriseure;
	    }*/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.search_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			Toast.makeText(this, "Einstellungen", Toast.LENGTH_SHORT).show();
			return true;
		}
		if (id == R.id.my_pictures) 
		{
			Intent ShowPicturesIntent = new Intent(Search.this,MainActivity.class);
	        ShowPicturesIntent.putExtra("category", ListType.custom);
	        ShowPicturesIntent.putExtra("UserID", GlobalConstants.currentUserID);
	        ShowPicturesIntent.putExtra("UserOrHaircutter", 0);

	        startActivity(ShowPicturesIntent);
	        return true;
		}
		if (id == R.id.map) 
		{
			Intent MapIntent = new Intent(Search.this,Map.class);
			
		    List<String> stylistLocations;
		    List<String> stylistNames;
		    List<Float> stylistRatings;

		    
		    // TODO  get these from database!! 
		    stylistLocations 	= new ArrayList<String>();
		    stylistNames 		= new ArrayList<String>();
		    stylistRatings       = new ArrayList<Float>();


			
//		    
//		    stylistLocations.add( "48.151057, 11.563882");
//		    stylistNames.add("SHAG");
//		    stylistLocations.add("48.151665, 11.573464");
//		    stylistNames.add("Patis");
//		    stylistLocations.add( "48.181646, 11.521441");
//		    stylistNames.add("Black VELVET");
//            stylistLocations.add( "48.080862, 11.526486");
//            stylistNames.add("Solln");
//            stylistLocations.add( "48.154464, 11.636663");
//            stylistNames.add("Salon Annegret");
//            stylistLocations.add( "48.154464, 11.636663");
//            stylistNames.add("Kurz");
//            stylistLocations.add( "48.132125, 11.690450");
//            stylistNames.add("Crew ");
//            stylistLocations.add( "48.105049, 11.769981");
//            stylistNames.add("am Rathaus");
//            stylistLocations.add( "48.141693, 11.585738");
//            stylistNames.add("Klaus Sch�tze Frise ");
//            stylistLocations.add( "48.130809, 11.574337");
//            stylistNames.add("Sperl");
//            
//
//		      int numberOfStylists = stylistNames.size();
//		      
	    		Database db = new Database(mPath.getPath());
	    		int numberOfStylists = db.getNumHaircutters();
	    		HaircutterInformation curHaircutterInfo = new HaircutterInformation();
	    		String loc;
	    		for(int i = 0;i<numberOfStylists;i++){
		    		try {
		    			curHaircutterInfo = db.getFriseur(i);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    		loc = String.valueOf(curHaircutterInfo.longitude) + ", " + String.valueOf(curHaircutterInfo.latitude);
		    		stylistLocations.add(loc);
		    		stylistNames.add(curHaircutterInfo.name);
		    		stylistRatings.add(curHaircutterInfo.rating);

	    		}			      
			      
		      
		      MapIntent.putExtra("NumberOfStylists",numberOfStylists);
		      for(int i = 0; i < numberOfStylists; i++)
		      {
		    	  MapIntent.putExtra("stylistNames"+String.valueOf(i), stylistNames.get(i));
		    	  MapIntent.putExtra("LatLng"+String.valueOf(i), stylistLocations.get(i));
		    	  MapIntent.putExtra("stylistRatings"+String.valueOf(i),stylistRatings.get(i));
		    	  Log.i("MAP","MainActivity sends: " + stylistLocations.get(i));
		    	  Log.i("MAP","MainActivity sends rating: " + stylistRatings.get(i));
		      }
		      
			
			startActivity(MapIntent);
			//Toast.makeText(this, "Karte", Toast.LENGTH_SHORT).show();
			return true;
		}
//		if (id == R.id.cam) 
//		{
//			Intent StartCameraIntent = new Intent(Search.this,CameraActivity.class);
//			startActivityForResult(StartCameraIntent, CameraRequestCode);
//			//Toast.makeText(this, "Kamera", Toast.LENGTH_SHORT).show();
//			return true;
//		}
		if (id == R.id.pictures) 
		{
			Intent StartMainIntent = new Intent(Search.this,MainActivity.class);
			StartMainIntent.putExtra("category", ListType.bestselfie);
			startActivity(StartMainIntent);
			return true;
		}
		if (id == R.id.haircutters) 
		{
			Intent StartMainIntent = new Intent(Search.this,MainActivity.class);
			StartMainIntent.putExtra("category", ListType.besthaircutter);
			startActivity(StartMainIntent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
