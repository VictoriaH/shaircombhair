package com.example.shaircombhair;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;


















import java.util.Date;
import java.util.List;

import org.json.JSONException;

import nxr.tpad.lib.TPad;
import nxr.tpad.lib.TPadImpl;
import nxr.tpad.lib.consts.TPadVibration;
import android.view.MotionEvent;
import android.view.VelocityTracker;

import com.example.shaircombhair.ListPicture.Category;
import com.example.shaircombhair.MainActivity.ListType;
import com.example.shaircombhair.createDataBase;

//import com.example.shaircombhair.PicturesListViewFragment;














import com.google.android.gms.maps.model.LatLng;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
//import android.app.FragmentTransaction;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings.Global;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

/*todo: in der main activity wird
	ausgelesen ob ein tpad genutzt wird und dementsprechend ein variable gesetzt
	navigationsleiste angezeigt
	ausgelesen welches fragment geladen wird
	fragment geladen
	falls noetig kamerabutton eingebelendet
*/ 

public class MainActivity extends Activity implements OnTouchEventListener
{

//	public PicturesListViewFragment BestSelfiesFragment;  //wird doch nicht als fragment gemacht
	private static final int LOAD_MORE_ITEMS_REQUEST = 0; //konstante fuer aufruf der LoadMorePictures activity
	static private final int CameraRequestCode = 1; //Requestcode
	
	// returned variables from camera activity
	String mSelfieFrontPath;
	String mSelfieSidePath;
    String mCurrentPosition;	
	
	PicturesListViewAdapter mAdapterSB;//leere listview adapter erstellen
	PicturesListViewAdapter mAdapterSN;
	PicturesListViewAdapter mAdapterFB;
	PicturesListViewAdapter mAdapterFN;
	PicturesListViewAdapter mAdapterCustom;
	int FriseurIDForListview;
	int UserIDForListview;
	
	
	ListView  mListView; //leere Listview erstellen
	ArrayList<ListPicture> bildArray;
	ImageButton camButton;
	public enum ListType {
		newselfie, newhaircutter, bestselfie, besthaircutter, none, custom
	};
	ListType showCategory;
	
	//Variablen fuer saveinstances
	static final String STATE_CATEGORY = "shown category";
	boolean[] tempRatedArray;
	
	DatabaseAdapter mDatabaseAdapter;
	

	//variablen fuer Tpad
	private static TPad mTpad								= null;
	private static float vy 								= 0.0f;
	private static float vx 								= 0.0f;
	private static float px 								= 0.0f;
	private static float py 								= 0.0f;
	private double mCurrentVelocity 						= -1.0f;
	public VelocityTracker vTracker 						= null;
	// Current state
	public boolean mIsTextureLoaded 						= false;
	public int mCurrentInteractionMode 						= 1;	
	
	public String mMatchedStylist;
	public int mMatchedStylistId;
	
	// create Database
	
	createDataBase cDB;
	
	static final String file_name_friseur = "friseur_database.txt";
	static final String file_name_selfie = "selfie_database.txt";
	File mPath;

	
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	/* (non-Javadoc)
	 * @see android.app.Activity#onCreate(android.os.Bundle)
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mDatabaseAdapter = new DatabaseAdapter(); //Database Adapter setzen		
		
		//gespeicherte daten lesen
		if(savedInstanceState != null){
			//Array zur speicherung der bewertungszustaende aus saved instances lesen
			tempRatedArray = savedInstanceState.getBooleanArray(GlobalConstants.STATE_RATINGS);
			for(int i=0;i<tempRatedArray.length;i++){
				GlobalConstants.ratedSorted.set(i, tempRatedArray[i]);
			}
		}else{
			//Array zur speicherung der bewertungszustaende initialisieren
			for(int i=0;i<mDatabaseAdapter.numberOfSelfies;i++){
				GlobalConstants.ratedSorted.add(false);	
			}
		}
		
		setContentView(R.layout.activity_main);
		Log.i("TAG", "oncreate:contentview set");
		
		/**Create database*/
		// create Database if "friseur_database.txt" not exist
		mPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
		mPath.mkdirs();	
		File file_friseur = new File(mPath.getPath() +  "/" + file_name_friseur);
		File file_selfie = new File(mPath.getPath() +  "/" + file_name_selfie);
		if(file_friseur.exists() &&  file_selfie.exists())    
		{
			Log.i("database", "file friseur and serfie exist. Would not update either data files!");
		}
		else
		{
			cDB  = new createDataBase();
			try {

				String jsonResponse = cDB.writeJsonResponse_friseur();
				cDB.saveData(mPath.getPath(), jsonResponse, file_name_friseur);	
	            Log.i("database","in main activity "+ mPath.getPath());
	            
	            String jsonResponse_selfie = cDB.writeJsonResponse_selfie();
				cDB.saveData(mPath.getPath(), jsonResponse_selfie, file_name_selfie);	
	            Log.i("database","in main activity "+ mPath.getPath());
	            
	            
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Log.i("database","error main");
			}
		}

		/** Read database from file
		 * Example: how to get the Friseur info by id*/
		/*
		Database db = new Database(mPath.getPath());
		try {
			HaircutterInformation currentHaircutterInfo1 = db.getFriseur(0);
			Log.i("db","cutter info 1  " + currentHaircutterInfo1.id + " " + currentHaircutterInfo1.name);
			HaircutterInformation currentHaircutterInfo2 = db.getFriseur(1);
			Log.i("db","cutter info 2  " + currentHaircutterInfo2.id + " " + currentHaircutterInfo2.name);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		*/
		

		
		/** add/append new HaircutterInfo*/
		/*
		HaircutterInformation newHaircutterInfo = new HaircutterInformation();
		newHaircutterInfo.name = "new haircutter info";
		try {
			db.addHaircutter2db(newHaircutterInfo);
		} catch (JSONException e) {
			Log.i("db","did not work 1");
			e.printStackTrace();
		}
		catch (FileNotFoundException e) {
			Log.i("db","did not work 2");
			e.printStackTrace();
		}
		*/
		
		
		/**Update an existing haircutter info
		 * Change the haircutterinfo with index i*/
		/*
		newHaircutterInfo.id = 1;
		newHaircutterInfo.name = "update name";
		try {
			db.updateHaircutterInfo( newHaircutterInfo);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		*/
		
		
		
		////////////End update a haircurtter info

		mTpad = new TPadImpl(this);
	
		
		mAdapterSB = new PicturesListViewAdapter(MainActivity.this,mTpad);//adapter1 erstellen
		mAdapterSN = new PicturesListViewAdapter(MainActivity.this,mTpad);//adapter2 erstellen
		mAdapterFB = new PicturesListViewAdapter(MainActivity.this,mTpad);//adapter3 erstellen
		mAdapterFN = new PicturesListViewAdapter(MainActivity.this,mTpad);//adapter4 erstellen
		mAdapterCustom = new PicturesListViewAdapter(MainActivity.this,mTpad);//custom adapter erstellen

		bildArray = mDatabaseAdapter.getPictureArray(ListType.bestselfie); //Adapter1 fuellen
		mAdapterSB.add(bildArray);
		bildArray = mDatabaseAdapter.getPictureArray(ListType.newselfie); // Adapter2 fuellen
		mAdapterSN.add(bildArray);
		bildArray = mDatabaseAdapter.getPictureArray(ListType.besthaircutter); // Adapter 3 fuellen
		mAdapterFB.add(bildArray);
		bildArray = mDatabaseAdapter.getPictureArray(ListType.newhaircutter); // Adapter 4 fuellen
		mAdapterFN.add(bildArray);
		
		//listview finden
		mListView = (ListView) findViewById(R.id.mainlistview);
		
		
		//falls activity �ber intent gestartet richtige kategorie setzen
		Intent MainActivityStartIntent = getIntent();
		if (MainActivityStartIntent != null){
			ListType categoryFromIntent  = (ListType)MainActivityStartIntent.getSerializableExtra("category");
			Log.i("TAG", "getintentextra(category)");
			if(categoryFromIntent == null){
				showCategory = ListType.bestselfie;
			}else{
				showCategory = categoryFromIntent; //custom liste ersellen
				if (showCategory == ListType.custom){
					if (MainActivityStartIntent.getIntExtra("UserOrHaircutter", 2) == 1){
					FriseurIDForListview = MainActivityStartIntent.getIntExtra("HaircutterID", 0);
					bildArray = mDatabaseAdapter.getPictureArrayFromHaircutterID(FriseurIDForListview); // Custom Adapter fuellen
					} else if (MainActivityStartIntent.getIntExtra("UserOrHaircutter", 2) == 0){
						UserIDForListview = MainActivityStartIntent.getIntExtra("UserID", 0);
						bildArray = mDatabaseAdapter.getPictureArrayFromUserID(UserIDForListview); // Custom Adapter fuellen
						}
					
					mAdapterCustom.add(bildArray);
				}
			}
			Log.i("TAG","category from intent: " + getIntFromCategory(showCategory));
		}else{
			showCategory = ListType.bestselfie;//standard anzeigekategorie setzen
		}
		
		setCategory(showCategory);//richtige bilder in listview laden
		Log.i("Main", "category set");

		
//		mListView.setOnTouchListener(new OnTouchListener()
//		{
//			
//			@Override
//			public boolean onTouch(View v, MotionEvent event) 
//			{
//
//				onTouchEvent(event);
//				
//				return false;
//			}
//		});
		//code fuer click auf item in listview
//		mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
//		{
//			
//			
//			@Override
//			public void onItemClick(android.widget.AdapterView<?> parent, View view, int position, long id)
//			{
//				Toast.makeText(getApplicationContext(), "Clicked on: ", Toast.LENGTH_SHORT).show();
//				Log.i("TAG", "entered onItemClick");
//			//Intent ListItemIntent = new Intent(MainActivity.this,DetailsPage.class);
//			
//			//ListItemIntent.putExtra("PicturerClicked", (Serializable)bildArray.get(1));
//			
//			//startActivity(ListItemIntent);
//			}
//			});//end mListView.setOnItemClickListener(new OnItemClickListener(){
		

	

		//folgender code ware fur ein fragment
		// only if activity is created, add fragment container (see main.xml) and start the fragment transaction
		//if (savedInstanceState == null) {
			// Instantiate own fragment objects 
		//	BestSelfiesFragment = new PicturesListViewFragment();
			
			// dynamically fill the containers  --> see fragment_acitivity_main.xml
		//	FragmentTransaction transaction = getFragmentManager().beginTransaction();
			
		//	transaction.add(R.id.mainfragmentcontainer, BestSelfiesFragment);
		//	transaction.commit();
		//}
		 
		 //kamerabutton
		 camButton = (ImageButton) findViewById(R.id.cambutton);
			camButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent StartCameraIntent = new Intent(MainActivity.this,CameraActivity.class);	
				    List<String> stylistLocations;
				    List<String> stylistNames;
				    List<Float> stylistRatings;
				    
				    stylistLocations 	= new ArrayList<String>();
				    stylistNames 		= new ArrayList<String>();
				    stylistRatings       = new ArrayList<Float>();
					
				    // TODO  get these from database!!
//				    stylistLocations.add( "48.151057, 11.563882");
//				    stylistNames.add("SHAG");
//				    stylistLocations.add("48.151665, 11.573464");
//				    stylistNames.add("Patis");
//				    stylistLocations.add( "48.181646, 11.521441");
//				    stylistNames.add("Black VELVET");
//		            stylistLocations.add( "48.080862, 11.526486");
//		            stylistNames.add("Solln");
//		            stylistLocations.add( "48.154464, 11.636663");
//		            stylistNames.add("Salon Annegret");
//		            stylistLocations.add( "48.154464, 11.636663");
//		            stylistNames.add("Kurz");
//		            stylistLocations.add( "48.132125, 11.690450");
//		            stylistNames.add("Crew ");
//		            stylistLocations.add( "48.105049, 11.769981");
//		            stylistNames.add("am Rathaus");
//		            stylistLocations.add( "48.141693, 11.585738");
//		            stylistNames.add("Klaus Sch�tze Frise ");
//		            stylistLocations.add( "48.130809, 11.574337");
//		            stylistNames.add("Sperl");
		            
//				    int numberOfStylists = stylistNames.size();

				    
		    		Database db = new Database(mPath.getPath());
		    		int numberOfStylists = db.getNumHaircutters();
		    		HaircutterInformation curHaircutterInfo = new HaircutterInformation();
		    		String loc;
		    		for(int i = 0;i<numberOfStylists;i++){
			    		try {
			    			curHaircutterInfo = db.getFriseur(i);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			    		loc = String.valueOf(curHaircutterInfo.longitude) + ", " + String.valueOf(curHaircutterInfo.latitude);
			    		stylistLocations.add(loc);
			    		stylistNames.add(curHaircutterInfo.name);
			    		stylistRatings.add(curHaircutterInfo.rating);
		    		}			      
				      
			      
				    StartCameraIntent.putExtra("NumberOfStylists",numberOfStylists);
				    for(int i = 0; i < numberOfStylists; i++)
				    {
				    	  StartCameraIntent.putExtra("stylistNames"+String.valueOf(i), stylistNames.get(i));
				    	  StartCameraIntent.putExtra("LatLng"+String.valueOf(i), stylistLocations.get(i));
				    	  StartCameraIntent.putExtra("stylistRatings"+String.valueOf(i),stylistRatings.get(i));
				    	  Log.i("CameraActivity","MainActivity sends: " + stylistLocations.get(i));
				    }		

					startActivityForResult(StartCameraIntent, CameraRequestCode);
					//Toast.makeText(MainActivity.this, "Kamera", Toast.LENGTH_SHORT).show();
					
				}
			});
				
			
			
	}//end onCreate
	
	
	
	@Override
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data)
	{
		super.onActivityResult(requestCode, resultCode, data);
		
		if (requestCode == CameraRequestCode) 
		{
	        if(resultCode == Activity.RESULT_OK)
	        {
	            mSelfieFrontPath 	= data.getStringExtra("FirstSelfie");
	            mSelfieSidePath 	= data.getStringExtra("SecondSelfie");
	            mCurrentPosition 	= data.getStringExtra("MyLocation");
	            mMatchedStylist = data.getStringExtra("MatchedStylist");
	            mMatchedStylistId = data.getIntExtra("MatchedStylistId", 0);
	            Log.i("matchedStylist", "matched stylist id: "+ mMatchedStylistId + "matched stylist name: "+ mMatchedStylist);
	            
	            
	            Log.i("MainActivity","Result from Camera: " + mSelfieFrontPath + " " + mSelfieSidePath + "   Current Position: " + mCurrentPosition);
	            
	            Database db = new Database(mPath.getPath());
	            SelfieInformation newSelfieInfo = new SelfieInformation();
	            newSelfieInfo.mPictureLocation = mSelfieFrontPath;
	            newSelfieInfo.mPictureLocation2 = mSelfieSidePath;
	            newSelfieInfo.userName = GlobalConstants.currentUserName;
	            newSelfieInfo.userID = GlobalConstants.currentUserID;
	            newSelfieInfo.shotAtHaircutterID = mMatchedStylistId;
	            //Todo: add haircutter from position
	            //Todo: add date
	            Log.i("selfieDB", "path: "+mSelfieFrontPath + " path2: " + mSelfieSidePath);

	            try {
					db.addSelfie2db(newSelfieInfo);
					Log.i("sefieDB","added new selfieInfo");
					GlobalConstants.ratedSorted.add(false);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					Log.i("selfieDB","fail to add new selfieInfo 1");
					e.printStackTrace();
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					Log.i("selfieDB","fail to add new selfieInfo 2");
					e.printStackTrace();
				}
	        //aktualisiere Listen und setzen
//	            bildArray = mDatabaseAdapter.getPictureArray(ListType.bestselfie); //Adapter1 fuellen
//	    		mAdapterSB.changeData(bildArray);
//	    		bildArray = mDatabaseAdapter.getPictureArray(ListType.newselfie); // Adapter2 fuellen
//	    		mAdapterSN.changeData(bildArray);
//	    		bildArray = mDatabaseAdapter.getPictureArray(ListType.besthaircutter); // Adapter 3 fuellen
//	    		mAdapterFB.changeData(bildArray);
//	    		bildArray = mDatabaseAdapter.getPictureArray(ListType.newhaircutter); // Adapter 4 fuellen
//	    		mAdapterFN.changeData(bildArray);
//	            
//	    		setCategory(ListType.newselfie);
	            Intent resetIntent = getIntent();
	            finish();
	            startActivity(resetIntent);
	        }
	        if (resultCode == Activity.RESULT_CANCELED)
	        {
	            mSelfieFrontPath 	= null;
	            mSelfieSidePath 	= null;
	            mCurrentPosition 	= null;
	        }
	    }		
		
		
	}
	
	
	
	
	
	public void setCategory(ListType category){//setzt den adapter mit den bildern der entsprechenen kategorie auf die listview
		showCategory = category;
		switch (getIntFromCategory(category))
		{
		case 1: mListView.setAdapter(mAdapterSB);
				break;
		case 2: mListView.setAdapter(mAdapterSN);
				break;
		case 3: mListView.setAdapter(mAdapterFB);
				break;
		case 4: mListView.setAdapter(mAdapterFN);
				break;
		case 5: mListView.setAdapter(mAdapterSB);//5 wenn was nicht stimmt -> default: best selfies
				break;
		case 6: mListView.setAdapter(mAdapterCustom);
				break;
		default: mListView.setAdapter(mAdapterSB);
		}
		
	}
	
	public int getIntFromCategory (ListType category) {
		int statusInt;
		statusInt = 0;
		if(category == ListType.bestselfie)
		{
			statusInt = 1;
		}
		if(category == ListType.newselfie)
		{
			statusInt = 2;
		}
		if(category == ListType.besthaircutter)
		{
			statusInt = 3;
		}
		if(category == ListType.newhaircutter)
		{
			statusInt = 4;
		}
		if(category == ListType.none)
		{
			statusInt = 5;
		}
		if(category == ListType.custom)
		{
			statusInt = 6;
		}
		if(statusInt == 0)
		{
			statusInt = 5;
		}
		return statusInt;
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			Toast.makeText(this, "Einstellungen", Toast.LENGTH_SHORT).show();
			return true;
		}
		if (id == R.id.my_pictures) 
		{
			//Toast.makeText(this, "Meine Bilder", Toast.LENGTH_SHORT).show();
			 Intent ShowPicturesIntent = new Intent(MainActivity.this,MainActivity.class);
		        ShowPicturesIntent.putExtra("category", ListType.custom);
		        ShowPicturesIntent.putExtra("UserID", GlobalConstants.currentUserID);
		        ShowPicturesIntent.putExtra("UserOrHaircutter", 0);

		        startActivity(ShowPicturesIntent);
			return true;
		}
		if (id == R.id.map) 
		{
			Intent MapIntent = new Intent(MainActivity.this,Map.class);
			
		    List<String> stylistLocations;
		    List<String> stylistNames;
		    List<Float> stylistRatings;

		    
		    // TODO  get these from database!! 
		    stylistLocations 	= new ArrayList<String>();
		    stylistNames 		= new ArrayList<String>();
		    stylistRatings       = new ArrayList<Float>();


			
//		    
//		    stylistLocations.add( "48.151057, 11.563882");
//		    stylistNames.add("SHAG");
//		    stylistLocations.add("48.151665, 11.573464");
//		    stylistNames.add("Patis");
//		    stylistLocations.add( "48.181646, 11.521441");
//		    stylistNames.add("Black VELVET");
//            stylistLocations.add( "48.080862, 11.526486");
//            stylistNames.add("Solln");
//            stylistLocations.add( "48.154464, 11.636663");
//            stylistNames.add("Salon Annegret");
//            stylistLocations.add( "48.154464, 11.636663");
//            stylistNames.add("Kurz");
//            stylistLocations.add( "48.132125, 11.690450");
//            stylistNames.add("Crew ");
//            stylistLocations.add( "48.105049, 11.769981");
//            stylistNames.add("am Rathaus");
//            stylistLocations.add( "48.141693, 11.585738");
//            stylistNames.add("Klaus Sch�tze Frise ");
//            stylistLocations.add( "48.130809, 11.574337");
//            stylistNames.add("Sperl");
//            
//
//		      int numberOfStylists = stylistNames.size();
//		      
	    		Database db = new Database(mPath.getPath());
	    		int numberOfStylists = db.getNumHaircutters();
	    		HaircutterInformation curHaircutterInfo = new HaircutterInformation();
	    		String loc;
	    		for(int i = 0;i<numberOfStylists;i++){
		    		try {
		    			curHaircutterInfo = db.getFriseur(i);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    		loc = String.valueOf(curHaircutterInfo.longitude) + ", " + String.valueOf(curHaircutterInfo.latitude);
		    		stylistLocations.add(loc);
		    		stylistNames.add(curHaircutterInfo.name);
		    		stylistRatings.add(curHaircutterInfo.rating);

	    		}			      
			      
		      
		      MapIntent.putExtra("NumberOfStylists",numberOfStylists);
		      for(int i = 0; i < numberOfStylists; i++)
		      {
		    	  MapIntent.putExtra("stylistNames"+String.valueOf(i), stylistNames.get(i));
		    	  MapIntent.putExtra("LatLng"+String.valueOf(i), stylistLocations.get(i));
		    	  MapIntent.putExtra("stylistRatings"+String.valueOf(i),stylistRatings.get(i));
		    	  Log.i("MAP","MainActivity sends: " + stylistLocations.get(i));
		    	  Log.i("MAP","MainActivity sends rating: " + stylistRatings.get(i));
		      }
		      
			
			startActivity(MapIntent);
			//Toast.makeText(this, "Karte", Toast.LENGTH_SHORT).show();
			return true;
		}
		if (id == R.id.search) 
		{
			Intent SearchIntent = new Intent(MainActivity.this,Search.class);
			startActivity(SearchIntent);
			//Toast.makeText(this, "Suchen", Toast.LENGTH_SHORT).show();
			return true;
		}
		if (id == R.id.switch_best_new) 
		{			
			Log.i("TAG", "switch best new");

			if (showCategory == ListType.besthaircutter){
			showCategory = ListType.newhaircutter;
//			bildArray = mDatabaseAdapter.getPictureArray(showCategory); 
//			mAdapter.changeData(bildArray);
//			mListView.setAdapter(mAdapterFN);
			}else if (showCategory == ListType.bestselfie){
				showCategory = ListType.newselfie;
//				bildArray = mDatabaseAdapter.getPictureArray(showCategory); 
//				mAdapter.changeData(bildArray);
//				mListView.setAdapter(mAdapterSN);
				} else if (showCategory == ListType.newhaircutter){
					showCategory = ListType.besthaircutter;
//					bildArray = mDatabaseAdapter.getPictureArray(showCategory); 
//					mAdapter.changeData(bildArray);
//					mListView.setAdapter(mAdapterFB);
					}else if (showCategory == ListType.newselfie){
						showCategory = ListType.bestselfie;
//						bildArray = mDatabaseAdapter.getPictureArray(showCategory); 
//						mAdapter.changeData(bildArray);
//						mListView.setAdapter(mAdapterSB);
						}else {
							showCategory = ListType.newselfie;
						}
			setCategory(showCategory);
			return true;
		}
//		if (id == R.id.cam) 
//		{
//			
//			Intent StartCameraIntent = new Intent(MainActivity.this,CameraActivity.class);	
//		    List<String> stylistLocations;
//		    List<String> stylistNames;
//		    List<Float> stylistRatings;
//
//		    
//		    // TODO  get these from database!! 
//		    stylistLocations 	= new ArrayList<String>();
//		    stylistNames 		= new ArrayList<String>();
//		    stylistRatings       = new ArrayList<Float>();
//
//
//			
////		    // TODO  get these from database!!
////		    stylistLocations.add( "48.151057, 11.563882");
////		    stylistNames.add("SHAG");
////		    stylistLocations.add("48.151665, 11.573464");
////		    stylistNames.add("Patis");
////		    stylistLocations.add( "48.181646, 11.521441");
////		    stylistNames.add("Black VELVET");
////            stylistLocations.add( "48.080862, 11.526486");
////            stylistNames.add("Solln");
////            stylistLocations.add( "48.154464, 11.636663");
////            stylistNames.add("Salon Annegret");
////            stylistLocations.add( "48.154464, 11.636663");
////            stylistNames.add("Kurz");
////            stylistLocations.add( "48.132125, 11.690450");
////            stylistNames.add("Crew ");
////            stylistLocations.add( "48.105049, 11.769981");
////            stylistNames.add("am Rathaus");
////            stylistLocations.add( "48.141693, 11.585738");
////            stylistNames.add("Klaus Sch�tze Frise ");
////            stylistLocations.add( "48.130809, 11.574337");
////            stylistNames.add("Sperl");
////            
////
////		    int numberOfStylists = stylistNames.size();
//		    
//    		Database db = new Database(mPath.getPath());
//    		int numberOfStylists = db.getNumHaircutters();
//    		HaircutterInformation curHaircutterInfo = new HaircutterInformation();
//    		String loc;
//    		for(int i = 0;i<numberOfStylists;i++){
//	    		try {
//	    			curHaircutterInfo = db.getFriseur(i);
//				} catch (JSONException e) {
//					// TODO Auto-generated catch block
//					e.printStackTrace();
//				}
//	    		loc = String.valueOf(curHaircutterInfo.longitude) + ", " + String.valueOf(curHaircutterInfo.latitude);
//	    		stylistLocations.add(loc);
//	    		stylistNames.add(curHaircutterInfo.name);
//	    		stylistRatings.add(curHaircutterInfo.rating);
//
//    		}			      
//		      
//		      
//		      
//	      
//		    StartCameraIntent.putExtra("NumberOfStylists",numberOfStylists);
//		    for(int i = 0; i < numberOfStylists; i++)
//		    {
//		    	  StartCameraIntent.putExtra("stylistNames"+String.valueOf(i), stylistNames.get(i));
//		    	  StartCameraIntent.putExtra("LatLng"+String.valueOf(i), stylistLocations.get(i));
//		    	  StartCameraIntent.putExtra("stylistRatings"+String.valueOf(i),stylistRatings.get(i));
//		    	  Log.i("CameraActivity","MainActivity sends: " + stylistLocations.get(i));
//		    }		
//
//			startActivityForResult(StartCameraIntent, CameraRequestCode);
//			//Toast.makeText(this, "Kamera", Toast.LENGTH_SHORT).show();
//			return true;
//		}
		if (id == R.id.pictures) 
		{ 			
			Log.i("TAG", "pictures");

			if (showCategory == ListType.newhaircutter){
				showCategory = ListType.newselfie;
//				bildArray = mDatabaseAdapter.getPictureArray(showCategory); 
//				mAdapter.changeData(bildArray);
//				mListView.setAdapter(mAdapterSN);
				}else{
				showCategory = ListType.bestselfie;
//				bildArray = mDatabaseAdapter.getPictureArray(showCategory); 
//				mAdapter.changeData(bildArray);
//				mListView.setAdapter(mAdapterSB);
				} 
			setCategory(showCategory);
			return true;
		}
		if (id == R.id.haircutters) 
		{
			Log.i("TAG", "haircutters");

			if (showCategory == ListType.newselfie){
				showCategory = ListType.newhaircutter;
//				bildArray = mDatabaseAdapter.getPictureArray(showCategory); 
//				mAdapter.changeData(bildArray);
//				mListView.setAdapter(mAdapterFN);
				Log.i("TAG", "2");
			}else{
				showCategory = ListType.besthaircutter;
//				bildArray = mDatabaseAdapter.getPictureArray(showCategory); 
//				mAdapter.changeData(bildArray);
//				mListView.setAdapter(mAdapterFB);
				Log.i("TAG", "1");
				} 
			setCategory(showCategory);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState) {
	    
		tempRatedArray = new boolean[GlobalConstants.ratedSorted.size()];
		for(int i=0;i<GlobalConstants.ratedSorted.size();i++){
			tempRatedArray[i] = GlobalConstants.ratedSorted.get(i);
		}
		
	  savedInstanceState.putBooleanArray(GlobalConstants.STATE_RATINGS, tempRatedArray);
	    Log.i("Main", "Saved Instance State");
	    // Always call the superclass so it can save the view hierarchy state
	    super.onSaveInstanceState(savedInstanceState);
	}
	
	
	
	
	
	
	
	/**
	 * 
	 * @param MotionEvent
	 * @return
	 * 
	 * Main Touch even method. Gets called whenever Android reports a new touch event. We distribute it to the according subclasses in order to have a logical structure of handling these touch events.
	 * 
	 */
	public boolean onTouchEvent(MotionEvent event) 
	{
		// Set the current interaction state according to the number of fingers in contact
		if(event.getPointerCount() == 1)
			mCurrentInteractionMode = 1;
		if(event.getPointerCount() == 2 && this.mIsTextureLoaded)
			mCurrentInteractionMode = 2;
		if(event.getPointerCount() == 3)
			mCurrentInteractionMode = 3;


		// Get position coordinates, and scale for proper dataBitmap size
		px = event.getX();
		py = event.getY();

	

		// Switch by which type of event occured
		switch (event.getActionMasked()) 
		{

			// Case where user first touches down on the screen
			case MotionEvent.ACTION_DOWN:
	
				Log.i("TAG","TOUCHED!!!!!!!!!!!!!!!!!!! ");
				
				// Reset velocities to zero
				vx = 0.0f;
				vy = 0.0f;
	
				// Start a new velocity tracker
				if (vTracker == null) 
				{
					vTracker = VelocityTracker.obtain();
				} 
				else 
				{
					vTracker.clear();
				}
	
				// Add first event to tracker
				vTracker.addMovement(event);
	
				
				
				onFingerDownEvent(vx, vy, px, py);
				// Image display is called for one, two or three fingers, because it handles scaling, dragging and so on

	
				break;
	
				
				
				
				
			case MotionEvent.ACTION_MOVE:
	
				// Add new motion even to the velocity tracker
				vTracker.addMovement(event);
	
				// Compute velocity in pixels/ms
				vTracker.computeCurrentVelocity(1);
	
				// Get computed velocities, and scale them appropriately
				vx = vTracker.getXVelocity(); 
				vy = vTracker.getYVelocity(); 
	
				//Log.i(TAG,String.valueOf(this.mIsMultiTouchActive));
	
				onFingerMoveEvent(vx, vy, px, py);	 

	

	
	
				break;
	
			case MotionEvent.ACTION_UP:
				onFingerUpEvent(vx, vy, px, py);
				
			
	
				break;
	
				
				
				
			case MotionEvent.ACTION_CANCEL:
				// Recycle velocity tracker on a cancel event
				vTracker.recycle();
				//set to null after recycle to prevent illegal state
				vTracker = null;
				break;
		}

		return true;
	}
	@Override
	public void onFingerDownEvent(double vx, double vy, double px, double py)
	{
		
	}
	/**
	 * Calculate the current finger velocity.
	 * 
	 * @param
	 * @return
	 * 
	 * 
	 * 
	 */	
	@Override
	public void onFingerMoveEvent(double vx, double vy, double px, double py)
	{
		mCurrentVelocity = Math.sqrt(vx*vx + vy*vy);	
		Log.i("TAG","Current velocity: " + mCurrentVelocity);
		int vibr = 100;// + (int)(mCurrentVelocity * 50);
		//Log.i("TAG","TPad Frequency: " + vibr);
		
		mTpad.sendVibration(TPadVibration.SQUARE,vibr,1);
	}
	/**
	 * Reset current finger velocity.
	 * @param
	 * @return
	 * 
	 * 
	 */	
	@Override
	public void onFingerUpEvent(double vx, double vy, double px, double py)
	{
		mCurrentVelocity = 0;
		mTpad.turnOff();
	}
	public double getCurrentVelocity() 
	{
		return mCurrentVelocity;
	}	
	
	
	
	
	
	
	
	
	
	@Override
	protected void onDestroy() 
	{
		// TODO Auto-generated method stub
		super.onDestroy();
		mTpad.turnOff();
	}
	
	

	
	
	
	
}
