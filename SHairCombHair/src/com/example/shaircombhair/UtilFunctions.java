package com.example.shaircombhair;

import android.util.Log;

public class UtilFunctions 
{

	
	public static long getUsedMemory() 
	{
		final Runtime runtime = Runtime.getRuntime();
		final long usedMemInMB=(runtime.totalMemory() - runtime.freeMemory()) / 1048576L;
		final long maxHeapSizeInMB=runtime.maxMemory() / 1048576L;
		Log.i("UTIL", "Used in MB " + Long.toString(usedMemInMB) + ", total  " + Long.toString(maxHeapSizeInMB));
		return maxHeapSizeInMB;
	}	
	
	public static int mod(int x, int y)
	{
	    int result = x % y;
	    if (result < 0)
	        result += y;
	    return result;
	}
	
	
}
