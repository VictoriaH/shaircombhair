package com.example.shaircombhair;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import nxr.tpad.lib.TPad;
import nxr.tpad.lib.TPadImpl;
import nxr.tpad.lib.consts.TPadVibration;

import com.example.shaircombhair.MainActivity.ListType;
import com.example.shaircombhair.R.drawable;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnCameraChangeListener;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMyLocationChangeListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.widget.TextView;
import android.widget.Toast;
public class Map extends Activity implements SensorEventListener
{
	
	static private final int CameraRequestCode = 1; //Requestcode
   private GoogleMap googleMap;
   
   Polyline polylineFinal;
   
   private  Marker currentPositionMarker;
  
   private boolean mStartOneTime = true;
   
	private static TPad mTpad							= null;
	private static float vy 							= 0.0f;
	private static float vx 							= 0.0f;
	private static float px 							= 0.0f;
	private static float py 							= 0.0f;
	private double mCurrentVelocity 					= -1.0f;
	public VelocityTracker vTracker 					= null;
	// Current state
	public boolean mIsTextureLoaded 					= false;
	public int mCurrentInteractionMode 					= 1;   
   
	Timer mUpdateTimer;
    public List<LatLng> stylistLocations;
    public List<String> stylistNames;
    public List<Float>  ratings;

    public int mSelectedHairCutter 						= 0;
    
    private LatLng mCurrentPosition;
    private LatLng mClickPosition;
    private float mCurrentZoom = 17.0f;
    
    private LatLng startLatLng, endLatLng,end;
    private RotaTask mRotaTask;
   
    Intent StartMapHapticIntent;
    
    
    final float pi = (float) Math.PI;
    final float rad2deg = 180/pi;   
    
    private double targetAngle;
    private float mDifferenceAngle;
    
    
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mMagnetometer; 
    
    float[] mGravity;
    float[] mGeomagnetic;
    
	private float[] mValuesMagnet      = new float[3];
	private float[] mValuesAccel       = new float[3];
	private float[] mValuesOrientation = new float[3];


    
	SharedPreferences mSharedTargetAngle;
	
	
   
   @Override
   protected void onCreate(Bundle savedInstanceState) 
   {
      super.onCreate(savedInstanceState);
      getActionBar().setDisplayHomeAsUpEnabled(true);

      setContentView(R.layout.map);
      StartMapHapticIntent = new Intent(this,MapHapticActivity.class);
      
      mSharedTargetAngle= this.getSharedPreferences("SharedAngleFile", MODE_WORLD_READABLE);
      mSharedTargetAngle.edit().putFloat("SharedAngle", (float)targetAngle).commit();
      
      mCurrentPosition = new LatLng(48.149047, 11.563586);  // set initial position, do not delete
      mClickPosition = new LatLng(48.149047, 11.563586); 
      
      //Intent intent = new Intent(this, SensorService.class);
      //startService(intent);
      
      /*
       * 
       * Input: (von MainActivity)
       * 
       * locations (initial: 10)
       * Namen der Friseure
       * Bewertung
       * 
       * 
       * getDataFromDatabase()
       * 
       */
      

      
      
      
      
      
      stylistLocations 	= new ArrayList<LatLng>();
      stylistNames 		= new ArrayList<String>();
      ratings 			= new ArrayList<Float>();
      
      Bundle extras = getIntent().getExtras();
      int numberOfHairstylists;
    
      if(extras !=null)
      {
    	  
    	  numberOfHairstylists= extras.getInt("NumberOfStylists");
    	  //Log.i("MAP","Number of received styilists:" + numberOfHairstylists);
    	  

    	  mSelectedHairCutter = extras.getInt("SelectedHairCutter");
    	  

    	  
	    	  for(int i = 0; i < numberOfHairstylists; i++)
	    	  {
	    		  String name = extras.getString("stylistNames"+String.valueOf(i));
	    		  stylistNames.add(name);
	    		  
	    		  LatLng tempLatLng;
	    		  String tmp = extras.getString("LatLng"+String.valueOf(i));
	    		  Log.i("MAP","MapActivity receives: " + tmp);
	    		  String[] latlng = tmp.split(",");
	    		  
	    		  double latitude = Double.parseDouble(latlng[0]);
	    		  double longitude = Double.parseDouble(latlng[1]);
	    		  tempLatLng = new LatLng(latitude, longitude);
	    		  stylistLocations.add(tempLatLng);
	    		  
	    		  float tmpRating = extras.getFloat("stylistRatings"+String.valueOf(i));
	    		  ratings.add(tmpRating);
	    	  }
  
    	  
      }
      
      
      /*
      stylistLocations.add(new LatLng(48.149627, 11.561295));
      stylistLocations.add(new LatLng(48.149859, 11.566215));
      stylistLocations.add(new LatLng(48.149491, 11.565957));
      stylistLocations.add(new LatLng(48.149047, 11.563586));
      
      
      */
      
     
      
      mTpad = new TPadImpl(this);
      
      mSensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
      mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
      mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
      

      try 
      {
    	  
         if (googleMap == null) 
         {
        	
        	 
        	 
        	 //googleMapFragment = ((MapFragment) getFragmentManager().findFragmentById(R.id.map));
             googleMap         = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
            
            
            //googleMapFragment.getMapAsync(this);
            
             googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
             googleMap.setMyLocationEnabled(true);   
             currentPositionMarker = googleMap.addMarker(new MarkerOptions().position(mCurrentPosition).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher)).rotation(0.0f));
             
            // CameraUpdate update  = CameraUpdateFactory.newLatLngZoom(new LatLng(48.13,11.575480),14.0f);
             
             //googleMap.animateCamera(update);
             
             
             double[] gps = getlocation();
             //Log.i("gps","gps pos" + gps[0] + " " + gps[1]);
             startLatLng = new LatLng(gps[0],gps[1]);
             endLatLng = new LatLng(48.137379, 11.575480);
             
             
             
             CameraPosition cameraPosition = new CameraPosition.Builder().
                     target(new LatLng(gps[0],gps[1])).
                     tilt(65).
                     zoom(18.0f).
                     bearing(0).
                     build();

             googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
             
             //googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mapCenter, 13));
             //CameraPosition cameraPosition = CameraPosition.builder().target(mapCenter).zoom(13).bearing(00).build();
             // Animate the change in camera view over 2 seconds
             //googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition),2000, null); 
             
             /*String tmp = "Stylist:";
             int i = 1;
             for(LatLng point: stylistLocations)
             {
          	   
          	   Marker TP = googleMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_launcher)).position(point).title(tmp + i));
          	   i++;
             }*/
             
             //Setting the markers to the right position 
             if(mSelectedHairCutter == 0)
             {
	             for(int i = 0; i < stylistNames.size(); i++)
	             {
	            	 
	            	 googleMap.addMarker(new MarkerOptions().position(stylistLocations.get(i)).title(stylistNames.get(i)+"  "+ String.valueOf( ratings.get(i)+" of 5")).icon(BitmapDescriptorFactory.fromResource(drawable.ic_launcher)));
	            	 //googleMap.addMarker(new MarkerOptions().position(stylistLocations.get(i)).title(stylistNames.get(i)+" \n"+" of 5").icon(BitmapDescriptorFactory.fromResource(drawable.ic_launcher)));
	            	 
	             }
             }
             else
             {
            	 int i = mSelectedHairCutter;
            	 googleMap.addMarker(new MarkerOptions().position(stylistLocations.get(i)).title(stylistNames.get(i)+"  "+ String.valueOf( ratings.get(i)+" of 5")).icon(BitmapDescriptorFactory.fromResource(drawable.ic_launcher)));
            	 cameraPosition = new CameraPosition.Builder().target(stylistLocations.get(i)).tilt(65).zoom(18.0f).bearing(0).build();
            	 googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
             }
             /*googleMap.addMarker(new MarkerOptions().position(new LatLng (48.151057, 11.563882)).title("SHAG"));
             googleMap.addMarker(new MarkerOptions().position(new LatLng (48.151665, 11.573464)).title("Patis"));
             googleMap.addMarker(new MarkerOptions().position(new LatLng (48.181646, 11.521441)).title("Black VELVET"));
             googleMap.addMarker(new MarkerOptions().position(new LatLng (48.080862, 11.526486)).title("Solln"));
             googleMap.addMarker(new MarkerOptions().position(new LatLng (48.154464, 11.636663)).title("Salon Annegret"));
             googleMap.addMarker(new MarkerOptions().position(new LatLng (48.154464, 11.636663)).title("Kurz"));
             googleMap.addMarker(new MarkerOptions().position(new LatLng (48.132125, 11.690450)).title("Crew "));
             googleMap.addMarker(new MarkerOptions().position(new LatLng (48.105049, 11.769981)).title("am Rathaus"));
             googleMap.addMarker(new MarkerOptions().position(new LatLng (48.141693, 11.585738)).title("Klaus Sch�tze Frise "));
             googleMap.addMarker(new MarkerOptions().position(new LatLng (48.130809, 11.574337)).title("Sperl"));
             
  
             
             googleMap.addMarker(new MarkerOptions().position(new LatLng (48.150693,11.56815)).title("Nord"));
             googleMap.addMarker(new MarkerOptions().position(new LatLng (48.149562, 11.565372)).title("Westen"));
             googleMap.addMarker(new MarkerOptions().position(new LatLng (48.147644, 11.566530)).title("Sueden"));
             googleMap.addMarker(new MarkerOptions().position(new LatLng (48.148593, 11.568797)).title("Osten"));
             */
             
             
             googleMap.setOnMarkerClickListener(new OnMarkerClickListener()
             {
            	 
            	 
            	 int counter = 0;
                 @Override
                 public boolean onMarkerClick(Marker markerPos)
                 {
                	 mClickPosition = markerPos.getPosition();
                	 markerPos.showInfoWindow();
              	
                	 

                	 
                	 
                	 if(counter != 0)
                	 {
                	 /*final Polyline poly = new Polyline(null);
                	 poly.remove();*/
                		 polylineFinal.remove();
                	 }
                	  end =  markerPos.getPosition();
                	 
                	 //Log.i("tag","end pos "+ end.latitude + " " + end.longitude);
                	 
                	 mRotaTask = new RotaTask(getApplicationContext(), googleMap,  startLatLng, end);
                	 mRotaTask.execute();
                	 counter++;
                	 
                	 
                	 CameraPosition cameraPosition = new CameraPosition.Builder().target( mCurrentPosition).tilt(65).zoom(18.0f).bearing((float)(targetAngle)).build();
         	        
     				currentPositionMarker.remove();
         	        currentPositionMarker = googleMap.addMarker(new MarkerOptions().position(mCurrentPosition).icon(BitmapDescriptorFactory.fromResource(R.drawable.arrow2)).flat(true).rotation(mValuesOrientation[0]));
         	        
         	        /*
         	        double targetAnglecalcul = targetAngle;
         	        
         	        while(targetAnglecalcul>=0){
         	        	Log.i("TAG","Angle: " + targetAnglecalcul);
         	        	  cameraPosition = new CameraPosition.Builder().target( mCurrentPosition).tilt(65).zoom(18.0f).bearing((float)(targetAngle)).build();
         	        	  targetAnglecalcul=targetAnglecalcul - 10;
             		googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
         	        }
                	
                     		
                	
                	*/
                	 
             	    mUpdateTimer = new Timer("mUpdateTimer");
            	    mUpdateTimer.scheduleAtFixedRate(new TimerTask()
            	    {
            	    	public void run()
            			{
            				targetAngle = getAngleBetweenLocations(mCurrentPosition, end);
            					
            				//targetAngle = mValuesOrientation[0] - currentAngle;
            				targetAngle = (targetAngle+360) % 360;
            				
            				mDifferenceAngle = (float) (targetAngle - mValuesOrientation[0]);
            				mDifferenceAngle = (mDifferenceAngle+360) % 360;
            				
            				 mSharedTargetAngle.edit().putFloat("SharedAngle", (float)targetAngle).commit();
            				 //Log.i("MyMap","Angle: " + mDifferenceAngle);
            					
            				if(mValuesOrientation[1] > -1.0  && mStartOneTime)
            				{
            					mUpdateTimer.cancel();
            					mStartOneTime = false;
            					startActivity(StartMapHapticIntent);
            					
            					//
            				}
            					
            			
            				
            			}
            		}, 
            		0,200); 
                	 
            	    
            	    String[] spitStr= markerPos.getId().split("m");
            	    Log.i("MyMap","   "+spitStr[1]);
            	    int id = Integer.parseInt(spitStr[1])-1;
            	    
            	    
            	    
            	    if(id < ratings.size())
            	    {
            	    	Log.i("MyMap"," Rating  "+ratings.get(id));
	            	    float intensity = (ratings.get(id)/5.0f);
	            	    Log.i("MyMap","   "+intensity);
	              		mTpad.sendVibration(TPadVibration.SAWTOOTH,100,intensity);
	              		try 
	              		{
							Thread.sleep(1000);
						}
	              		catch (InterruptedException e) 
	              		{
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
            	    }
              		mTpad.turnOff();
                     return true;
                 }
                 
           		
             });  
             
             
             
             googleMap.setOnCameraChangeListener(new OnCameraChangeListener() 
             {
      			
      			@Override
      			public void onCameraChange(CameraPosition position) 
      			{

      				
            		if (position.zoom != mCurrentZoom)
            		{
            			mCurrentZoom = position.zoom;
                    }


    				CameraPosition cameraPosition = new CameraPosition.Builder().target( mClickPosition).tilt(65).zoom(mCurrentZoom).bearing((float)(targetAngle)).build();
        	       
    				
    				currentPositionMarker.remove();
        	        currentPositionMarker = googleMap.addMarker(new MarkerOptions().position(mCurrentPosition).icon(BitmapDescriptorFactory.fromResource(R.drawable.arrow2)).flat(true).rotation(mValuesOrientation[0]));

            		googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
             		
            		
                    if(mSelectedHairCutter != 0)
                    {
                   	 	int i = mSelectedHairCutter;
                   	 	cameraPosition = new CameraPosition.Builder().target(stylistLocations.get(i)).tilt(65).zoom(mCurrentZoom).bearing(0).build();
                   	 	googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                    }
            		
      			}
      		});
             
             googleMap.setOnMapClickListener(new OnMapClickListener() 
             {
 
                 @Override
                 public void onMapClick(LatLng pos) 
                 {

                     googleMap.animateCamera(CameraUpdateFactory.newLatLng(pos));
                     mClickPosition = pos;
                     mTpad.turnOff();
                 }
             });
             
             
             
             
             googleMap.setOnMyLocationChangeListener(new OnMyLocationChangeListener() 
             {
            	 @Override
            	    public void onMyLocationChange(Location location) 
            	 {
            	        LatLng loc = new LatLng(location.getLatitude(), location.getLongitude());
            	        mCurrentPosition = loc;

            	    }
			});
             
             
             

         }
     
      }
      catch (Exception e) 
      {
         e.printStackTrace();
      } 
   }
   
   
   
   public void onAccuracyChanged(Sensor sensor, int accuracy) {  }


   public void onSensorChanged(SensorEvent event) 
   {
     if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
       mGravity = event.values;
     if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
       mGeomagnetic = event.values;
     
     if (mGravity != null && mGeomagnetic != null) 
     {
       float R[] = new float[9];
       float I[] = new float[9];
       boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
       
       if (success) 
       {
    	   float diff = 0;
    	   float orientation[] = new float[3];
    	   SensorManager.getOrientation(R, orientation);
    	   mValuesOrientation[0] = orientation[0]*rad2deg; // orientation contains: azimut, pitch and roll
    	   mValuesOrientation[1] = orientation[1]*rad2deg;
    	   mValuesOrientation[2] = orientation[2]*rad2deg;
         
         if  (mValuesOrientation[0] < 0)
         {
        	 
        	  mValuesOrientation[0] = mValuesOrientation[0] + 360;
         }
         
         
         
 	     //float tmp = (mValuesOrientation[0]+360) % 360;
 	     //mValuesOrientation[0] = 360 - tmp;  
 	     
         
         //Log.i("TAG","Angle: " + mValuesOrientation[1]);
       }
     }
   }

   
   public double getAngleBetweenLocations(LatLng src, LatLng dest)
   {
	   double angle = 0.0;
	   double lat1 = src.latitude;
	   double long1 = src.longitude;
	   double lat2 = dest.latitude;
       double long2 = dest.longitude;
       
	   double dLon = (long2 - long1);

	   double y = Math.sin(dLon) * Math.cos(lat2);
	   double x = Math.cos(lat1) * Math.sin(lat2) - Math.sin(lat1) * Math.cos(lat2) * Math.cos(dLon);

	    double brng = Math.atan2(y, x);

	    brng = Math.toDegrees(brng);
	    brng = (brng + 360) % 360;
	    angle = 360 - brng;
	   
	    
	    
	   return angle;
	   
	   
   }
   
   
   // get location
   public double[] getlocation() {
	    LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
	    List<String> providers = lm.getProviders(true);
	    Location l = null;
	    for (int i = 0; i < providers.size(); i++) {
	        l = lm.getLastKnownLocation(providers.get(i));
	        if (l != null)
	            break;
	    }
	    double[] gps = new double[2];
	    if (l != null) {
	        gps[0] = l.getLatitude();
	        gps[1] = l.getLongitude();
	    }
	    return gps;
	    
	}
   
 
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   
   public class RotaTask extends AsyncTask<Void, Integer, Boolean> {
	   
	    private static final String TOAST_MSG = "Calculating";
	    private static final String TOAST_ERR_MAJ = "Impossible to trace Itinerary";
	   
	    private Context context;
	    private GoogleMap gMap;
//	    private String editFrom;
//	    private String editTo;
	    private LatLng start;
	    private LatLng end;
	    private final ArrayList<LatLng> lstLatLng = new ArrayList<LatLng>();
	   
//	    public RotaTask(final Context context, final GoogleMap gMap, final String editFrom, final String editTo) {
	    public RotaTask(final Context context, final GoogleMap gMap, final LatLng start, final LatLng end) {
	        this.context = context;
	        this.gMap= gMap;
//	        this.editFrom = editFrom;
//	        this.editTo = editTo;
	        this.start = start;
	        this.end = end;
	        
       	 Log.i("tag","in Ansyctask end pos "+ end.latitude + " " + end.longitude);
	    }
	   
	    /**
	    * {@inheritDoc}
	    */
	    @Override
	      protected void onPreExecute() {
	        Toast.makeText(context, TOAST_MSG, Toast.LENGTH_LONG).show();
	    }
	   
	    /***
	    * {@inheritDoc}
	    */
	    @Override
	    protected Boolean doInBackground(Void... params) {
	        try {
	            final StringBuilder url = new StringBuilder("http://maps.googleapis.com/maps/api/directions/xml?sensor=false&language=pt");
	            url.append("&origin=");
	            //url.append(editFrom.replace(' ', '+'));
	            url.append(start.latitude + "," + start.longitude);
	            url.append("&destination=");
	            //url.append(editTo.replace(' ', '+'));
	            url.append(end.latitude + "," + end.longitude);
	            
	            Log.i("gps background","gps background pos " + start.latitude + " " + start.longitude);
	   
	            final InputStream stream = new URL(url.toString()).openStream();
	   
	            final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
	            documentBuilderFactory.setIgnoringComments(true);
	   
	            final DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
	   
	            final Document document = documentBuilder.parse(stream);
	            document.getDocumentElement().normalize();
	   
	            final String status = document.getElementsByTagName("status").item(0).getTextContent();
	            if(!"OK".equals(status)) {
	                return false;
	            }
	   
	            final Element elementLeg = (Element) document.getElementsByTagName("leg").item(0);
	            final NodeList nodeListStep = elementLeg.getElementsByTagName("step");
	            final int length = nodeListStep.getLength();
	   
	            for(int i=0; i<length; i++) {        
	                final Node nodeStep = nodeListStep.item(i);
	   
	                if(nodeStep.getNodeType() == Node.ELEMENT_NODE) {
	                    final Element elementStep = (Element) nodeStep;
	             decodePolylines(elementStep.getElementsByTagName("points").item(0).getTextContent());
	                }
	            }
	   
	            return true;          
	        }
	        catch(final Exception e) {
	            return false;
	        }
	    }
	   
	    private void decodePolylines(final String encodedPoints) {
	        int index = 0;
	        int lat = 0, lng = 0;
	   
	        while (index < encodedPoints.length()) {
	            int b, shift = 0, result = 0;
	   
	            do {
	                b = encodedPoints.charAt(index++) - 63;
	                result |= (b & 0x1f) << shift;
	                shift += 5;
	            } while (b >= 0x20);
	   
	            int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
	            lat += dlat;
	            shift = 0;
	            result = 0;
	   
	            do {
	                b = encodedPoints.charAt(index++) - 63;
	                result |= (b & 0x1f) << shift;
	                shift += 5;
	            } while (b >= 0x20);
	   
	            int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
	            lng += dlng;
	   
	            lstLatLng.add(new LatLng((double)lat/1E5, (double)lng/1E5));
	        }
	    }
	   
	    /**
	    * {@inheritDoc}
	    */
	    @Override
	    protected void onPostExecute(final Boolean result) { 
	        if(!result) {
	            Toast.makeText(context, TOAST_ERR_MAJ, Toast.LENGTH_SHORT).show();
	        }
	        else {
	            final PolylineOptions polylines = new PolylineOptions();
	            polylines.color(Color.BLUE);
	            
	            
	   
	            for(final LatLng latLng : lstLatLng) {
	                    polylines.add(latLng);
	            }
	   
	            final MarkerOptions markerA = new MarkerOptions();
	            markerA.position(lstLatLng.get(0));
	            markerA.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
	   
	            final MarkerOptions markerB = new MarkerOptions();
	            markerB.position(lstLatLng.get(lstLatLng.size()-1));
	            markerB.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
	            
	            
	            
	            //gMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lstLatLng.get(0), 16));
	            gMap.addMarker(markerA);
	            polylineFinal = gMap.addPolyline(polylines);
	            gMap.addMarker(markerB); 
	            
	            
	            
	           
	        }
	        
	        
	    }
	  }
	
   
   
   
   
   
   
   
	@Override 
	protected void onPause() 
	{
	    super.onPause();
	    if(mSensorManager != null)
	    	mSensorManager.unregisterListener(this);
	    if(mUpdateTimer != null)
	    	mUpdateTimer.cancel();
	    
	    
	}
	
	@Override 
	protected void onResume()
	{
	    super.onResume();
	    mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
	    mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_UI);	
		
	    mStartOneTime = true;

	}



	
	
	
	@Override
	protected void onDestroy() 
	{
		super.onDestroy();
		
		if(mSensorManager != null)
			mSensorManager.unregisterListener(this);
		if(mTpad != null)
			mTpad.turnOff();
	    if(mUpdateTimer != null)
	    	mUpdateTimer.cancel();
	    
	    //Intent intent = new Intent(this, SensorService.class);
	    //  stopService(intent);
	    if(googleMap != null) 
	    	googleMap.clear();  
	}
   
   
   
	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) 
		{
			Toast.makeText(this, "Einstellungen", Toast.LENGTH_SHORT).show();
			return true;
		}
		if (id == R.id.my_pictures) 
		{
			Intent ShowPicturesIntent = new Intent(Map.this,MainActivity.class);
	        ShowPicturesIntent.putExtra("category", ListType.custom);
	        ShowPicturesIntent.putExtra("UserID", GlobalConstants.currentUserID);
	        ShowPicturesIntent.putExtra("UserOrHaircutter", 0);

	        startActivity(ShowPicturesIntent);
	        return true;
		}
		if (id == R.id.search) 
		{
			Intent SearchIntent = new Intent(Map.this,Search.class);
			//Toast.makeText(this, "Kamera", Toast.LENGTH_SHORT).show();
			startActivity(SearchIntent);
			return true;
		}
//		if (id == R.id.cam) 
//		{
//			Intent StartCameraIntent = new Intent(Map.this,CameraActivity.class);
//			startActivityForResult(StartCameraIntent, CameraRequestCode);
//			//Toast.makeText(this, "Kamera", Toast.LENGTH_SHORT).show();
//			return true;
//		}
		if (id == R.id.pictures) 
		{
			Intent StartMainIntent = new Intent(Map.this,MainActivity.class);
			StartMainIntent.putExtra("category", ListType.bestselfie);
			startActivity(StartMainIntent);
			return true;
		}
		if (id == R.id.haircutters) 
		{
			Intent StartMainIntent = new Intent(Map.this,MainActivity.class);
			StartMainIntent.putExtra("category", ListType.besthaircutter);
			startActivity(StartMainIntent);
			return true;
		}
	 	if(id == android.R.id.home)
	 	{
		    if(mUpdateTimer != null)
		    	mUpdateTimer.cancel();
			if(mTpad != null)
				mTpad.turnOff();
	        finish();
	        return true;
	    }

		return super.onOptionsItemSelected(item);
	}


   
   
   
   
   
   
}













/*

			Intent MapIntent = new Intent(MainActivity.this,Map.class);
			
		    List<String> stylistLocations;
		    List<String> stylistNames;
		    
		    stylistLocations 	= new ArrayList<String>();
		    stylistNames 		= new ArrayList<String>();
			
		    
		    stylistLocations.add( "48.151057, 11.563882");
		    stylistNames.add("SHAG");
		    stylistLocations.add("48.151665, 11.573464");
		    stylistNames.add("Patis");
		    stylistLocations.add( "48.181646, 11.521441");
		    stylistNames.add("Black VELVET");
            stylistLocations.add( "48.080862, 11.526486");
            stylistNames.add("Solln");
            stylistLocations.add( "48.154464, 11.636663");
            stylistNames.add("Salon Annegret");
            stylistLocations.add( "48.154464, 11.636663");
            stylistNames.add("Kurz");
            stylistLocations.add( "48.132125, 11.690450");
            stylistNames.add("Crew ");
            stylistLocations.add( "48.105049, 11.769981");
            stylistNames.add("am Rathaus");
            stylistLocations.add( "48.141693, 11.585738");
            stylistNames.add("Klaus Sch�tze Frise ");
            stylistLocations.add( "48.130809, 11.574337");
            stylistNames.add("Sperl");
            
 
            
			

		      int numberOfStylists = stylistNames.size();
		      
		      MapIntent.putExtra("NumberOfStylists",numberOfStylists);
		      for(int i = 0; i < numberOfStylists; i++)
		      {
		    	  MapIntent.putExtra("stylistNames"+String.valueOf(i), stylistNames.get(i));
		    	  MapIntent.putExtra("LatLng"+String.valueOf(i), stylistLocations.get(i));
		    	  Log.i("MAP","MainActivity sends: " + stylistLocations.get(i));
		      }

			
			
			
			
			
			startActivity(MapIntent);
			//Toast.makeText(this, "Karte", Toast.LENGTH_SHORT).show();
			return true;
 
 */
