//beschreibt ein einzelnes bild in der listview

package com.example.shaircombhair;

import java.util.ArrayList;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Locale;

import android.media.Image;
import android.os.Environment;
import android.util.Log;

public class ListPicture {
	
	DatabaseAdapter mDatabaseAdapter = new DatabaseAdapter(); //Database Adapter setzen		
	
	//delfie und friseur	
	public enum Category {
		selfie, haircutter, none
	};
	public final static SimpleDateFormat FORMAT = new SimpleDateFormat(
			"yyyy-MM-dd HH:mm:ss", Locale.US);
	
	private Category mCategory = Category.none;
	private Date mDate = new Date();
	private String mPictureLocation ="selfie.jpg";
	private String mPictureLocation2 ="selfie.jpg";
	
	
	//selfiedaten
	private int selfieID = 0;
	private int shotAtHaircutterID = 0;
	private float mRating = 0.0f;
	private int mNumberOfRatings = 0; 
	private int userID = 0;
	private String userName = "noUserName";
	
	//haircutterdaten
	private float haircutterRating = 0.0f;
	private String haircutterName = "noHaircutterName";
	private String haircutterAdress = "no adress";
	private String haircutterOpeningHours = "Montag 00:00-00:00 \nDienstag 00:00-00:00 \nMittwoch 00:00-00:00 \n"
			+ "Donnerstag 00:00-00:00 \nFreitag 00:00-00:00 \nSamstag 00:00-00:00";
	private float haircutterLongitude = 0;
	private float haircutterLatitude = 0;
	private int haircutterID = 0;

	
	public Category getCategory() {
		return mCategory;
	}
	
	public void setCategory( Category pictureCategory) {
		mCategory = pictureCategory;
	}
	
	public Date getDate() {
		return mDate;
	}

	public void setDate(Date pictureDate) {
		mDate = pictureDate;
	}
	
	public String getImageLocation() {
		return mPictureLocation;//evtl alternativ direkt image weitergeben
	}
	
	public void setImageLocation(String pictureLocation) {
		mPictureLocation = pictureLocation;
	}

	public String getImageLocation2() {
		return mPictureLocation2;
	}

	public void setImageLocation2(String mPictureLocation2) {
		this.mPictureLocation2 = mPictureLocation2;
	}

	public Float getRating() {
		return mRating;
	}
	
	public void setRating(Float pictureRating) {
		//erhoeht die zahl der ratings und rechnet einen neuen mittelwert aus
		mNumberOfRatings = mNumberOfRatings + 1;
		mRating = (pictureRating + mRating*(mNumberOfRatings-1))/mNumberOfRatings;
		Log.i("tag", "mrating " + mRating);
	}
	
	public void updateRatingInDatabase(){
		ListPicture mTempListPicture = mDatabaseAdapter.getListPictureFromSelfieID(selfieID);
		mTempListPicture.setRating(mRating);
		mTempListPicture.setNumberOfRatings(mNumberOfRatings);
		mDatabaseAdapter.updateSelfieInfoInDB(mDatabaseAdapter.getSelfieInformationFromListpicture(mTempListPicture));
	}
	
	public int getSelfieID() {
		return selfieID;
	}
	
	public void setSelfieID(int id) {
		selfieID=id;
	}
	
	public int getHaircutterID() {
		return haircutterID;
	}
	
	public void setHaircutterID(int id) {
		haircutterID=id;
	}
	
	public String getUserName() {
		return userName;
	}
	
	public void setUserName(String name) {
		userName = name;
	}
	
	public String getHaircutterName() {
		return haircutterName;
	}
	
	public void setHaircutterName(String name) {
		haircutterName = name;
	}

	public String getHaircutterAdress() {
		return haircutterAdress;
	}

	public void setHaircutterAdress(String adress) {
		haircutterAdress = adress;
	}

	public String getHaircutterOpeningHours() {
		return haircutterOpeningHours;
	}

	public void setHaircutterOpeningHours(String openingHours) {
		haircutterOpeningHours = openingHours;
	}

	public int getUserID() {
		return userID;
	}

	public void setUserID(int userID) {
		this.userID = userID;
	}

	public float getHaircutterLongitude() {
		return haircutterLongitude;
	}

	public void setHaircutterLongitude(float longitude) {
		haircutterLongitude = longitude;
	}

	public float getHaircutterLatitude() {
		return haircutterLatitude;
	}

	public void setHaircutterLatitude(float latitude) {
		haircutterLatitude = latitude;
	}

	public int getShotAtHaircutterID() {
		return shotAtHaircutterID;
	}

	public void setShotAtHaircutterID(int shotAtHaircutterID) {
		this.shotAtHaircutterID = shotAtHaircutterID;
	}

	public float getHaircutterRating() {
		return haircutterRating;
	}

	public void setHaircutterRating(float haircutterRating) {
		this.haircutterRating = haircutterRating;
	}
	
	public void refreshHaircutterRating(){
		HaircutterInformation tempHaircutterInfo = new HaircutterInformation();
		ArrayList<ListPicture> TemporarySelfieArray = mDatabaseAdapter.getPictureArrayFromHaircutterID(haircutterID);
		int length;
		length = TemporarySelfieArray.size();
		Boolean rated;
		Float rating;
		int numNotRatedSelfies = 0;
		if (length != 0){
			float temporaryRating = 0;
			for (int i=0 ; i < length ;i++)
				{
					//JXU: should only count the rating of the selfie which is marked as rated. Otherwise, the unrated selfie has 
					//rating 0, then the temp rating will be wrong.
					rated = GlobalConstants.ratedSorted.get(i);
					rating = TemporarySelfieArray.get(i).getRating();
					if(rated == false && rating == 0){
						rating = (float) 0.0;
						numNotRatedSelfies++;
					}
					Log.i("Rating Test", "index: " + i + "if rated: " + rated + " rating: " + rating);
					temporaryRating = temporaryRating + rating;
//					temporaryRating = temporaryRating+TemporarySelfieArray.get(i).getRating();
					
				}
			//haircutterRating = temporaryRating/length;
			//JXU: the not rated selfies should not affect the haircutterRating yet.
			haircutterRating = temporaryRating/(length-numNotRatedSelfies);
			Log.i("Rating Test", "num not reated selfies: " + numNotRatedSelfies + " total num selfies: " + length + " current haircutter rating: " + haircutterRating);
		 	}
		 	else haircutterRating = 0;
		tempHaircutterInfo = mDatabaseAdapter.getHaircuterInformationFromListpicture(mDatabaseAdapter.getListPictureFromHaircutterID(haircutterID));
		tempHaircutterInfo.rating = haircutterRating;
		mDatabaseAdapter.updateHaircutterInfoInDB(tempHaircutterInfo);
		Log.i("ListPicture", "HaircutterRating" + haircutterRating);
	}
	
	public void setNumberOfRatings(int i){
		mNumberOfRatings=i;
	}
	
	public int getNumberOfRatings() {
		return mNumberOfRatings;
	}
}
