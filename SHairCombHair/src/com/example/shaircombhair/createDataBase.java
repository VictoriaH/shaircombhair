package com.example.shaircombhair;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

public class createDataBase {
	int numberOfHaircutters;
	int numberOfSelfies;
 
    public createDataBase(){
    	numberOfHaircutters = 10; //TODO: Update
    	numberOfSelfies = 5; //TODO: Update
    }//Konstrukter
       

    public String writeJsonResponse_friseur() throws JSONException {
    	//haircutter datenbank
    	JSONObject json1 = new JSONObject();
    	JSONObject json2 = new JSONObject();
    	JSONObject json3 = new JSONObject();
    	JSONObject json4 = new JSONObject();
    	JSONObject json5 = new JSONObject();
    	JSONObject json6 = new JSONObject();
    	JSONObject json7 = new JSONObject();
    	JSONObject json8 = new JSONObject();
    	JSONObject json9 = new JSONObject();
    	JSONObject json10 = new JSONObject();
    	
    	json1.put("id", "0");
    	json1.put("name", "New Cut Hairstyling Friseur");
    	json1.put("mPictureLocation","new_cut.png");
    	json1.put("rating",Float.toString((float) 4.3));
    	json1.put("adress", "Schellingstr. 111, 80797 Muenchen");
    	json1.put("openingHours", "Montag 09:00-20:00 \nDienstag 09:00-20:00 \nMittwoch 09:00-20:00 \n"
    			+ "Donnerstag 09:00-20:00 \nFreitag 09:00-20:00 \nSamstag 09:00-16:00");
    	json1.put("longitude",Float.toString((float) 48.155648));
    	json1.put("latitude",Float.toString((float) 11.565374));
    	
    	
    	json2.put("id", "1");
    	json2.put("name", "Soulhair Friseure");
    	json2.put("mPictureLocation","soulhair_friseure.png");
    	json2.put("rating",Float.toString((float) 4.5));
    	json2.put("adress", "Gabelsbergerstr. 62, 80333 Muenchen");
    	json2.put("openingHours", "Montag 09:00-20:00 \nDienstag 09:00-20:00 \nMittwoch 09:00-20:00 \n"
    			+ "Donnerstag 09:00-20:00 \nFreitag 09:00-20:00 \nSamstag 09:00-16:00");
    	json2.put("longitude",Float.toString((float) 48.149709));
    	json2.put("latitude",Float.toString((float) 11.561566));
    	
    	json3.put("id", "2");
    	json3.put("name", "Barberella Friseure GbR");
    	json3.put("mPictureLocation","barberella_friseure.png");
    	json3.put("rating",Float.toString((float) 4.4));
    	json3.put("adress", "Gabelsbergerstr. 62, 80333 Muenchen");
    	json3.put("openingHours", "Montag 09:00-22:00 \nDienstag 09:00-22:00 \nMittwoch 09:00-22:00 \n"
    			+ "Donnerstag 09:00-22:00 \nFreitag 09:00-22:00 \nSamstag 09:00-15:00");
    	json3.put("longitude",Float.toString((float) 48.147819));
    	json3.put("latitude",Float.toString((float) 11.564184));
    	
    	json4.put("id", "3");
    	json4.put("name", "Friseur SHAG");
    	json4.put("mPictureLocation","friseur_shag.png");
    	json4.put("rating",Float.toString((float) 4.7));
    	json4.put("adress", "Schellingstr. 48, 80333 Muenchen");
    	json4.put("openingHours", "Montag 10:00-21:00 \nDienstag 10:00-21:00 \nMittwoch 10:00-21:00 \n"
    			+ "Donnerstag 10:00-21:00 \nFreitag 10:00-21:00 \nSamstag 10:00-19:00");
    	json4.put("longitude",Float.toString((float) 48.151827));
    	json4.put("latitude",Float.toString((float) 11.573926));
    	
    	json5.put("id", "4");
    	json5.put("name", "Ralph Salger Friseure");
    	json5.put("mPictureLocation","ralph_salger_friseure.png");
    	json5.put("rating",Float.toString((float) 4.2));
    	json5.put("adress", "Karlstr. 49, 80333 Muenchen");
    	json5.put("openingHours", "Montag 09:00-20:00 \nDienstag 09:00-20:00 \nMittwoch 09:00-20:00 \n"
    			+ "Donnerstag 09:00-20:00 \nFreitag 09:00-20:00 \nSamstag 09:00-14:00");
    	json5.put("longitude",Float.toString((float) 48.145092));
    	json5.put("latitude",Float.toString((float) 11.560712));
    	
    	json6.put("id", "5");
    	json6.put("name", "Hairstyling Paradiso");
    	json6.put("mPictureLocation","hairstyling_paradiso.png");
    	json6.put("rating",Float.toString((float) 4.1));
    	json6.put("adress", "Augustenstr. 72, 80333 Muenchen");
    	json6.put("openingHours", "Montag 09:00-20:00 \nDienstag 09:00-20:00 \nMittwoch 09:00-20:00 \n"
    			+ "Donnerstag 09:00-20:00 \nFreitag 09:00-20:00 \nSamstag 09:00-19:00");
    	json6.put("longitude",Float.toString((float) 48.150016));
    	json6.put("latitude",Float.toString((float) 11.563545));
    	
    	
    	json7.put("id", "6");
    	json7.put("name", "Hairconnect");
    	json7.put("mPictureLocation","hairconnect.png");
    	json7.put("rating",Float.toString((float) 3.8));
    	json7.put("adress", "Brienner Str. 49, 80333 Muenchen");
    	json7.put("openingHours", "Montag 10:00-20:00 \nDienstag 10:00-20:00 \nMittwoch 10:00-20:00 \n"
    			+ "Donnerstag 10:00-20:00 \nFreitag 10:00-20:00 \nSamstag 09:00-16:00");
    	json7.put("longitude",Float.toString((float) 48.147896));
    	json7.put("latitude",Float.toString((float) 11.559167));
    	
    	json8.put("id", "7");
    	json8.put("name", "WellBeing Salon- Friseur-Kosmetik-Nagelpflege");
    	json8.put("mPictureLocation","wellbeing_salon.png");
    	json8.put("rating",Float.toString((float) 5.0));
    	json8.put("adress", "Dachauer Str. 99, 80335 Muenchen");
    	json8.put("openingHours", "Montag 09:00-18:00 \nDienstag 09:00-18:00 \nMittwoch 09:00-20:00 \n"
    			+ "Donnerstag 09:00-20:00 \nFreitag 09:00-20:00 \nSamstag 09:00-16:00");
    	json8.put("longitude",Float.toString((float) 48.152076));
    	json8.put("latitude",Float.toString((float) 11.556850));
    	
      	
    	json9.put("id", "8");
    	json9.put("name", "Unique Friseure");
    	json9.put("mPictureLocation","unique_friseur.png");
    	json9.put("rating",Float.toString((float) 4.8));
    	json9.put("adress", "Hartmannstr. 1, 80333 Muenchen");
    	json9.put("openingHours", "Montag 10:00-19:00 \nDienstag 10:00-19:00 \nMittwoch 10:00-19:00 \n"
    			+ "Donnerstag 10:00-19:00 \nFreitag 10:00-19:00 \nSamstag 10:00-14:30");
    	json9.put("longitude",Float.toString((float) 48.140192));
    	json9.put("latitude",Float.toString((float) 11.572428));
    	
    	json10.put("id", "9");
    	json10.put("name", "Lipperts Friseure");
    	json10.put("mPictureLocation","lippert_friseure.png");
    	json10.put("rating",Float.toString((float) 3.1));
    	json10.put("adress", "Lenbachpl. 3, 80333 Muenchen");
    	json10.put("openingHours", "Montag 10:00-20:30 \nDienstag 08:00-19:30 \nMittwoch 08:00-21:30 \n"
    			+ "Donnerstag 08:00-21:30 \nFreitag 08:00-20:30 \nSamstag 08:00-16:30");
    	json10.put("longitude",Float.toString((float) 48.142225));
    	json10.put("latitude",Float.toString((float) 11.568222));
    	
    	
    	
    	
    	
    	JSONArray jsonArray = new JSONArray();
    	jsonArray.put(json1);
    	jsonArray.put(json2);
    	jsonArray.put(json3);
    	jsonArray.put(json4);
    	jsonArray.put(json5);
    	jsonArray.put(json6);
    	jsonArray.put(json7);
    	jsonArray.put(json8);
    	jsonArray.put(json9);
    	jsonArray.put(json10);
    	
    	JSONObject haircutterObj = new JSONObject();
    	haircutterObj.put("haircutters", jsonArray);
    	String mJsonResponse = haircutterObj.toString();
    	
    	return mJsonResponse;
    }

    public String writeJsonResponse_selfie() throws JSONException {
  //selfie datenbank
	JSONObject selfieJson1 = new JSONObject();
	JSONObject selfieJson2 = new JSONObject();
	JSONObject selfieJson3 = new JSONObject();
	JSONObject selfieJson4 = new JSONObject();
	JSONObject selfieJson5 = new JSONObject();
	
	selfieJson1.put("id", "0");
	selfieJson1.put("userName", "User0");
	selfieJson1.put("userID", "0");
	selfieJson1.put("mPictureLocation","IMG_6697.JPG");
	selfieJson1.put("mPictureLocation2","IMG_6698.JPG");
	selfieJson1.put("rating",Float.toString((float) 4.3)); 
	selfieJson1.put("mNumberOfRatings", "1");
	selfieJson1.put("shotAtHaircutterID", "3");
	selfieJson1.put("mDate", "2016-01-29 12:35:05");
	
	selfieJson2.put("id", "1");
	selfieJson2.put("userName", "User1");
	selfieJson2.put("userID", "1");
	selfieJson2.put("mPictureLocation","IMG_6699.JPG");
	selfieJson2.put("mPictureLocation2","IMG_6701.JPG");
	selfieJson2.put("rating",Float.toString((float) 3.2)); 
	selfieJson2.put("mNumberOfRatings", "1");
	selfieJson2.put("shotAtHaircutterID", "1");
	selfieJson2.put("mDate", "2016-01-29 12:35:05");
	
	selfieJson3.put("id", "2");
	selfieJson3.put("userName", "User2");
	selfieJson3.put("userID", "2");
	selfieJson3.put("mPictureLocation","IMG_6702.JPG");
	selfieJson3.put("mPictureLocation2","IMG_6703.JPG");
	selfieJson3.put("rating",Float.toString((float) 4.2)); 
	selfieJson3.put("mNumberOfRatings", "1");
	selfieJson3.put("shotAtHaircutterID", "3");
	selfieJson3.put("mDate", "2016-01-29 12:35:05");

	selfieJson4.put("id", "3");
	selfieJson4.put("userName", "User3");
	selfieJson4.put("userID", "3");
	selfieJson4.put("mPictureLocation","IMG_6704.JPG");
	selfieJson4.put("mPictureLocation2","IMG_6705.JPG");
	selfieJson4.put("rating",Float.toString((float) 4.9)); 
	selfieJson4.put("mNumberOfRatings", "1");
	selfieJson4.put("shotAtHaircutterID", "1");
	selfieJson4.put("mDate", "2016-01-29 12:35:05");
	
	selfieJson5.put("id", "4");
	selfieJson5.put("userName", "User4");
	selfieJson5.put("userID", "4");
	selfieJson5.put("mPictureLocation","IMG_6706.JPG");
	selfieJson5.put("mPictureLocation2","IMG_6708.JPG");
	selfieJson5.put("rating",Float.toString((float) 2.1)); 
	selfieJson5.put("mNumberOfRatings", "1");
	selfieJson5.put("shotAtHaircutterID", "1");
	selfieJson5.put("mDate", "2016-01-29 12:35:05");
	
	
	JSONArray selfieJsonArray = new JSONArray();
	selfieJsonArray.put(selfieJson1);
	selfieJsonArray.put(selfieJson2);
	selfieJsonArray.put(selfieJson3);
	selfieJsonArray.put(selfieJson4);
	selfieJsonArray.put(selfieJson5);
	
	JSONObject selfieObj = new JSONObject();
	selfieObj.put("selfies", selfieJsonArray);
	String mJsonResponse = selfieObj.toString();
	
	return mJsonResponse;
	
    }
    
    
    
    public void saveData(String path, String mJsonResponse, String fileName) {
    	String temp;
    	int temp2;
        try {

            FileWriter fw = new FileWriter(path + "/" + fileName,true);

            Log.i("database","in create database "+ path + "/"+ fileName);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(mJsonResponse);
            bw.close();
            fw.close();
            
            if (fileName == "friseur_database.txt"){
            temp =	"numberOfHaircutters.txt";
            temp2 =numberOfHaircutters;
            }else{
            	temp="numberOfSelfies.txt";
            	temp2 =numberOfSelfies;
            }
            
            fw = new FileWriter(path + "/" + temp,false);

            Log.i("database","in create database "+ path + "/"+ temp);
            bw = new BufferedWriter(fw);
            bw.write(String.valueOf(temp2));
            bw.close();
            fw.close();

	        } catch (IOException e) {
	            Log.e("TAG", "Error in Writing: " + e.getLocalizedMessage());
	        }
        
    }

    
    
}
